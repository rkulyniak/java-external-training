package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.rkulyniak.app.view.UserInterface.inputLine;

public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - I/O performance comparing");
            put("2", "2 - Push read data back to IntStream");
            put("3", "3 - Print comments from source code");
            put("4", "4 - Print directory tree");
            put("5", "5 - SomeBuffer write");
            put("6", "6 - SomeBuffer read");
            put("7", "7 - NIO client-server");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.compareUsualAndBufferedIOPerformance(inputLine("file")));
            put("2", () -> controller.getIntStreamFromFile(inputLine("file")));
            put("3", () -> controller.getCommentsFromSource(inputLine("file")));
            put("4", () -> controller.getDirectoryTree(inputLine("directory")));
            put("5", () -> controller.writeDataByUsingNIOSomeBuffer(inputLine("file")));
            put("6", () -> controller.readDataByUsingNIOSomeBuffer(inputLine("file")));
            put("7", () -> {});
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}