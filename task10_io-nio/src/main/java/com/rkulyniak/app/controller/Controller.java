package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.FileReader;
import com.rkulyniak.app.model.FileUtil;
import com.rkulyniak.app.model.IOPerformanceMeter;
import com.rkulyniak.app.model.SomeBuffer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    public void compareUsualAndBufferedIOPerformance(String fileName) {
        try {
            IOPerformanceMeter ioPerformanceMeter = new IOPerformanceMeter(getFile(fileName));
            logger.info("");
            logger.info("Writing - " + ioPerformanceMeter.measureWritingPerformance());
            logger.info("Reading - " + ioPerformanceMeter.measureReadingPerformance());
            logger.info("Writing buff(75 Byte) - " +
                    ioPerformanceMeter.measureBufferedWritingPerformance(75));
            logger.info("Reading buff(75 Byte)- " +
                    ioPerformanceMeter.measureBufferedReadingPerformance(75));
            logger.info("Writing buff(500 Byte) - " +
                    ioPerformanceMeter.measureBufferedWritingPerformance(500));
            logger.info("Reading buff(500 Byte)- " +
                    ioPerformanceMeter.measureBufferedReadingPerformance(500));
        } catch (IOException e) {
            logger.error("unable to compare usual and buffered IO performance; " +
                    "error msg[" + e.getMessage() + "]");
        }
    }

    public void getIntStreamFromFile(String fileName) {
        try {
            IntStream intStream = new FileReader().ints(getFile(fileName));
            logger.info("received int stream from " + fileName + "; " +
                    Arrays.toString(intStream.toArray()));
        } catch (IOException e) {
            logger.error("unable to get int stream from file; " +
                    "error msg[" + e.getMessage() + "]");
        }
    }

    public void getCommentsFromSource(String fileName) {
        try {
            List<String> comments = new FileReader().getCommentsFromSource(getFile(fileName));
            logger.info("received list of comments from " + fileName + " source;" + comments);
        } catch (IOException e) {
            logger.error("unable to get comments from source; " +
                    "error msg[" + e.getMessage() + "]");
        }
    }

    public void getDirectoryTree(String directoryName) {
        new FileUtil().printDirectoryTree(new File(directoryName));
    }

    public void writeDataByUsingNIOSomeBuffer(String fileName) {
        try {
            new SomeBuffer().byteChannelWrite(getFile(fileName), "some data");
            logger.info("'some data' wrote to " + fileName + " by byteChannelWrite");
        } catch (IOException e) {
            logger.error("unable to write data by using NIO SomeBuffer; " +
                    "error msg[" + e.getMessage() + "]");
        }
    }

    public void readDataByUsingNIOSomeBuffer(String fileName) {
        try {
            String data = new SomeBuffer().byteChannelRead(getFile(fileName), 10);
            logger.info("received data=" + data);
        } catch (IOException e) {
            logger.error("unable to read data by using NIO SomeBuffer; " +
                    "error msg[" + e.getMessage() + "]");
        }
    }

    private File getFile(String fileName) throws FileNotFoundException {
        URL url = ClassLoader.getSystemClassLoader().getResource(fileName);
        if (url == null) {
            throw new FileNotFoundException(fileName + " not found");
        }
        return new File(url.getFile());
    }
}
