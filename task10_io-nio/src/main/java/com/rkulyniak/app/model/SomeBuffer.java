package com.rkulyniak.app.model;


import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;

public class SomeBuffer {

    public void byteChannelWrite(File file, String data) throws IOException {
        Set options = new HashSet();
        options.add(StandardOpenOption.CREATE);
        options.add(StandardOpenOption.APPEND);

        SeekableByteChannel byteChannel = Files.newByteChannel(file.toPath(), options);
        byteChannel.write(ByteBuffer.wrap(data.getBytes()));
    }

    public String byteChannelRead(File file, int bufferSize) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        SeekableByteChannel byteChannel = Files.newByteChannel(file.toPath());
        ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);

        Charset charset = Charset.forName("US-ASCII");
        while (byteChannel.read(byteBuffer) > 0) {
            byteBuffer.rewind();
            stringBuilder.append(charset.decode(byteBuffer));
            byteBuffer.flip();
        }
        return stringBuilder.toString();
    }
}
