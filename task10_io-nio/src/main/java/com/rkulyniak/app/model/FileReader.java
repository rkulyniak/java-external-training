package com.rkulyniak.app.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FileReader {

    public IntStream ints(File file) throws IOException {
        IntStream.Builder builder = IntStream.builder();
        Files.lines(Paths.get(file.toURI()))
                .forEach(line -> {
                    try {
                        builder.add(Integer.parseInt(line));
                    } catch (NumberFormatException e) {
                        // skip not number
                    }
                });
        return builder.build();
    }

    public List<String> getCommentsFromSource(File file) throws IOException {
        return Files.lines(Paths.get(file.toURI()))
                .filter(line -> line.startsWith("//"))
                .collect(Collectors.toList());
    }
}
