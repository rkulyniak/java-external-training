package com.rkulyniak.app.model;

import java.io.File;

public class FileUtil {

    public void printDirectoryTree(File file) {
        printTreeRecursively(1, file);
    }

    private void printTreeRecursively(int indent, File file) {
        for (int i = 0; i < indent; i++) {
            System.out.print('-');
        }
        System.out.println(file.getName());
        File[] subFiles = file.listFiles();
        if (subFiles != null) {
            for (File value : subFiles) {
                printTreeRecursively(indent + 4, value);
            }
        }
    }
}
