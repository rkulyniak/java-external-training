package com.rkulyniak.app.model;

import java.io.*;
import java.util.concurrent.TimeUnit;

public class IOPerformanceMeter {

    private File file;

    public IOPerformanceMeter(File file) {
        this.file = file;
    }

    public String measureWritingPerformance() throws IOException {
        long begin = System.nanoTime();
        try (OutputStream outputStream = new FileOutputStream(file, false)) {
            for (int i = 0; i < 100_000; i++) {
                outputStream.write(i);
            }
        }
        long finish = System.nanoTime();
        return TimeUnit.NANOSECONDS.toMillis(finish - begin) + " ms";
    }

    public String measureReadingPerformance() throws IOException {
        long begin = System.nanoTime();
        try (InputStream inputStream = new FileInputStream(file)) {
            while (inputStream.read() != -1) ;
        }
        long finish = System.nanoTime();
        return TimeUnit.NANOSECONDS.toMillis(finish - begin) + " ms";
    }

    public String measureBufferedWritingPerformance(int bufferSize) throws IOException {
        long begin = System.nanoTime();
        try (OutputStream os = new FileOutputStream(file, false);
             BufferedOutputStream output = new BufferedOutputStream(os, bufferSize)) {
            for (int i = 0; i < 100_000; i++) {
                output.write(i);
            }
        }
        long finish = System.nanoTime();
        return TimeUnit.NANOSECONDS.toMillis(finish - begin) + " ms";
    }

    public String measureBufferedReadingPerformance(int bufferSize) throws IOException {
        long begin = System.nanoTime();
        try (InputStream is = new FileInputStream(file);
             BufferedInputStream input = new BufferedInputStream(is, bufferSize)) {
            while (input.read() != -1) ;
        }
        long finish = System.nanoTime();
        return TimeUnit.NANOSECONDS.toMillis(finish - begin) + " ms";
    }
}
