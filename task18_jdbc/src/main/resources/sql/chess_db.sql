create table player
(
    id          int auto_increment,
    first_name  varchar(30) not null,
    last_name   varchar(30) not null,
    nationality varchar(50),
    rating      int default 1200,
    primary key (id)
);

create table sponsor
(
    id                 int auto_increment,
    name               varchar(50),
    finance_assistance decimal(10, 2) default 0.00,
    primary key (id),
    unique key (name)
);

create table tournament
(
    id         int auto_increment,
    name       varchar(100) not null,
    date       date         not null,
    prize_pool decimal(10, 2) default 0.00,
    winner_id  int,
    primary key (id),
    unique key (name, date),
    foreign key (winner_id) references player (id) on update cascade on delete set null
);

create table tournament_sponsor
(
    tournament_id int,
    sponsor_id    int,
    primary key (tournament_id, sponsor_id)
);

create table tournament_stage
(
    id   int auto_increment,
    name varchar(20) not null,
    primary key (id),
    unique key (name)
);

create table battle
(
    id                  int auto_increment,
    first_player_id     int null,
    second_player_id    int null,
    tournament_stage_id int not null,
    tournament_id       int not null,
    primary key (id),
    unique key (first_player_id, second_player_id, tournament_stage_id),
    foreign key (first_player_id) references player (id) on update cascade on delete set null,
    foreign key (second_player_id) references player (id) on update cascade on delete set null,
    foreign key (tournament_stage_id) references tournament_stage (id) on update restrict on delete restrict,
    foreign key (tournament_id) references tournament (id) on update cascade on delete cascade
);

create table battle_result
(
    battle_id           int           not null,
    first_player_score  decimal(1, 1) not null,
    second_player_score decimal(1, 1) not null,
    primary key (battle_id),
    foreign key (battle_id) references battle (id) on update cascade on delete cascade
);