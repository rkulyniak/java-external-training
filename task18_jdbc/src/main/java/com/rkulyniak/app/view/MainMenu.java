package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;
import com.rkulyniak.app.model.entity.Player;
import com.rkulyniak.app.model.entity.Sponsor;
import com.rkulyniak.app.model.entity.Tournament;
import lombok.extern.slf4j.Slf4j;

import java.sql.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.rkulyniak.app.view.UserInterface.*;

@Slf4j
public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - read connection metadata");
            put("2", "2 - read SQL query metadata");
            put("3", "3 - getAll tournaments");
            put("4", "4 - get tournament by id");
            put("5", "5 - save tournament");
            put("6", "6 - delete tournament");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new HashMap<>() {{
            put("1", () -> controller.logConnectionMetadata());
            put("2", () -> controller.logSqlQueryMetadata(inputLine("SQL query")));
            put("3", () -> controller.logGettingAllTournaments());
            put("4", () -> controller.logGettingTournamentById(inputInteger("id")));
            put("5", () -> controller.logSavingTournament(Tournament.builder()
                    .name(inputLine("tournament name"))
                    .date(Date.valueOf(inputLine("tournament date")))
                    .prizePool(inputDouble("tournament prize pool"))
                    .sponsor(Sponsor.builder()
                            .name(inputLine("sponsor name"))
                            .financeAssistance(inputDouble("sponsor finance assistance"))
                            .build())
                    .winner(Player.builder()
                            .firstName(inputLine("player first name"))
                            .lastName(inputLine("player last name"))
                            .nationality(inputLine("nationality"))
                            .rating(inputDouble("player rating"))
                            .build())
                    .build()));
            put("6", () -> controller.logDeletingTournament(inputInteger("id")));
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
