package com.rkulyniak.app.view;

import java.util.Scanner;

class UserInterface {

    private static Scanner input = new Scanner(System.in);

    static String inputLine(String text) {
        System.out.print(text + ": ");
        return input.nextLine();
    }

    static int inputInteger(String text) {
        while (true) {
            try {
                System.out.print(text + ": ");
                return Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
            }
        }
    }

    static double inputDouble(String text) {
        while (true) {
            try {
                System.out.print(text + ": ");
                return Float.parseFloat(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
            }
        }
    }
}