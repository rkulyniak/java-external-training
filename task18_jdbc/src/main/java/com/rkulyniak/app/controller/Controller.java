package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.MetadataReader;
import com.rkulyniak.app.model.dao.TournamentDao;
import com.rkulyniak.app.model.dao.TournamentDaoImpl;
import com.rkulyniak.app.model.entity.Tournament;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Controller {

    private MetadataReader metadataReader;
    private TournamentDao tournamentDao;

    public Controller() {
        metadataReader = new MetadataReader();
        tournamentDao = new TournamentDaoImpl();
    }

    public void logConnectionMetadata() {
        metadataReader.logConnectionMetadata();
    }

    public void logSqlQueryMetadata(String sqlQuery) {
        metadataReader.logSqlQueryMetaData(sqlQuery);
    }

    public void logGettingAllTournaments() {
        log.info("tournaments: " + tournamentDao.getAll().toString());
    }

    public void logGettingTournamentById(int id) {
        log.info("tournament [id-" + id + "]: " + tournamentDao.get(id));
    }

    public void logSavingTournament(Tournament tournament) {
        tournamentDao.save(tournament);
    }

    public void logDeletingTournament(int id) {
        tournamentDao.delete(id);
    }
}
