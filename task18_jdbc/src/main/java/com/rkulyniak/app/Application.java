package com.rkulyniak.app;

import com.rkulyniak.app.controller.Controller;
import com.rkulyniak.app.view.MainMenu;
import com.rkulyniak.app.view.Menu;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application {

    public static void main(String[] args) {
        Controller controller = new Controller();
        Menu mainMenu = new MainMenu(controller);

        log.debug("the application has started execution");
        mainMenu.launch();
        log.debug("the application has finished execution");
    }
}
