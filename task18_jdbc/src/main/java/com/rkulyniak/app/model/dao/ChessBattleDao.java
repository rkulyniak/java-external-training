package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.entity.ChessBattle;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public interface ChessBattleDao extends GenericDao<ChessBattle> {

    @Override
    List<ChessBattle> getAll();

    @Override
    Optional<ChessBattle> get(@NonNull Integer id);

    @Override
    void save(@NonNull ChessBattle battle);

    @Override
    void update(@NonNull ChessBattle battle);

    @Override
    void delete(@NonNull Integer id);
}
