package com.rkulyniak.app.model.dao;

import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public interface GenericDao<T> {

    List<T> getAll();

    Optional<T> get(@NonNull Integer id);

    void save(@NonNull T entity);

    void update(@NonNull T entity);

    void delete(@NonNull Integer id);
}
