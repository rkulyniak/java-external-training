package com.rkulyniak.app.model.exception;

public class DataIntegrityException extends RuntimeException {

    public DataIntegrityException() {
        super();
    }

    public DataIntegrityException(String message) {
        super(message);
    }
}
