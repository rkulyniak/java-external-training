package com.rkulyniak.app.model.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Sponsor {

    private Integer id;
    private String name;
    @Builder.Default
    private Double financeAssistance = 0.00;
}
