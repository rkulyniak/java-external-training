package com.rkulyniak.app.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;

import java.sql.Date;
import java.util.List;

@Data
@Builder
public class Tournament {

    private Integer id;
    @NonNull
    private String name;
    @NonNull
    private Date date;
    @Builder.Default
    private Double prizePool = 0.00;
    private Player winner;
    @Singular
    private List<Sponsor> sponsors;
}
