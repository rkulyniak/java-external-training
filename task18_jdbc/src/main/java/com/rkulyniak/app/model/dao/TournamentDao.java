package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.entity.Tournament;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public interface TournamentDao extends GenericDao<Tournament> {

    @Override
    List<Tournament> getAll();

    @Override
    Optional<Tournament> get(@NonNull Integer id);

    @Override
    void save(@NonNull Tournament tournament);

    @Override
    void update(@NonNull Tournament tournament);

    @Override
    void delete(@NonNull Integer id);
}
