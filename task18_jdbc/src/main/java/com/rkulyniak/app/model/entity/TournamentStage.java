package com.rkulyniak.app.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class TournamentStage {

    private Integer id;
    @NonNull
    private String name;
}
