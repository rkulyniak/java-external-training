package com.rkulyniak.app.model;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
public class DBConnectionProvider {

    private static final String URL = "jdbc:mysql://localhost:3306/chess";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";

    public static boolean tryConnect() {
        try {
            DriverManager.getConnection(URL, USERNAME, PASSWORD);
            log.info("connection was successfully established with DB;");
            return true;
        } catch (SQLException e) {
            log.error("unable to connect to the DB; error msg: " + e.getMessage());
            return false;
        }
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }

    public static Connection getDisableAutocomitConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        connection.setAutoCommit(false);
        return connection;
    }

    public static Connection getReadOnlyConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        connection.setReadOnly(true);
        return connection;
    }
}
