package com.rkulyniak.app.model.transformer;

import com.rkulyniak.app.model.dao.*;
import com.rkulyniak.app.model.entity.ChessBattle;
import com.rkulyniak.app.model.exception.DataIntegrityException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ChessBattleTransformer implements Transformer<ChessBattle> {

    private final int idColumnIndex = 1;
    private final int firstPlayerColumnIndex = 2;
    private final int secondPlayerColumnIndex = 3;
    private final int tournamentStageColumnIndex = 4;
    private final int tournamentColumnIndex = 5;
    private final int firstPlayerScoreColumnIndex = 6;
    private final int secondPlayerScoreColumnIndex = 7;

    private PlayerDao playerDao;
    private TournamentStageDao tournamentStageDao;
    private TournamentDao tournamentDao;

    public ChessBattleTransformer() {
        this(new PlayerDaoImpl(), new TournamentStageDaoImpl(), new TournamentDaoImpl());
    }

    public ChessBattleTransformer(PlayerDao playerDao,
                                  TournamentStageDao tournamentStageDao, TournamentDao tournamentDao) {
        this.playerDao = playerDao;
        this.tournamentStageDao = tournamentStageDao;
        this.tournamentDao = tournamentDao;
    }

    @Override
    public ChessBattle transformEntity(ResultSet resultSet) throws SQLException {
        return ChessBattle.builder()
                .id(resultSet.getInt(idColumnIndex))
                .firstPlayer(ChessBattle.PlayerScore.builder()
                        .player(playerDao.get(resultSet.getInt(firstPlayerColumnIndex))
                                .orElseThrow(DataIntegrityException::new))
                        .score(resultSet.getDouble(firstPlayerScoreColumnIndex))
                        .build())
                .secondPlayer(ChessBattle.PlayerScore.builder()
                        .player(playerDao.get(resultSet.getInt(secondPlayerColumnIndex))
                                .orElseThrow(DataIntegrityException::new))
                        .score(resultSet.getDouble(secondPlayerScoreColumnIndex))
                        .build())
                .stage(tournamentStageDao.get(resultSet.getInt(tournamentStageColumnIndex))
                        .orElseThrow(DataIntegrityException::new))
                .tournament(tournamentDao.get(resultSet.getInt(tournamentColumnIndex))
                        .orElseThrow(DataIntegrityException::new))
                .build();
    }
}
