package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.PersistenceData;
import com.rkulyniak.app.model.entity.ChessBattle;
import com.rkulyniak.app.model.entity.Player;
import com.rkulyniak.app.model.entity.Tournament;
import com.rkulyniak.app.model.entity.TournamentStage;
import com.rkulyniak.app.model.exception.ConfigPreparedStatmentException;
import com.rkulyniak.app.model.exception.DataIntegrityException;
import com.rkulyniak.app.model.transformer.ChessBattleTransformer;
import com.rkulyniak.app.model.transformer.Transformer;
import lombok.NonNull;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ChessBattleDaoImpl implements ChessBattleDao {

    private static final String GET_LIST_OF_CHESS_BATTLES_QUERY =
            "select id, first_player_id, second_player_id, tournament_stage_id, tournament_id, " +
                    "   first_player_score, second_player_score\n" +
                    "from battle\n" +
                    "         left join battle_result br on battle.id = br.battle_id;";
    private static final String GET_BATTLE_BY_ID_QUERY =
            "select id, first_player_id, second_player_id, tournament_stage_id, tournament_id, " +
                    "   first_player_score, second_player_score\n" +
                    "from battle\n" +
                    "         left join battle_result br on battle.id = br.battle_id\n" +
                    "where battle.id = ?;";
    private static final String INSERT_BATTLE_QUERY =
            "insert into battle (first_player_id, second_player_id, tournament_stage_id, tournament_id)\n" +
                    "values (?, ?, ?, ?);";
    private static final String INSERT_BATTLE_RESULT_QUERY =
            "insert into battle_result (battle_id, first_player_score, second_player_score)\n" +
                    "values (?, ?, ?);";
    private static final String UPDATE_BATTLE_QUERY =
            "update battle\n" +
                    "set first_player_id = ?, second_player_id = ?, tournament_stage_id = ?, tournament_id = ?\n" +
                    "where id = ?;";
    private static final String UPDATE_BATTLE_RESULT_QUERY =
            "update battle_result\n" +
                    "set first_player_score = ?, second_player_score = ?\n" +
                    "where battle_id = ?;";
    private static final String DELETE_BATTLE_QUERY =
            "delete from battle where id = ?;";

    private PersistenceData<ChessBattle> chessBattlePersistence;

    public ChessBattleDaoImpl() {
        this(new ChessBattleTransformer());
    }

    public ChessBattleDaoImpl(Transformer<ChessBattle> chessBattleTransformer) {
        chessBattlePersistence = new PersistenceData<>(chessBattleTransformer);
    }

    @Override
    public List<ChessBattle> getAll() {
        return chessBattlePersistence.getList(GET_LIST_OF_CHESS_BATTLES_QUERY, statement -> {});
    }

    @Override
    public Optional<ChessBattle> get(@NonNull Integer id) {
        ChessBattle battle = chessBattlePersistence.get(GET_BATTLE_BY_ID_QUERY,
                statement -> setArgsForGettingByIdStatement(statement, id));
        return Optional.ofNullable(battle);
    }

    private void setArgsForGettingByIdStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void save(@NonNull ChessBattle battle) {
        checkPlayerDataIntegrity(battle.getFirstPlayer().getPlayer());
        checkPlayerDataIntegrity(battle.getSecondPlayer().getPlayer());
        checkTournamentStageDataIntegrity(battle.getStage());
        checkTournamentDataIntegrity(battle.getTournament());
        Integer generatedId = chessBattlePersistence.save(INSERT_BATTLE_QUERY,
                statement -> setArgsForSavingBattleStatement(statement, battle));
        battle.setId(generatedId);
        chessBattlePersistence.save(INSERT_BATTLE_RESULT_QUERY,
                statement -> setArgsForSavingBattleResultStatement(statement, battle));
    }

    private void setArgsForSavingBattleStatement(PreparedStatement statement, ChessBattle battle) {
        try {
            statement.setInt(1, battle.getFirstPlayer().getPlayer().getId());
            statement.setInt(2, battle.getSecondPlayer().getPlayer().getId());
            statement.setInt(3, battle.getStage().getId());
            statement.setInt(4, battle.getTournament().getId());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    private void setArgsForSavingBattleResultStatement(PreparedStatement statement, ChessBattle battle) {
        try {
            statement.setInt(1, battle.getId());
            statement.setDouble(2, battle.getFirstPlayer().getScore());
            statement.setDouble(3, battle.getSecondPlayer().getScore());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void update(@NonNull ChessBattle battle) {
        chessBattlePersistence.update(UPDATE_BATTLE_QUERY,
                statement -> setArgsForUpdatingBattleStatement(statement, battle));
        chessBattlePersistence.update(UPDATE_BATTLE_RESULT_QUERY,
                statement -> setArgsForUpdatingBattleResultStatement(statement, battle));
    }

    private void setArgsForUpdatingBattleStatement(PreparedStatement statement, ChessBattle battle) {
        try {
            setArgsForSavingBattleStatement(statement, battle);
            statement.setInt(5, battle.getId());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    private void setArgsForUpdatingBattleResultStatement(PreparedStatement statement, ChessBattle battle) {
        try {
            statement.setDouble(1, battle.getFirstPlayer().getScore());
            statement.setDouble(2, battle.getSecondPlayer().getScore());
            statement.setInt(3, battle.getId());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void delete(@NonNull Integer id) {
        chessBattlePersistence.delete(DELETE_BATTLE_QUERY,
                statement -> setArgsForDeletingBattleStatement(statement, id));
    }

    private void setArgsForDeletingBattleStatement(PreparedStatement statement, Integer battleId) {
        try {
            statement.setInt(1, battleId);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    private void checkPlayerDataIntegrity(Player player) {
        if (player == null) {
            return;
        }
        checkPlayerDataIntegrity(player.getId());

        PlayerDao playerDao = new PlayerDaoImpl();
        playerDao.get(player.getId())
                .orElseThrow(() -> new DataIntegrityException("" +
                        "player {id:" + player.getId() + "} doesn't exist in DB"));
    }

    private void checkPlayerDataIntegrity(Integer playerId) {
        if (playerId == null) {
            throw new DataIntegrityException("invalid player {id:null}");
        }
    }

    private void checkTournamentStageDataIntegrity(TournamentStage tournamentStage) {
        if (tournamentStage == null) {
            throw new DataIntegrityException("invalid tournamentStage {tournamentStage:null}");
        }
        checkTournamentStageDataIntegrity(tournamentStage.getId());

        TournamentStageDao tournamentStageDao = new TournamentStageDaoImpl();
        tournamentStageDao.get(tournamentStage.getId())
                .orElseThrow(() -> new DataIntegrityException("" +
                        "tournamentStage {id:" + tournamentStage.getId() + "} doesn't exist in DB"));
    }

    private void checkTournamentStageDataIntegrity(Integer tournamentStageId) {
        if (tournamentStageId == null) {
            throw new DataIntegrityException("invalid tournamentStage {id:null}");
        }
    }

    private void checkTournamentDataIntegrity(Tournament tournament) {
        if (tournament == null) {
            throw new DataIntegrityException("invalid tournament {tournament:null}");
        }
        checkTournamentDataIntegrity(tournament.getId());

        TournamentDao tournamentDao = new TournamentDaoImpl();
        tournamentDao.get(tournament.getId())
                .orElseThrow(() -> new DataIntegrityException("" +
                        "tournament {id:" + tournament.getId() + "} doesn't exist in DB"));
    }

    private void checkTournamentDataIntegrity(Integer tournamentId) {
        if (tournamentId == null) {
            throw new DataIntegrityException("invalid tournament {id:null}");
        }
    }
}
