package com.rkulyniak.app.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class ChessBattle {

    private Integer id;
    private PlayerScore firstPlayer;
    private PlayerScore secondPlayer;
    @NonNull
    private TournamentStage stage;
    @NonNull
    private Tournament tournament;

    @Data
    @Builder
    public static class PlayerScore {

        private Player player;
        @NonNull
        private Double score;
    }
}
