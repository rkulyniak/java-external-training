package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.entity.Player;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public interface PlayerDao extends GenericDao<Player> {

    @Override
    List<Player> getAll();

    @Override
    Optional<Player> get(@NonNull Integer id);

    @Override
    void save(@NonNull Player player);

    @Override
    void update(@NonNull Player player);

    @Override
    void delete(@NonNull Integer id);
}
