package com.rkulyniak.app.model.transformer;

import com.rkulyniak.app.model.entity.Sponsor;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SponsorTransformer implements Transformer<Sponsor> {

    private final int idColumnIndex = 1;
    private final int nameColumnIndex = 2;
    private final int financeAssistanceColumnIndex = 3;


    @Override
    public Sponsor transformEntity(@NonNull ResultSet resultSet) throws SQLException {
        return Sponsor.builder()
                .id(resultSet.getInt(idColumnIndex))
                .name(resultSet.getString(nameColumnIndex))
                .financeAssistance(resultSet.getDouble(financeAssistanceColumnIndex))
                .build();
    }
}
