package com.rkulyniak.app.model.transformer;

import com.rkulyniak.app.model.dao.PlayerDaoImpl;
import com.rkulyniak.app.model.dao.SponsorDao;
import com.rkulyniak.app.model.dao.SponsorDaoImpl;
import com.rkulyniak.app.model.entity.Player;
import com.rkulyniak.app.model.entity.Tournament;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TournamentTransformer implements Transformer<Tournament> {

    private final int idColumnIndex = 1;
    private final int nameColumnIndex = 2;
    private final int dateColumnIndex = 3;
    private final int prizePoolColumnIndex = 4;
    private final int winnerIdColumnIndex = 5;

    private final SponsorDao sponsorDao;

    public TournamentTransformer() {
        sponsorDao = new SponsorDaoImpl();
    }

    public TournamentTransformer(SponsorDao sponsorDao) {
        this.sponsorDao = sponsorDao;
    }

    @Override
    public Tournament transformEntity(@NonNull ResultSet resultSet) throws SQLException {
        Player winner = new PlayerDaoImpl()
                .get(resultSet.getInt(winnerIdColumnIndex))
                .orElse(null);
        return Tournament.builder()
                .id(resultSet.getInt(idColumnIndex))
                .name(resultSet.getString(nameColumnIndex))
                .date(resultSet.getDate(dateColumnIndex))
                .prizePool(resultSet.getDouble(prizePoolColumnIndex))
                .winner(winner)
                .sponsors(sponsorDao.getAll(resultSet.getInt(idColumnIndex)))
                .build();
    }
}
