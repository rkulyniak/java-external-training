package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.PersistenceData;
import com.rkulyniak.app.model.entity.Player;
import com.rkulyniak.app.model.exception.ConfigPreparedStatmentException;
import com.rkulyniak.app.model.transformer.PlayerTransformer;
import com.rkulyniak.app.model.transformer.Transformer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Slf4j
public class PlayerDaoImpl implements PlayerDao {

    private static final String GET_LIST_OF_PLAYERS_QUERY =
            "select id, first_name, last_name, nationality, rating from player;";
    private static final String GET_PLAYER_BY_ID_QUERY =
            "select id, first_name, last_name, nationality, rating from player where id = ?;";
    private static final String INSERT_PLAYER_QUERY =
            "insert into player (first_name, last_name, nationality, rating) values (?, ?, ?, ?);";
    private static final String UPDATE_PLAYER_QUERY =
            "update player set first_name = ?, last_name = ?, nationality = ?, rating = ? where id = ?;";
    private static final String DELETE_PLAYER_QUERY =
            "delete from player where id = ?;";

    private PersistenceData<Player> playerPersistence;

    public PlayerDaoImpl() {
        this(new PlayerTransformer());
    }

    public PlayerDaoImpl(Transformer<Player> playerTransformer) {
        playerPersistence = new PersistenceData<>(playerTransformer);
    }

    @Override
    public List<Player> getAll() {
        return playerPersistence.getList(GET_LIST_OF_PLAYERS_QUERY, statement -> {});
    }

    @Override
    public Optional<Player> get(@NonNull Integer id) {
        Player player = playerPersistence.get(GET_PLAYER_BY_ID_QUERY,
                statement -> setArgsForGettingByIdStatement(statement, id));
        return Optional.ofNullable(player);
    }

    private void setArgsForGettingByIdStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void save(@NonNull Player player) {
        Integer generatedId = playerPersistence.save(INSERT_PLAYER_QUERY,
                statement -> setArgsForSavingStatement(statement, player));
        player.setId(generatedId);
    }

    private void setArgsForSavingStatement(PreparedStatement statement, Player player) {
        try {
            statement.setString(1, player.getFirstName());
            statement.setString(2, player.getLastName());
            statement.setString(3, player.getNationality());
            statement.setDouble(4, player.getRating());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void update(@NonNull Player player) {
        playerPersistence.update(UPDATE_PLAYER_QUERY,
                statement -> setArgsForUpdatingStatement(statement, player));
    }

    private void setArgsForUpdatingStatement(PreparedStatement statement, Player player) {
        try {
            statement.setString(1, player.getFirstName());
            statement.setString(2, player.getLastName());
            statement.setString(3, player.getNationality());
            statement.setDouble(4, player.getRating());
            statement.setInt(5, player.getId());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void delete(@NonNull Integer id) {
        playerPersistence.delete(DELETE_PLAYER_QUERY,
                statement -> setArgsForDeletingStatement(statement, id));
    }

    private void setArgsForDeletingStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }
}
