package com.rkulyniak.app.model.transformer;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Transformer<T> {

    T transformEntity(ResultSet resultSet) throws SQLException;
}
