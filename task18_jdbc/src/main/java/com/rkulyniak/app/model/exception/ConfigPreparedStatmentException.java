package com.rkulyniak.app.model.exception;

public class ConfigPreparedStatmentException extends RuntimeException {

    public ConfigPreparedStatmentException() {
    }

    public ConfigPreparedStatmentException(String message) {
        super(message);
    }
}
