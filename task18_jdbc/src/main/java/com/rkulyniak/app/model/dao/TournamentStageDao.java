package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.entity.TournamentStage;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public interface TournamentStageDao extends GenericDao<TournamentStage> {

    @Override
    List<TournamentStage> getAll();

    @Override
    Optional<TournamentStage> get(@NonNull Integer id);

    @Override
    void save(@NonNull TournamentStage entity);

    @Override
    void update(@NonNull TournamentStage entity);

    @Override
    void delete(@NonNull Integer id);
}
