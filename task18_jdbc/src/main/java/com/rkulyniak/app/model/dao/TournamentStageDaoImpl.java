package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.PersistenceData;
import com.rkulyniak.app.model.entity.TournamentStage;
import com.rkulyniak.app.model.exception.ConfigPreparedStatmentException;
import com.rkulyniak.app.model.transformer.TournamentStageTransformer;
import com.rkulyniak.app.model.transformer.Transformer;
import lombok.NonNull;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class TournamentStageDaoImpl implements TournamentStageDao {

    private static final String GET_LIST_OF_TOURNAMENT_STAGES_QUERY =
            "select id, name from tournament_stage;";
    private static final String GET_TOURNAMENT_STAGE_BY_ID_QUERY =
            "select id, name from tournament_stage where id = ?;";
    private static final String INSERT_TOURNAMENT_STAGE_QUERY =
            "insert into tournament_stage (name) values (?);";
    private static final String UPDATE_TOURNAMENT_STAGE_QUERY =
            "update tournament_stage set name = ? where id = ?;";
    private static final String DELETE_TOURNAMENT_STAGE_QUERY =
            "delete from tournament_stage where id = ?;";


    private PersistenceData<TournamentStage> tournamentStagePersistence;

    public TournamentStageDaoImpl() {
        this(new TournamentStageTransformer());
    }

    public TournamentStageDaoImpl(Transformer<TournamentStage> tournamentStageTransformer) {
        tournamentStagePersistence = new PersistenceData<>(tournamentStageTransformer);
    }

    @Override
    public List<TournamentStage> getAll() {
        return tournamentStagePersistence.getList(GET_LIST_OF_TOURNAMENT_STAGES_QUERY, statement -> {});
    }

    @Override
    public Optional<TournamentStage> get(@NonNull Integer id) {
        TournamentStage tournamentStage = tournamentStagePersistence.get(GET_TOURNAMENT_STAGE_BY_ID_QUERY,
                statement -> setArgsForGettingByIdStatement(statement, id));
        return Optional.ofNullable(tournamentStage);
    }

    private void setArgsForGettingByIdStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void save(@NonNull TournamentStage tournamentStage) {
        Integer generatedId = tournamentStagePersistence.save(INSERT_TOURNAMENT_STAGE_QUERY,
                statement -> setArgsForSavingStatement(statement, tournamentStage));
        tournamentStage.setId(generatedId);
    }

    private void setArgsForSavingStatement(PreparedStatement statement, TournamentStage tournamentStage) {
        try {
            statement.setString(1, tournamentStage.getName());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void update(@NonNull TournamentStage tournamentStage) {
        tournamentStagePersistence.update(UPDATE_TOURNAMENT_STAGE_QUERY,
                statement -> setArgsForUpdatingStatement(statement, tournamentStage));
    }

    private void setArgsForUpdatingStatement(PreparedStatement statement, TournamentStage tournamentStage) {
        try {
            statement.setString(1, tournamentStage.getName());
            statement.setInt(2, tournamentStage.getId());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void delete(@NonNull Integer id) {
        tournamentStagePersistence.delete(DELETE_TOURNAMENT_STAGE_QUERY,
                statement -> setArgsForDeletingStatement(statement, id));
    }

    private void setArgsForDeletingStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }
}
