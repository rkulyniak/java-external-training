package com.rkulyniak.app.model;

import com.rkulyniak.app.model.transformer.Transformer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
public class PersistenceData<T> {

    private static final String GET_LATEST_INSERTED_ID_QUERY = "select LAST_INSERT_ID();";

    @NonNull
    private Transformer<T> transformer;

    public PersistenceData(@NonNull Transformer<T> transformer) {
        this.transformer = transformer;
    }

    public List<T> getList(String sqlQuery, Consumer<PreparedStatement> preparedStatementConfig) {
        List<T> listOfEntities = new ArrayList<>();
        try (Connection connection = DBConnectionProvider.getReadOnlyConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatementConfig.accept(preparedStatement);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                T entity = transformer.transformEntity(resultSet);
                listOfEntities.add(entity);
            }
        } catch (SQLException e) {
            log.error("error has been occurred while executing query [" + sqlQuery + "]; " +
                    "error msg: " + e.getMessage());
        }
        return listOfEntities;
    }

    public T get(String sqlQuery, Consumer<PreparedStatement> preparedStatementConfig) {
        T entity = null;
        try (Connection connection = DBConnectionProvider.getReadOnlyConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatementConfig.accept(preparedStatement);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                entity = transformer.transformEntity(resultSet);
            }
        } catch (SQLException e) {
            log.error("error has been occurred while executing query [" + sqlQuery + "]; " +
                    "error msg: " + e.getMessage());
        }
        return entity;
    }

    public Integer save(String sqlQuery, Consumer<PreparedStatement> preparedStatementConfig) {
        Connection connection = null;
        Integer generatedId = null;
        try {
            try {
                connection = DBConnectionProvider.getDisableAutocomitConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
                preparedStatementConfig.accept(preparedStatement);

                int modifiedRowCount = preparedStatement.executeUpdate();
                if (modifiedRowCount == 1) {
                    generatedId = getLatestInsertedId(connection);
                    connection.commit();
                } else {
                    connection.rollback();
                }
            } catch (SQLException e) {
                if (connection != null) {
                    connection.rollback();
                }
                log.error("error has been occurred while executing query [" + sqlQuery + "]; " +
                        "error msg: " + e.getMessage());
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return generatedId;
    }

    private Integer getLatestInsertedId(Connection connection) {
        Integer latestInsertedId = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_LATEST_INSERTED_ID_QUERY);
            if (resultSet.next()) {
                latestInsertedId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            log.error("error has been occurred while getting latest inserted id; " +
                    "error msg: " + e.getMessage());
        }
        return latestInsertedId;
    }

    public int update(String sqlQuery, Consumer<PreparedStatement> preparedStatementConfig) {
        Connection connection = null;
        int modifiedRowCount = 0;
        try {
            try {
                connection = DBConnectionProvider.getDisableAutocomitConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
                preparedStatementConfig.accept(preparedStatement);

                modifiedRowCount = preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                if (connection != null) {
                    connection.rollback();
                }
                log.error("error has been occurred while executing query [" + sqlQuery + "]; " +
                        "error msg: " + e.getMessage());
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return modifiedRowCount;
    }

    public int delete(String sqlQuery, Consumer<PreparedStatement> preparedStatementConfig) {
        return update(sqlQuery, preparedStatementConfig);
    }
}
