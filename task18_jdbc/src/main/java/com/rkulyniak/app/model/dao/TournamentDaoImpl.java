package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.PersistenceData;
import com.rkulyniak.app.model.entity.Player;
import com.rkulyniak.app.model.entity.Sponsor;
import com.rkulyniak.app.model.entity.Tournament;
import com.rkulyniak.app.model.exception.ConfigPreparedStatmentException;
import com.rkulyniak.app.model.exception.DataIntegrityException;
import com.rkulyniak.app.model.transformer.TournamentTransformer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Optional;

@Slf4j
public class TournamentDaoImpl implements TournamentDao {

    private static final String GET_LIST_OF_TOURNAMENTS_QUERY =
            "select id, name, date, prize_pool, winner_id from tournament;";
    private static final String GET_TOURNAMENT_BY_ID_QUERY =
            "select id, name, date, prize_pool, winner_id from tournament where id = ?;";
    private static final String INSERT_TOURNAMENT_QUERY =
            "insert into tournament (name, date, prize_pool, winner_id) values (?, ?, ?, ?);";
    private static final String UPDATE_TOURNAMENT_QUERY =
            "update tournament set name = ?, date = ?, prize_pool = ?, winner_id = ? where id = ?;";
    private static final String DELETE_TOURNAMENT_QUERY =
            "delete from tournament where id = ?;";

    private PersistenceData<Tournament> tournamentPersistence;

    public TournamentDaoImpl() {
        this(new TournamentTransformer());
    }

    public TournamentDaoImpl(TournamentTransformer tournamentTransformer) {
        tournamentPersistence = new PersistenceData<>(tournamentTransformer);
    }

    @Override
    public List<Tournament> getAll() {
        return tournamentPersistence.getList(GET_LIST_OF_TOURNAMENTS_QUERY, statement -> {});
    }

    @Override
    public Optional<Tournament> get(@NonNull Integer id) {
        Tournament tournament = tournamentPersistence.get(GET_TOURNAMENT_BY_ID_QUERY,
                statement -> setArgsForGettingByIdStatement(statement, id));
        return Optional.ofNullable(tournament);
    }

    private void setArgsForGettingByIdStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void save(@NonNull Tournament tournament) {
        checkWinnerDataIntegrity(tournament.getWinner());
        checkSponsorDataIntegrity(tournament.getSponsors());
        Integer generatedId = tournamentPersistence.save(INSERT_TOURNAMENT_QUERY,
                statement -> setArgsForSavingStatement(statement, tournament));
        tournament.setId(generatedId);
    }

    private void setArgsForSavingStatement(PreparedStatement statement, Tournament tournament) {
        try {
            statement.setString(1, tournament.getName());
            statement.setDate(2, tournament.getDate());
            statement.setDouble(3, tournament.getPrizePool());
            if (tournament.getWinner() != null &&
                    tournament.getWinner().getId() != null) {
                statement.setInt(4, tournament.getWinner().getId());
            } else {
                statement.setNull(4, Types.INTEGER);
            }
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void update(@NonNull Tournament tournament) {
        checkWinnerDataIntegrity(tournament.getWinner());
        checkSponsorDataIntegrity(tournament.getSponsors());
        tournamentPersistence.update(UPDATE_TOURNAMENT_QUERY,
                statement -> setArgsForUpdatingStatement(statement, tournament));
    }

    private void setArgsForUpdatingStatement(PreparedStatement statement, Tournament tournament) {
        try {
            setArgsForSavingStatement(statement, tournament);
            statement.setInt(5, tournament.getId());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void delete(@NonNull Integer id) {
        checkWinnerDataIntegrity(id);
        checkSponsorDataIntegrity(id);
        tournamentPersistence.delete(DELETE_TOURNAMENT_QUERY,
                statement -> setArgsForDeletingStatement(statement, id));
    }

    private void setArgsForDeletingStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    private void checkWinnerDataIntegrity(Player winner) {
        if (winner == null) {
            return;
        }
        checkWinnerDataIntegrity(winner.getId());

        PlayerDao playerDao = new PlayerDaoImpl();
        playerDao.get(winner.getId())
                .orElseThrow(() -> new DataIntegrityException("" +
                        "player {id:" + winner.getId() + "} doesn't exist in DB"));
    }

    private void checkWinnerDataIntegrity(Integer winnerId) {
        if (winnerId == null) {
            throw new DataIntegrityException("invalid player {id:null}");
        }
    }

    private void checkSponsorDataIntegrity(List<Sponsor> sponsors) {
        if (sponsors == null || sponsors.isEmpty()) {
            return;
        }
        sponsors.forEach(sponsor -> checkSponsorDataIntegrity(sponsor.getId()));

        SponsorDao sponsorDao = new SponsorDaoImpl();
        for (Sponsor sponsor : sponsors) {
            sponsorDao.get(sponsor.getId())
                    .orElseThrow(() -> new DataIntegrityException("" +
                            "sponsor {id:" + sponsor.getId() + "} doesn't exist in DB"));
        }
    }

    private void checkSponsorDataIntegrity(Integer sponsorId) {
        if (sponsorId == null) {
            throw new DataIntegrityException("invalid sponsor id");
        }
    }
}
