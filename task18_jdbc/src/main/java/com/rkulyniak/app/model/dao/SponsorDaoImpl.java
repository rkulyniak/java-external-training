package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.PersistenceData;
import com.rkulyniak.app.model.entity.Sponsor;
import com.rkulyniak.app.model.exception.ConfigPreparedStatmentException;
import com.rkulyniak.app.model.transformer.SponsorTransformer;
import com.rkulyniak.app.model.transformer.Transformer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Slf4j
public class SponsorDaoImpl implements SponsorDao {

    private static final String GET_LIST_OF_SPONSORS_QUERY =
            "select id, name, finance_assistance from sponsor;";
    private static final String GET_LIST_OF_SPONSORS_BY_TOURNAMENT_ID =
            "select id, name, finance_assistance from sponsor where sponsor.id IN (" +
                    "select sponsor_id from tournament_sponsor where tournament_id = ?);";
    private static final String GET_SPONSOR_BY_ID_QUERY =
            "select id, name, finance_assistance from sponsor where id = ?;";
    private static final String INSERT_SPONSOR_QUERY =
            "insert into sponsor (name, finance_assistance) values (?, ?);";
    private static final String UPDATE_SPONSOR_QUERY =
            "update sponsor set name = ?, finance_assistance = ? where id = ?;";
    private static final String DELETE_SPONSOR_QUERY =
            "delete from sponsor where id = ?;";

    private PersistenceData<Sponsor> sponsorPersistence;

    public SponsorDaoImpl() {
        this(new SponsorTransformer());
    }

    public SponsorDaoImpl(Transformer<Sponsor> sponsorTransformer) {
        sponsorPersistence = new PersistenceData<>(sponsorTransformer);
    }

    @Override
    public List<Sponsor> getAll() {
        return sponsorPersistence.getList(GET_LIST_OF_SPONSORS_QUERY, statement -> {});
    }

    @Override
    public List<Sponsor> getAll(@NonNull Integer tournamentId) {
        return sponsorPersistence.getList(GET_LIST_OF_SPONSORS_BY_TOURNAMENT_ID,
                statement -> setArgsForGettingSponsorsByTournamentIdStatement(statement, tournamentId));
    }

    private void setArgsForGettingSponsorsByTournamentIdStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public Optional<Sponsor> get(@NonNull Integer id) {
        Sponsor sponsor = sponsorPersistence.get(GET_SPONSOR_BY_ID_QUERY,
                statement -> setArgsForGettingByIdStatement(statement, id));
        return Optional.ofNullable(sponsor);
    }

    private void setArgsForGettingByIdStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void save(@NonNull Sponsor sponsor) {
        Integer generatedId = sponsorPersistence.save(INSERT_SPONSOR_QUERY,
                statement -> setArgsForSavingStatement(statement, sponsor));
        sponsor.setId(generatedId);
    }

    private void setArgsForSavingStatement(PreparedStatement statement, Sponsor sponsor) {
        try {
            statement.setString(1, sponsor.getName());
            statement.setDouble(2, sponsor.getFinanceAssistance());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void update(@NonNull Sponsor sponsor) {
        sponsorPersistence.update(UPDATE_SPONSOR_QUERY,
                statement -> setArgsForUpdatingStatement(statement, sponsor));
    }

    private void setArgsForUpdatingStatement(PreparedStatement statement, Sponsor sponsor) {
        try {
            statement.setString(1, sponsor.getName());
            statement.setDouble(2, sponsor.getFinanceAssistance());
            statement.setInt(3, sponsor.getId());
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }

    @Override
    public void delete(@NonNull Integer id) {
        sponsorPersistence.delete(DELETE_SPONSOR_QUERY,
                statement -> setArgsForDeletingStatement(statement, id));
    }

    private void setArgsForDeletingStatement(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (SQLException e) {
            throw new ConfigPreparedStatmentException(e.getMessage());
        }
    }
}
