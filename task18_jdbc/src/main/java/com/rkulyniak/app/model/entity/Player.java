package com.rkulyniak.app.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class Player {

    private Integer id;
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    private String nationality;
    @Builder.Default
    private Double rating = 1200.0;
}
