package com.rkulyniak.app.model.dao;

import com.rkulyniak.app.model.entity.Sponsor;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public interface SponsorDao extends GenericDao<Sponsor> {

    @Override
    List<Sponsor> getAll();

    List<Sponsor> getAll(@NonNull Integer tournamentId);

    @Override
    Optional<Sponsor> get(@NonNull Integer id);

    @Override
    void save(@NonNull Sponsor sponsor);

    @Override
    void update(@NonNull Sponsor sponsor);

    @Override
    void delete(@NonNull Integer id);
}
