package com.rkulyniak.app.model.transformer;

import com.rkulyniak.app.model.entity.TournamentStage;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TournamentStageTransformer implements Transformer<TournamentStage> {

    private final int idColumnIndex = 1;
    private final int nameColumnIndex = 2;

    @Override
    public TournamentStage transformEntity(ResultSet resultSet) throws SQLException {
        return TournamentStage.builder()
                .id(resultSet.getInt(idColumnIndex))
                .name(resultSet.getString(nameColumnIndex))
                .build();
    }
}
