package com.rkulyniak.app.model.transformer;

import com.rkulyniak.app.model.entity.Player;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerTransformer implements Transformer<Player> {

    private final int idColumnIndex = 1;
    private final int firstNameColumnIndex = 2;
    private final int lastNameColumnIndex = 3;
    private final int nationalityColumnIndex = 4;
    private final int ratingColumnIndex = 5;

    @Override
    public Player transformEntity(@NonNull ResultSet resultSet) throws SQLException {
        return Player.builder()
                .id(resultSet.getInt(idColumnIndex))
                .firstName(resultSet.getString(firstNameColumnIndex))
                .lastName(resultSet.getString(lastNameColumnIndex))
                .nationality(resultSet.getString(nationalityColumnIndex))
                .rating(resultSet.getDouble(ratingColumnIndex))
                .build();
    }
}
