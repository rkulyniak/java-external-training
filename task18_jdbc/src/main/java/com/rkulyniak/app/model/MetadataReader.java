package com.rkulyniak.app.model;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;

@Slf4j
public class MetadataReader {

    public void logConnectionMetadata() {
        try (Connection connection = DBConnectionProvider.getReadOnlyConnection()) {
            DatabaseMetaData connectionMetaData = connection.getMetaData();
            log.info("Driver Name: " + connectionMetaData.getDriverName());
            log.info("Driver Version: " + connectionMetaData.getDriverVersion());
            log.info("URL: " + connectionMetaData.getURL());
            log.info("User Name: " + connectionMetaData.getUserName());
            log.info((connectionMetaData.supportsANSI92FullSQL() ?
                    "ANSI92FullSQL supported." : "ANSI92FullSQL not supported."));
            log.info((connectionMetaData.supportsTransactions() ?
                    "Transaction supported." : "Transaction not supported."));
        } catch (SQLException e) {
            log.error("error has been occurred while reading connection metadata; " +
                    "error msg: " + e.getMessage());
        }
    }

    public void logSqlQueryMetaData(String sqlQuery) {
        try (Connection connection = DBConnectionProvider.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            ResultSetMetaData queryMetaData = resultSet.getMetaData();
            if (queryMetaData != null) {
                log.info("Number of Columns: " + queryMetaData.getColumnCount());
                log.info("Table Name: " + queryMetaData.getTableName(1));
                log.info("Catalog Name: " + queryMetaData.getCatalogName(1));
                log.info("------------------------------------------");
                for (int i = 1; i <= queryMetaData.getColumnCount(); i++) {
                    log.info("Class Name: " + queryMetaData.getColumnClassName(i));
                    log.info("Column Name: " + queryMetaData.getColumnName(i));
                    log.info("Column Type Name: " + queryMetaData.getColumnTypeName(i));
                    log.info("--------------------------------------");
                }
            }
        } catch (SQLException e) {
            log.error("error has been occurred while reading connection metadata; " +
                    "error msg: " + e.getMessage());
        }
    }
}
