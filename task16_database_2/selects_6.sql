#1
select maker,
       case
           when (select count(1)
                 from Product as Product2
                          join PC on Product2.model = PC.model
                 where Product.maker = Product2.maker) > 0 THEN CONCAT('yes', '(', (select count(1)
                                                                                    from Product as Product2
                                                                                             join PC on Product2.model = PC.model
                                                                                    where Product.maker = Product2.maker),
                                                                       ')')
           else 'no'
           end
from Product
group by maker
order by maker;

#2
select Income_o.point, Income_o.date, SUM(inc), SUM(`out`)
from Income_o
join Outcome_o on Income_o.point = Outcome_o.point
group by Income_o.point, Income_o.date;

#3

#4
select trip_no,
       Company.name,
       plane,
       town_from,
       town_to,
       TIMEDIFF(time_in, time_out)
from Trip
         join Company on Trip.ID_comp = Company.ID_comp;

#5

