#1
select model, price
from Printer
where price = (select MAX(price) from Printer);

#2
select 'Laptop' as 'type', model, speed
from Laptop
where speed < (select MIN(speed) from PC);

#3
select maker, price
from Product
         join Printer on Product.model = Printer.model
where color = 'y'
  and price = (select MIN(price) from Printer where color = 'y');

#4
select maker, count(PC.model)
from Product
         join PC on Product.model = PC.model
group by maker;

#5
select avg(hd)
from Product
         join PC on Product.model = PC.model
where maker = any (select maker
                   from Product
                   where maker in (select maker FROM Product where type = 'PC')
                     and maker in (select maker FROM Product where type = 'Printer'));

#6
select date, count(T.trip_no)
from Pass_in_trip
         join Trip T on Pass_in_trip.trip_no = T.trip_no
where town_from = 'London'
group by date;

#7
select date, count(T.trip_no)
from Pass_in_trip
         join Trip T on Pass_in_trip.trip_no = T.trip_no
where town_to = 'Moscow'
group by date;

#8
select country, MAX(nLounched)
from (select country, launched, count(launched) as 'nLounched'
      from Ships
               join Classes C on Ships.class = C.class
      group by country, launched) as ship_launching
group by country;

#9
select battle, count(ship) as 'nShip', country
from Ships
         join Outcomes on Ships.name = Outcomes.ship
         join Classes on Ships.class = Classes.class
group by country, battle
having nShip >= 2;
