create database if not exists airport;

use airport;

create table if not exists country
(
    id   int auto_increment,
    name varchar(50) unique not null,
    primary key (id)
);

create table if not exists city
(
    id         int auto_increment,
    name       varchar(50) unique not null,
    population int,
    country_id int                not null,
    primary key (id),
    foreign key (country_id) references country (id) on update cascade
);

create table if not exists pilot_rang
(
    id         int auto_increment,
    short_name varchar(10) unique not null,
    full_name  varchar(30) unique not null,
    primary key (id)
);

create table if not exists pilot_qualification
(
    id   int auto_increment,
    name varchar(30) unique not null,
    primary key (id)
);

create table if not exists pilot
(
    id                     int auto_increment,
    first_name             varchar(30) not null,
    last_name              varchar(30) not null,
    pilot_qualification_id int         not null,
    pilot_rang_id          int         null,
    work_start_date        date        not null,
    primary key (id),
    foreign key (pilot_qualification_id) references pilot_qualification (id) on update cascade,
    foreign key (pilot_rang_id) references pilot_rang (id) on update cascade on delete set null
);

create table if not exists passenger_aircraft
(
    id                 int auto_increment,
    name               varchar(30) unique not null,
    speed              double             not null,
    passenger_capacity int                not null,
    primary key (id)
);

create table if not exists passenger_place_type
(
    aircraft_id int         not null,
    place_type  varchar(30) not null,
    primary key (aircraft_id, place_type),
    foreign key (aircraft_id) references passenger_aircraft (id) on update cascade on delete cascade
);

create table if not exists passenger_flight
(
    id                    int auto_increment,
    flight_date           date not null,
    flight_time           time not null,
    passenger_aircraft_id int  null,
    first_pilot_id        int  null,
    second_pilot_id       int  null,
    city_from_id          int  not null,
    city_to_id            int  not null,
    primary key (id),
    unique key (flight_date, flight_time, passenger_aircraft_id),
    foreign key (passenger_aircraft_id) references passenger_aircraft (id) on update cascade on delete set null,
    foreign key (first_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (second_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (city_from_id) references city (id) on update cascade,
    foreign key (city_to_id) references city (id) on update cascade
);

create table if not exists passenger
(
    id         int auto_increment,
    first_name varchar(30) not null,
    last_name  varchar(30) not null,
    primary key (id)
);


create table if not exists passenger_flight_passenger
(
    passenger_flight_id int not null,
    passenger_id        int not null,
    primary key (passenger_flight_id, passenger_id),
    foreign key (passenger_flight_id) references passenger_flight (id),
    foreign key (passenger_id) references passenger (id)
);

create table if not exists ticket
(
    passenger_id         int         not null,
    passenger_flight_id  int         not null,
    passenger_place_type varchar(30) not null,
    primary key (passenger_id, passenger_flight_id, passenger_place_type),
    foreign key (passenger_id) references passenger (id),
    foreign key (passenger_flight_id) references passenger_flight (id),
    foreign key (passenger_flight_id, passenger_place_type) references passenger_place_type (aircraft_id, place_type)
);

create table if not exists cargo_load_method
(
    id     int auto_increment,
    method varchar(30) not null,
    primary key (id)
);

create table if not exists cargo_aircraft
(
    id             int auto_increment,
    name           varchar(30) unique not null,
    speed          double             not null,
    load_capacity  bigint             not null,
    load_method_id int,
    primary key (id),
    foreign key (load_method_id) references cargo_load_method (id) on update cascade on delete set null
);

create table if not exists cargo_flight
(
    id                int auto_increment,
    flight_date       date not null,
    flight_time       time not null,
    cargo_aircraft_id int  null,
    first_pilot_id    int  null,
    second_pilot_id   int  null,create database if not exists airport;

use airport;

create table if not exists country
(
    id   int auto_increment,
    name varchar(50) unique not null,
    primary key (id)
);

create table if not exists city
(
    id         int auto_increment,
    name       varchar(50) unique not null,
    population int,
    country_id int                not null,
    primary key (id),
    foreign key (country_id) references country (id) on update cascade
);

create table if not exists pilot_rang
(
    id         int auto_increment,
    short_name varchar(10) unique not null,
    full_name  varchar(30) unique not null,
    primary key (id)
);

create table if not exists pilot_qualification
(
    id   int auto_increment,
    name varchar(30) unique not null,
    primary key (id)
);

create table if not exists pilot
(
    id                     int auto_increment,
    first_name             varchar(30) not null,
    last_name              varchar(30) not null,
    pilot_qualification_id int         not null,
    pilot_rang_id          int         null,
    work_start_date        date        not null,
    primary key (id),
    foreign key (pilot_qualification_id) references pilot_qualification (id) on update cascade,
    foreign key (pilot_rang_id) references pilot_rang (id) on update cascade on delete set null
);

create table if not exists passenger_aircraft
(
    id                 int auto_increment,
    name               varchar(30) unique not null,
    speed              double             not null,
    passenger_capacity int                not null,
    primary key (id)
);

create table if not exists passenger_place_type
(
    aircraft_id int         not null,
    place_type  varchar(30) not null,
    primary key (aircraft_id, place_type),
    foreign key (aircraft_id) references passenger_aircraft (id) on update cascade on delete cascade
);

create table if not exists passenger_flight
(
    id                    int auto_increment,
    flight_date           date not null,
    flight_time           time not null,
    passenger_aircraft_id int  null,
    first_pilot_id        int  null,
    second_pilot_id       int  null,
    city_from_id          int  not null,
    city_to_id            int  not null,
    primary key (id),
    unique key (flight_date, flight_time, passenger_aircraft_id),
    foreign key (passenger_aircraft_id) references passenger_aircraft (id) on update cascade on delete set null,
    foreign key (first_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (second_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (city_from_id) references city (id) on update cascade,
    foreign key (city_to_id) references city (id) on update cascade
);

create table if not exists passenger
(
    id         int auto_increment,
    first_name varchar(30) not null,
    last_name  varchar(30) not null,
    primary key (id)
);


create table if not exists passenger_flight_passenger
(
    passenger_flight_id int not null,
    passenger_id        int not null,
    primary key (passenger_flight_id, passenger_id),
    foreign key (passenger_flight_id) references passenger_flight (id),
    foreign key (passenger_id) references passenger (id)
);

create table if not exists ticket
(
    passenger_id         int         not null,
    passenger_flight_id  int         not null,
    passenger_place_type varchar(30) not null,
    primary key (passenger_id, passenger_flight_id, passenger_place_type),
    foreign key (passenger_id) references passenger (id),
    foreign key (passenger_flight_id) references passenger_flight (id),
    foreign key (passenger_flight_id, passenger_place_type) references passenger_place_type (aircraft_id, place_type)
);

create table if not exists cargo_load_method
(
    id     int auto_increment,
    method varchar(30) not null,
    primary key (id)
);

create table if not exists cargo_aircraft
(
    id             int auto_increment,
    name           varchar(30) unique not null,
    speed          double             not null,
    load_capacity  bigint             not null,
    load_method_id int,
    primary key (id),
    foreign key (load_method_id) references cargo_load_method (id) on update cascade on delete set null
);

create table if not exists cargo_flight
(
    id                int auto_increment,
    flight_date       date not null,
    flight_time       time not null,
    cargo_aircraft_id int  null,
    first_pilot_id    int  null,
    second_pilot_id   int  null,
    city_from_id      int  not null,
    city_to_id        int  not null,
    primary key (id),
    unique key (flight_date, flight_time, cargo_aircraft_id),
    foreign key (cargo_aircraft_id) references cargo_aircraft (id) on update cascade on delete set null,
    foreign key (first_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (second_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (city_from_id) references city (id) on update cascade,
    foreign key (city_to_id) references city (id) on update cascade
);

create table if not exists cargo
(
    cargo_flight_id int         not null,
    name            varchar(50) not null,
    weight          bigint      not null,
    primary key (cargo_flight_id, name),
    foreign key (cargo_flight_id) references cargo_flight (id) on update cascade on delete cascade
);

    city_from_id      int  not null,
    city_to_id        int  not null,
    primary key (id),
    unique key (flight_date, flight_time, cargo_aircraft_id),
    foreign key (cargo_aircraft_id) references cargo_aircraft (id) on update cascade on delete set null,
    foreign key (first_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (second_pilot_id) references pilot (id) on update cascade on delete set null,
    foreign key (city_from_id) references city (id) on update cascade,
    foreign key (city_to_id) references city (id) on update cascade
);

create table if not exists cargo
(
    cargo_flight_id int         not null,
    name            varchar(50) not null,
    weight          bigint      not null,
    primary key (cargo_flight_id, name),
    foreign key (cargo_flight_id) references cargo_flight (id) on update cascade on delete cascade
);

