#1
select maker
from Product as product
where exists(select *
             from PC as pc
             where pc.model = product.model);

#2
select maker
from Product as product
where exists(select *
             from PC as pc
             where pc.model = product.model
               and speed >= 750);

#3
select maker
from Product as product
where exists(select *
             from PC as pc
             where product.model = pc.model
               and speed >= 750)
  and exists(select *
             from Laptop as laptop
             where product.model = laptop.model
               and speed >= 750)

#4
select maker
from Product
         join PC on Product.model = PC.model
where speed = (select MAX(speed) from PC)
  and maker IN (select maker from Product where type = 'Printer');

#5
select name, launched, displacement
from Ships
         join Classes C on Ships.class = C.class
where launched >= 1922
  and displacement > 35000;

#6
select Classes.class
from Outcomes
         join Ships on Outcomes.ship = Ships.name
         join Classes on Ships.class = Classes.class
where result = 'sunk';

#7
select distinct maker
from Product
where maker in (select maker from Product where type = 'Laptop')
  and maker in (select maker from Product where type = 'Printer');

#8
select distinct maker
from Product
where maker in (select maker from Product where type = 'Laptop')
  and maker not in (select maker from Product where type = 'Printer');
