#1
select maker, PC.model, type, price
from Product
         join PC on Product.model = PC.model
UNION
select maker, Laptop.model, type, price
from Product
         join Laptop on Product.model = Laptop.model
UNION
select maker, Printer.model, Product.type, price
from Product
         join Printer on Product.model = Printer.model

#2
select model, MAX(price)
from PC
group by model
union
select model, MAX(price)
from Laptop
group by model
union
select model, MAX(price)
from Printer
group by model

#3
select AVG(avg_price)
from (select AVG(price) as 'avg_price'
      from Product
               join PC on Product.model = PC.model
      WHERE maker = 'A'
      UNION
      select AVG(price) as 'avg_price'
      from Product
               join Laptop on Product.model = Laptop.model
      WHERE maker = 'A') as PC_Laptop;

#4
select name
from Ships
union
select ship
from Outcomes;

#5
select C.class
from Ships
         join Classes C on Ships.class = C.class
group by C.class
having count(Ships.name) = 1;

#6
select C.class, count(Ships.name)
from Ships
         join Classes C on Ships.class = C.class
group by C.class;

#7
select C.class
from Ships
         join Classes C on Ships.class = C.class
group by C.class
having count(Ships.name) = 1 or count(Ships.name) = 2;

#8
select name
from Ships
where launched < 1922;
