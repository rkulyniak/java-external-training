#1
select Product.maker, pc_stat.product_count + laptop_stat.product_count + printer_stat.product_count as products
from Product
         join (select maker, count(ALL PC.model) as product_count
               from Product
                        left join PC on Product.model = PC.model
               group by maker) as pc_stat on Product.maker = pc_stat.maker
         join (select maker, count(ALL Laptop.model) as product_count
               from Product
                        left join Laptop on Product.model = Laptop.model
               group by maker) as laptop_stat on Product.maker = laptop_stat.maker
         join (select maker, count(ALL Printer.model) as product_count
               from Product
                        left join Printer on Product.model = Printer.model
               group by maker) as printer_stat on Product.maker = printer_stat.maker
group by Product.maker
order by maker;

#2
select maker, avg(ALL Laptop.screen) as product_count
from Product
         left join Laptop on Product.model = Laptop.model
group by maker;

#3
select maker, MAX(ALL PC.price) as product_count
from Product
         left join PC on Product.model = PC.model
group by maker;

#4
select maker, MIN(ALL PC.price) as product_count
from Product
         left join PC on Product.model = PC.model
group by maker;

#5
select pc_1.code, pc_1.model, avg(ALL pc_2.price)
from PC as pc_1,
     PC as pc_2
where pc_1.code != pc_2.code
and pc_1.speed > 600
and pc_1.speed = pc_2.speed
group by pc_1.code, pc_1.model
order by pc_1.code;

#6
select avg(hd)
from PC
         join Product on PC.model = Product.model
    and maker in (select distinct maker
                  from Product
                  where maker in (select maker from Product where type = 'PC')
                    and maker in (select maker from Product where type = 'Printer'));

#7
select Ships.name, Classes.displacement, Classes.numGuns
from Outcomes
         join Ships on Outcomes.ship = Ships.name
         join Classes on Ships.class = Classes.class
where Outcomes.battle = 'Guadalcanal';

#8
select Outcomes.ship, Classes.country, Classes.numGuns
from Outcomes
         join Ships on Outcomes.ship = Ships.name
         join Classes on Ships.class = Classes.class
where Outcomes.result = 'damaged';
