# 1
select distinct maker
from Product
where maker in (select distinct maker from Product where type = 'PC')
and maker not in(select maker from Product where type = 'Laptop');

#2
select distinct maker
from Product
where type = 'PC'
  and maker != all (select distinct maker from Product where type = 'Laptop');

#3
select distinct maker
from Product
where maker = any (select distinct maker from Product where type = 'PC')
  and maker != all (select maker from Product where type = 'Laptop');

#4
select maker
from Product
where maker in (select maker from Product where type = 'PC')
  and maker in (select maker from Product where type = 'Laptop');

#5
select distinct maker
from Product
where (type = 'PC' or type = 'Laptop')
  and maker != all (select distinct maker
                    from Product
                    where maker in (select distinct maker from Product where type = 'PC')
                      and maker not in (select maker from Product where type = 'Laptop'))
  and maker != all (select distinct maker
                    from Product
                    where maker in (select distinct maker from Product where type = 'Laptop')
                      and maker not in (select maker from Product where type = 'PC'));

#6
select maker
from Product
where maker = any (select maker from Product where type = 'PC')
  and maker = any (select maker from Product where type = 'Laptop');

#7
select distinct maker
from Product
where type = 'PC'
  and model in (select model from PC);

#8
select country, class
from Classes
where country = 'Ukraine';

#9

#10
select count(model)
from Product
where maker = 'A'
  and type = 'PC';

#11
select model
from Product
where type = 'PC'
  and model != all (select model from PC);

#12
select model, price from Laptop
where price > all (select price from PC);

