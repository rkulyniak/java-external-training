#1
select AVG(price) 'середня ціна'
from Laptop;

#2
select concat('ID: ', code) 'code',
       concat('model: ', model) 'model',
       concat('speed: ', speed) 'speed',
       concat('ram: ', ram) 'ram',
       concat('hd: ', hd) 'hd',
       concat('cd: ', cd) 'cd',
       concat('price: ', price) 'price'
from PC;

#3
select concat(year(date), '.', month(date), '.', day(date)) 'date'
from Income;

#4
select distinct ship,
                result,
                case
                    when result = 'OK' THEN 'все добре'
                    when result = 'damaged' THEN 'пошкодження'
                    when result = 'sunk' THEN 'потопленно'
                    else '*не визначно'
                    end as resultUA
from Outcomes;

#5
select
       concat('ряд: ', SUBSTRING(place, 1, 1)) as 'ряд',
       concat('місце: ', SUBSTRING(place, 2, 2)) as 'місце'
from Pass_in_trip;

#6
select concat('from ', town_from, ' to ', town_to) 'trip'
from Trip;
