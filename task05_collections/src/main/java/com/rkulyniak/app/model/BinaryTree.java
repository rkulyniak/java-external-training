package com.rkulyniak.app.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//TODO: fix duplicates
public class BinaryTree<K extends Comparable<K>, V> {

    public static class Node<K, V> {

        private K key;
        private V value;
        private Node<K, V> rightChild;
        private Node<K, V> leftChild;

        Node(K key, V value) {
            this.key = key;
            this.value = value;

            rightChild = null;
            leftChild = null;
        }

        @Override
        public String toString() {
            return "[" + key + "] " + value;
        }
    }

    private Logger logger;
    private Node<K, V> root;

    public BinaryTree() {
        logger = LogManager.getLogger(BinaryTree.class);
        root = null;
    }

    public boolean add(K key, V value) {
        if (key == null) {
            logger.error("unable to add element; received null key");
            return false;
        }
        if (value == null) {
            logger.error("unable to add element; received null value");
            return false;
        }

        Node<K, V> newNode = new Node<>(key, value);
        Node<K, V> current = root;
        if (current == null) {
            root = newNode;
            logger.info(newNode + " has been added as root");
            return true;
        }

        while (true) {
            Node<K, V> parent = current;
            if (current.key.compareTo(key) > 0) {
                current = current.leftChild;
                if (current == null) {
                    parent.leftChild = newNode;
                    break;
                }
            } else {
                current = current.rightChild;
                if (current == null) {
                    parent.rightChild = newNode;
                    break;
                }
            }
        }

        logger.info(newNode + " has been added to the BTree");
        return true;
    }

    public boolean remove(K key) {
        if (key == null) {
            logger.error("unable to remove element; received null key");
            return false;
        }
        if (root == null) {
            logger.error("unable to remove element; BTree node is empty");
            return false;
        }

        Node<K, V> current = root;
        Node<K, V> parent = root;
        boolean isLeftChild = true;

        while (current.key.compareTo(key) != 0) {
            parent = current;
            if (current.key.compareTo(key) > 0) {
                isLeftChild = true;
                current = current.leftChild;
            } else {
                isLeftChild = false;
                current = current.rightChild;
            }

            if (current == null) {
                logger.info("element has not been found; key [" + key + "]");
                return false;
            }
        }
        Node<K, V> delNode = current;

        if (current.leftChild == null && current.rightChild == null) {
            deletedNodeHasNoChildren(parent, current, isLeftChild);
        } else if (current.rightChild == null || current.leftChild == null) {
            deletedNodeHasOneChild(parent, current, isLeftChild);
        } else {
            deletedNodeHasChildren(parent, current, isLeftChild);
        }

        logger.info(delNode + " has been removed from BTree");
        return true;
    }

    private void deletedNodeHasNoChildren(
            Node<K, V> parent,
            Node<K, V> current,
            boolean isLeftChild) {

        if (current == root) {
            root = null;
        } else if (isLeftChild) {
            parent.leftChild = null;
        } else {
            parent.rightChild = null;
        }
    }

    private void deletedNodeHasOneChild(
            Node<K, V> parent,
            Node<K, V> current,
            boolean isLeftChild) {

        if (current.rightChild == null) {
            if (current == root) {
                root = current.leftChild;
            } else if (isLeftChild) {
                parent.leftChild = current.leftChild;
            } else {
                parent.rightChild = current.leftChild;
            }
        } else {
            if (current == root) {
                root = current.rightChild;
            } else if (isLeftChild) {
                parent.leftChild = current.rightChild;
            } else {
                parent.rightChild = current.rightChild;
            }
        }
    }

    private void deletedNodeHasChildren(
            Node<K, V> parent,
            Node<K, V> current,
            boolean isLeftChild) {

        Node<K, V> successor = getSuccessor(current);

        if (current == root) {
            root = successor;
        } else if (isLeftChild) {
            parent.leftChild = successor;
        } else {
            parent.rightChild = successor;
        }
        successor.leftChild = current.leftChild;
    }

    private Node<K, V> getSuccessor(Node<K, V> delNode) {
        Node<K, V> successorParent = delNode;
        Node<K, V> successor = delNode;
        Node<K, V> current = delNode.rightChild;

        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.leftChild;
        }

        if (successor != delNode.rightChild) {
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = delNode.rightChild;
        }

        return successor;
    }

    public Node<K, V> find(K key) {
        if (key == null) {
            logger.error("unable to find element; received null key");
            return null;
        }
        if (root == null) {
            logger.error("unable to find element; BTree node is empty");
            return null;
        }

        Node<K, V> current = root;
        while (current.key.compareTo(key) != 0) {
            if (current.key.compareTo(key) > 0) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }

            if (current == null) {
                logger.info("element has not been found; key [" + key + "]");
                return null;
            }
        }

        logger.info("element '" + current + "'  has been found;");
        return current;
    }

    public Node<K, V> min() {
        if (root == null) {
            logger.error("unable to find minimum value; BTree is empty");
            return null;
        }

        Node<K, V> current = root;
        Node<K, V> previous;

        do {
            previous = current;
            current = current.leftChild;
        } while (current != null);

        logger.info("min value " + previous);
        return previous;
    }

    public Node<K, V> max() {
        if (root == null) {
            logger.error("unable to find minimum value; BTree is empty");
            return null;
        }

        Node<K, V> current = root;
        Node<K, V> previous;

        do {
            previous = current;
            current = current.rightChild;
        } while (current != null);

        logger.info("max value " + previous);
        return previous;
    }
}
