package com.rkulyniak.app.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;

public class StringComparing implements Comparable<StringComparing>, Comparator<StringComparing> {

    private Logger logger;

    private String first;
    private String second;

    public StringComparing() {
        this("string1", "string2");
    }

    public StringComparing(String first, String second) {
        logger = LogManager.getLogger(StringComparing.class);

        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        logger.info("string1 '" + first + "'");
        return first;
    }

    public void setFirst(String first) {
        if (first == null) {
            logger.info("unable to set string1; received null");
            return;
        }

        this.first = first;
        logger.info("set newString1 " + first);
    }

    public String getSecond() {
        logger.info("string2 '" + second + "'");
        return second;
    }

    public void setSecond(String second) {
        if (second == null) {
            logger.info("unable to set string2; received null");
            return;
        }

        this.second = second;
        logger.info("set newString2 " + second);
    }

    @Override
    public int compareTo(StringComparing another) {
        if (another == null) {
            logger.info("unable to compare first strings; received null");
            return -1;
        }

        logger.info("string1 comparing [Comparable] result:" + this.first.compareTo(another.first));
        return this.first.compareTo(another.first);
    }

    @Override
    public int compare(StringComparing o1, StringComparing o2) {
        if (o1 == null || o2 == null) {
            logger.info("unable to compare second strings; received null");
            return -1;
        }

        logger.info("string2 comparing [Comparator] result:" + o1.second.compareTo(o2.second));
        return o1.second.compareTo(o2.second);
    }
}
