package com.rkulyniak.app.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class StringDeque {

    private static final int DEFAULT_CAPACITY = 10;

    private Logger logger;

    private String[] strings;
    private int top;
    private int tail;

    public StringDeque() {
        this(DEFAULT_CAPACITY);
    }

    public StringDeque(int capacity) {
        logger = LogManager.getLogger(StringDeque.class);

        strings = new String[capacity];
        top = -1;
        tail = -1;

    }

    public String[] getStrings() {
        return Arrays.copyOf(strings, strings.length);
    }

    public boolean addFirst(String element) {
        if (element == null) {
            logger.error("unable to add element; received null");
            return false;
        }
        if (isFull()) {
            logger.error("unable to add element; array is full");
            return false;
        }

        if (isEmpty()) {
            top = 0;
            tail = 0;
        } else {
            top = (top + 1 == strings.length) ? (0) : (top + 1);
        }

        strings[top] = element;
        logger.info(element + " added to deque");
        return true;
    }

    public boolean addLast(String element) {
        if (element == null) {
            logger.error("unable to add element; received null");
            return false;
        }
        if (isFull()) {
            logger.error("unable to add element; array is full");
            return false;
        }

        if (isEmpty()) {
            top = 0;
            tail = 0;
        } else {
            tail = (tail == 0) ? (strings.length - 1) : (tail - 1);
        }

        strings[tail] = element;
        logger.info(element + " added to deque");
        return true;
    }

    public String removeFirst() {
        if (isEmpty()) {
            logger.error("unable to remove element; array is empty");
            return null;
        }

        int rmIndex = top;
        String rmValue = strings[rmIndex];

        top = (top == 0) ? (strings.length - 1) : (top - 1);
        strings[rmIndex] = null;
        logger.info(rmValue + " removed from array");
        return rmValue;
    }

    public String removeLast() {
        if (isEmpty()) {
            logger.error("unable to remove element; array is empty");
            return null;
        }

        int rmIndex = tail;
        String rmValue = strings[rmIndex];

        tail = (tail == strings.length - 1) ? (0) : tail + 1;
        strings[rmIndex] = null;
        logger.info(rmValue + " removed from array");
        return rmValue;
    }

    public String getFirst() {
        if (isEmpty()) {
            logger.error("unable to get element; array is empty");
            return null;
        }

        return strings[top];
    }

    public String getLast() {
        if (isEmpty()) {
            logger.error("unable to get element; array is empty");
            return null;
        }

        return strings[tail];
    }

    private boolean isFull() {
        return ((top == strings.length - 1) && (tail == 0)) || (top + 1 == tail);
    }

    private boolean isEmpty() {
        return top == -1 && tail == -1;
    }

    public void clear() {
        for (int i = 0; i < strings.length; i++) {
            strings[i] = null;
        }
        logger.info("deque has been cleared");
    }

    public boolean contains(String element) {
        if (element == null) {
            logger.error("unable to check containing; received null");
            return false;
        }

        for (String string : strings) {
            if (string != null && string.equals(element)) {
                logger.info("deque contains '" + element + "'");
                return true;
            }
        }
        logger.info("deque doesn't contain '" + element + "'");
        return false;
    }
}
