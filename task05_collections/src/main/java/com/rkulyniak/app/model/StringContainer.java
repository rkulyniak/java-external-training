package com.rkulyniak.app.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class StringContainer {

    private static final int DEFAULT_CAPACITY = 10;

    private Logger logger;

    private String[] strings;
    private int size;

    public StringContainer() {
        this(DEFAULT_CAPACITY);
    }

    public StringContainer(int capacity) {
        logger = LogManager.getLogger(StringContainer.class);

        strings = new String[capacity];
        size = 0;
    }

    public String[] getStrings() {
        return Arrays.copyOf(strings, strings.length);
    }

    public boolean add(String string) {
        if (string == null) {
            logger.error("unable to add string; received null");
            return false;
        }

        if (isFull()) {
            logger.info("container is full; container grows");
            grow();
        }
        strings[size++] = string;
        logger.info(string + " added to container");
        return true;
    }

    private void grow() {
        int oldCapacity = strings.length;
        int newCapacity = (oldCapacity * 3) / 2 + 1;
        logger.info("new container capacity " + newCapacity);

        String[] newArray = new String[newCapacity];
        System.arraycopy(strings, 0, newArray, 0, strings.length);
        logger.info("old container has copied to new container");

        strings = newArray;
    }

    public String get(int index) {
        if (!checkIndex(index)) {
            logger.error("unable to get element; index is out of bound [" + index + "]");
            return null;
        }
        logger.info("returned " + strings[index]);
        return strings[index];
    }

    public boolean remove(String string) {
        if (string == null) {
            logger.error("unable to remove element; received null");
            return false;
        }
        if (isEmpty()) {
            logger.error("unable to remove element; container is empty");
            return false;
        }

        int index = getIndex(string);
        if (index == -1) {
            logger.error("unable to remove element; element not found");
            return false;
        }

        return remove(index);
    }

    private int getIndex(String string) {
        for (int i = 0; i < string.length(); i++) {
            if (strings[i].equals(string)) {
                return i;
            }
        }

        return -1;
    }

    public boolean remove(int index) {
        if (!checkIndex(index)) {
            logger.error("unable to remove element; index is out of bound");
            return false;
        }
        String logMsg = strings[index] + " removed from container";

        int numMoved = size - index - 1;
        System.arraycopy(strings, index + 1, strings, index, numMoved);
        strings[--size] = null;

        logger.info(logMsg);
        return true;
    }

    public boolean checkIndex(int index) {
        return index >= 0 && index < size;
    }

    public boolean isFull() {
        return size == strings.length;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(String element) {
        if (element == null) {
            logger.error("unable to check containing; received null");
            return false;
        }
        if (isEmpty()) {
            logger.error("unable to check containing; container is empty");
            return false;
        }

        for (String string : strings) {
            if (string.equals(element)) {
                logger.info("container contains '" + element + "'");
                return true;
            }
        }
        logger.info("container doesn't contain '" + element + "'");
        return false;
    }

    public int indexOf(String element) {
        if (element == null) {
            logger.error("unable to return indexOf; received null");
            return -1;
        }
        if (isEmpty()) {
            logger.error("unable to return indexOf; container is empty");
            return -1;
        }

        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals(element)) {
                logger.error("first index of " + element + " is " + i);
                return i;
            }
        }
        logger.error("unable to return first index of element; element not found");
        return -1;
    }

    public int lastIndexOf(String element) {
        if (element == null) {
            logger.error("unable to return lastIndexOf; received null");
            return -1;
        }
        if (isEmpty()) {
            logger.error("unable to return lastIndexOf; container is empty");
            return -1;
        }

        for (int i = strings.length - 1; i >= 0; i--) {
            if (strings[i].equals(element)) {
                logger.error("last index of " + element + " is " + i);
                return i;
            }
        }
        logger.error("unable to return last index of element; element not found");
        return -1;
    }

    public boolean set(int index, String element) {
        if (!checkIndex(index)) {
            logger.error("unable to set element; index is out of bound [" + index + "]");
            return false;
        }
        if (element == null) {
            logger.error("unable to set element; received null");
            return false;
        }

        String oldValue = null;

        for (int i = 0; i < strings.length; i++) {
            if (i == index) {
                oldValue = strings[i];
                strings[i] = element;
                break;
            }
        }
        logger.info("Element: " + oldValue + ", index: " + index + " has been updated; new value: " + element);
        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(strings);
    }

    @Override
    public boolean equals(Object otherObj) {
        if (this == otherObj) return true;
        if (otherObj == null) return false;
        if (getClass() != otherObj.getClass()) return false;

        StringContainer obj = (StringContainer) otherObj;
        if (this.size != obj.size) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (this.strings[i] != obj.strings[i]) {
                return false;
            }
        }
        return true;
    }

    public void clear() {
        for (int i = 0; i < strings.length; i++) {
            strings[i] = null;
        }
        logger.info("container has been cleared");
    }

    public boolean removeRange(int fromIndex, int toIndex) {
        if (checkIndex(fromIndex) && checkIndex(toIndex)) {
            logger.error("unable to remove range; " +
                    "index is out of bound [from=" + fromIndex + ",to=" + toIndex + "]");
            return false;
        }

        for (int i = fromIndex; i <= toIndex; i++) {
            remove(i);
        }
        return true;
    }

    public String[] subArray(int fromIndex, int toIndex) {
        if (checkIndex(fromIndex) && checkIndex(toIndex)) {
            logger.error("unable to return subContainer; " +
                    "index is out of bound [from=" + fromIndex + ",to=" + toIndex + "]");
            return null;
        }

        String[] newArray = new String[toIndex - fromIndex + 1];
        for (int i = 0, j = fromIndex; i < newArray.length; i++, j++) {
            newArray[i] = strings[j];
        }

        logger.info("returned subContainer fromIndex:" + fromIndex + " toIndex: " + toIndex);
        return newArray;
    }

    public void sort() {
        if (isEmpty()) {
            logger.error("unable to sort container; container is empty");
            return;
        }

        int h = 1;

        while (h <= strings.length / 3)
            h = h * 3 + 1;

        while (h > 0) {
            for (int i = h; i < strings.length; i++) {
                String mark = strings[i];
                int j = i;

                while (j > h - 1 && (strings[j - h].compareTo(mark)) >= 0) {
                    strings[j] = strings[j - h];
                    j -= h;
                }

                strings[j] = mark;
            }
            h = (h - 1) / 3;
        }

        logger.info("container has been sorted");
    }
}
