package com.rkulyniak.app;

import com.rkulyniak.app.controller.Controller;
import com.rkulyniak.app.view.UserInterface;

public class Application {

    public static void main(String[] args) {
        Controller controller = new Controller();
        new UserInterface(controller).launch();
    }
}
