package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.BTreeController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class BTreeItem implements Executable {

    private BTreeController controller;

    private Scanner input;
    private Logger logger;

    private Map<String, String> menu;
    private Map<String, Executable> items;


    BTreeItem(BTreeController controller) {
        this.controller = controller;

        input = new Scanner(System.in);
        logger = LogManager.getLogger(BTreeItem.class);

        menu = new LinkedHashMap<>();
        items = new LinkedHashMap<>();
    }

    @Override
    public void execute() {
        logger.info("user switched to BinaryThree menu");

        initMenu();
        initItems();

        UserInterface.startMenu(menu, items);
    }

    private void initMenu() {
        menu.put("1", "1 - add");
        menu.put("2", "2 - remove");
        menu.put("3", "3 - find");
        menu.put("4", "4 - min value");
        menu.put("5", "5 - max value");
        menu.put("Q", "Q - exit");
    }

    private void initItems() {
        items.put("1", () -> {
            System.out.print("Key: ");
            String key = input.nextLine();
            System.out.print("Value: ");
            String value = input.nextLine();
            controller.addBTreeItem(key, value);
        });
        items.put("2", () -> {
            System.out.print("Key: ");
            String key = input.nextLine();
            controller.removeBTreeItem(key);
        });
        items.put("3", () -> {
            System.out.print("Key: ");
            String key = input.nextLine();
            controller.findBTreeItem(key);
        });
        items.put("4", () -> controller.minBTreeValue());
        items.put("5", () -> controller.maxBTreeValue());
        items.put("Q", () -> {
            // ...
        });
    }
}
