package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class UserInterface {

    private Logger logger;
    private Controller controller;

    private Map<String, String> mainMenu;
    private Map<String, Executable> mainMenuItems;

    public UserInterface(Controller controller) {
        logger = LogManager.getLogger(UserInterface.class);
        this.controller = controller;

        mainMenu = new LinkedHashMap<>();
        mainMenuItems = new LinkedHashMap<>();
    }

    public void launch() {
        logger.info("the application has started execution");
        // .............
        initMenu();
        initItems();
        startMenu(mainMenu, mainMenuItems);
        // .............
        logger.info("the application has finished execution");
    }

    private void initMenu() {
        mainMenu.put("1", "1 - StringContainer");
        mainMenu.put("2", "2 - StringComparing");
        mainMenu.put("3", "3 - StringDeque");
        mainMenu.put("4", "4 - BinaryTree");
        mainMenu.put("Q", "Q - exit");
    }

    private void initItems() {
        mainMenuItems.put("1", new ContainerItem(controller));
        mainMenuItems.put("2", new ComparingItem(controller));
        mainMenuItems.put("3", new DequeItem(controller));
        mainMenuItems.put("4", new BTreeItem(controller));
        mainMenuItems.put("Q", () -> System.out.println("Bye"));
    }

    static void startMenu(Map<String, String> menu, Map<String, Executable> items) {
        Scanner input = new Scanner(System.in);

        String keyMenu;
        do {
            System.out.println("-----------------------------------");
            for (String item : menu.values()) {
                System.out.println(item);
            }

            System.out.print("\nSelect mainMenu item: ");
            keyMenu = input.nextLine().toUpperCase();

            System.out.println();
            try {
                items.get(keyMenu).execute();
            } catch (NullPointerException e) {
                System.out.println("\ninvalid input; please re-enter");
            }
        } while (!keyMenu.equals("Q"));
    }
}
