package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.ComparingController;
import com.rkulyniak.app.model.StringComparing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ComparingItem implements Executable {

    private ComparingController controller;

    private Scanner input;
    private Logger logger;

    private Map<String, String> menu;
    private Map<String, Executable> items;

    ComparingItem(ComparingController controller) {
        this.controller = controller;

        input = new Scanner(System.in);
        logger = LogManager.getLogger(DequeItem.class);

        menu = new LinkedHashMap<>();
        items = new LinkedHashMap<>();
    }

    @Override
    public void execute() {
        logger.info("user switched to Comparing menu");

        initMenu();
        initItems();

        UserInterface.startMenu(menu, items);
    }

    private void initMenu() {
        menu.put("1", "1 - get first string");
        menu.put("2", "2 - set first string");
        menu.put("3", "3 - get second string");
        menu.put("4", "4 - set second string");
        menu.put("5", "5 - compare first string");
        menu.put("6", "6 - compare second string");
        menu.put("Q", "Q - exit");
    }

    private void initItems() {
        items.put("1", () -> controller.getComparingFirstStr());
        items.put("2", () -> {
            System.out.print("String: ");
            String string = input.nextLine();
            controller.setComparingFirstStr(string);
        });
        items.put("3", () -> controller.getComparingSecondStr());
        items.put("4", () -> {
            System.out.print("String: ");
            String string = input.nextLine();
            controller.setComparingSecondStr(string);
        });
        items.put("5", () -> {
            System.out.print("First string: ");
            String firstString = input.nextLine();
            System.out.print("Second string: ");
            String secondString = input.nextLine();
            controller.compareFirstString(new StringComparing(firstString, secondString));
        });
        items.put("6", () -> {
            System.out.print("First string: ");
            String firstString = input.nextLine();
            System.out.print("Second string: ");
            String secondString = input.nextLine();
            StringComparing comparing1 = new StringComparing(firstString, secondString);

            System.out.print("First string: ");
            firstString = input.nextLine();
            System.out.print("Second string: ");
            secondString = input.nextLine();
            StringComparing comparing2 = new StringComparing(firstString, secondString);

            controller.compareSecondString(comparing1, comparing2);
        });
        items.put("Q", () -> {
            // ...
        });
    }
}
