package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;
import com.rkulyniak.app.controller.DequeController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class DequeItem implements Executable {

    private DequeController controller;

    private Scanner input;
    private Logger logger;

    private Map<String, String> menu;
    private Map<String, Executable> items;

    DequeItem(DequeController controller) {
        this.controller = controller;

        input = new Scanner(System.in);
        logger = LogManager.getLogger(DequeItem.class);

        menu = new LinkedHashMap<>();
        items = new LinkedHashMap<>();
    }

    @Override
    public void execute() {
        logger.info("user switched to Deque menu");

        initMenu();
        initItems();

        UserInterface.startMenu(menu, items);
    }

    private void initMenu() {
        menu.put("1", "1 - add first");
        menu.put("2", "2 - add last");
        menu.put("3", "3 - remove first");
        menu.put("4", "4 - remove last");
        menu.put("5", "5 - get first");
        menu.put("6", "6 - get last");
        menu.put("7", "7 - clear deque");
        menu.put("8", "8 - contains");
        menu.put("9", "9 - print deque");
        menu.put("Q", "Q - exit");
    }

    private void initItems() {
        items.put("1", () -> {
            System.out.print("Element: ");
            String element = input.nextLine();
            controller.addDequeFirst(element);
        });
        items.put("2", () -> {
            System.out.print("Element: ");
            String element = input.nextLine();
            controller.addDequeLast(element);
        });
        items.put("3", () -> controller.removeDequeFirst());
        items.put("4", () -> controller.removeDequeLast());
        items.put("5", () -> controller.getDequeFirst());
        items.put("6", () -> controller.getDequeLast());
        items.put("7", () -> controller.clearDeque());
        items.put("8", () -> {
            System.out.print("Element: ");
            String element = input.nextLine();
            controller.containsInDeque(element);
        });
        items.put("9", () -> controller.printDeque());
        items.put("Q", () -> {
            // ...
        });
    }
}
