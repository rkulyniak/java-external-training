package com.rkulyniak.app.view;

public interface Executable {

    void execute();
}
