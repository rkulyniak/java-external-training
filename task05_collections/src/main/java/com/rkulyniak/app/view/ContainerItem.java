package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.ContainerController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ContainerItem implements Executable {

    private ContainerController controller;

    private Scanner input;
    private Logger logger;

    private Map<String, String> menu;
    private Map<String, Executable> items;

    ContainerItem(ContainerController controller) {
        this.controller = controller;

        input = new Scanner(System.in);
        logger = LogManager.getLogger(DequeItem.class);

        menu = new LinkedHashMap<>();
        items = new LinkedHashMap<>();
    }

    @Override
    public void execute() {
        logger.info("user switched to Container menu");

        initMenu();
        initItems();

        UserInterface.startMenu(menu, items);
    }

    private void initMenu() {
        menu.put("1", "1 - add");
        menu.put("2", "2 - get");
        menu.put("3", "3 - remove (str)");
        menu.put("4", "4 - remove (idx)");
        menu.put("5", "5 - contains");
        menu.put("6", "6 - set");
        menu.put("7", "7 - clear");
        menu.put("8", "8 - sort");
        menu.put("9", "9 - print");
        menu.put("Q", "Q - quit");
    }

    private void initItems() {
        items.put("1", () -> {
            System.out.print("Element: ");
            String element = input.nextLine();
            controller.addContainerElement(element);
        });
        items.put("2", () -> {
            int index = inputIndex();
            controller.getContainerElement(index);
        });
        items.put("3", () -> {
            System.out.print("Element: ");
            String element = input.nextLine();
            controller.removeContainerElement(element);
        });
        items.put("4", () -> {
            int index = inputIndex();
            controller.removeContainerElement(index);
        });
        items.put("5", () -> {
            System.out.print("Element: ");
            String element = input.nextLine();
            controller.containsInContainer(element);
        });
        items.put("6", () -> {
            int index = inputIndex();
            System.out.print("Element: ");
            String element = input.nextLine();
            controller.setContainerElement(index, element);
        });
        items.put("7", () -> controller.clearContainer());
        items.put("8", () -> controller.sortContainer());
        items.put("9", () -> controller.printContainer());
        items.put("Q", () -> {
            // ...
        });
    }

    private int inputIndex() {
        int index;
        while (true) {
            System.out.print("Index: ");
            try {
                index = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
                continue;
            }
            break;
        }
        return index;
    }
}
