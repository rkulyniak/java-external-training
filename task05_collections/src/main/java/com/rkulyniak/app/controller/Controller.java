package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.BinaryTree;
import com.rkulyniak.app.model.StringComparing;
import com.rkulyniak.app.model.StringContainer;
import com.rkulyniak.app.model.StringDeque;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Controller implements
        ContainerController, ComparingController, DequeController, BTreeController {

    private Logger logger;

    private StringComparing stringComparing;
    private StringContainer stringContainer;
    private StringDeque stringDeque;
    private BinaryTree<String, String> binaryTree;


    public Controller() {
        logger = LogManager.getLogger(Controller.class);

        stringComparing = new StringComparing();
        stringContainer = new StringContainer();
        stringDeque = new StringDeque();
        binaryTree = new BinaryTree<>();
    }

    @Override
    public void addBTreeItem(String key, String value) {
        binaryTree.add(key, value);
    }

    @Override
    public void removeBTreeItem(String key) {
        binaryTree.remove(key);
    }

    @Override
    public void findBTreeItem(String key) {
        binaryTree.find(key);
    }

    @Override
    public void minBTreeValue() {
        binaryTree.min();
    }

    @Override
    public void maxBTreeValue() {
        binaryTree.max();
    }

    // .......................................................

    @Override
    public void addDequeFirst(String element) {
        stringDeque.addFirst(element);
    }

    @Override
    public void addDequeLast(String element) {
        stringDeque.addLast(element);
    }

    @Override
    public void removeDequeFirst() {
        stringDeque.removeFirst();
    }

    @Override
    public void removeDequeLast() {
        stringDeque.removeLast();
    }

    @Override
    public void getDequeFirst() {
        stringDeque.getFirst();
    }

    @Override
    public void getDequeLast() {
        stringDeque.getLast();
    }

    @Override
    public void printDeque() {
        String[] strings = stringDeque.getStrings();
        logger.info(Arrays.toString(strings));
    }

    @Override
    public void clearDeque() {
        stringDeque.clear();
    }

    @Override
    public void containsInDeque(String element) {
        stringDeque.contains(element);
    }

    // .......................................................


    @Override
    public void getComparingFirstStr() {
        stringComparing.getFirst();
    }

    @Override
    public void setComparingFirstStr(String string) {
        stringComparing.setFirst(string);
    }

    @Override
    public void getComparingSecondStr() {
        stringComparing.getSecond();
    }

    @Override
    public void setComparingSecondStr(String string) {
        stringComparing.setSecond(string);
    }

    @Override
    public void compareFirstString(StringComparing anotherComparing) {
        stringComparing.compareTo(anotherComparing);
    }

    @Override
    public void compareSecondString(
            StringComparing firstComparing,
            StringComparing secondComparing) {

        stringComparing.compare(firstComparing, secondComparing);
    }

    // .......................................................


    @Override
    public void addContainerElement(String element) {
        stringContainer.add(element);
    }

    @Override
    public void getContainerElement(int index) {
        stringContainer.get(index);
    }

    @Override
    public void removeContainerElement(String value) {
        stringContainer.remove(value);
    }

    @Override
    public void removeContainerElement(int index) {
        stringContainer.remove(index);
    }

    @Override
    public void containsInContainer(String element) {
        stringContainer.contains(element);
    }

    @Override
    public void setContainerElement(int index, String element) {
        stringContainer.set(index, element);
    }

    @Override
    public void clearContainer() {
        stringContainer.clear();
    }

    @Override
    public void sortContainer() {
        stringContainer.sort();
    }

    @Override
    public void printContainer() {
        String[] strings = stringContainer.getStrings();
        logger.info(Arrays.toString(strings));
    }
}
