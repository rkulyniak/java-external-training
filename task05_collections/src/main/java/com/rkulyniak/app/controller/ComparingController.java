package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.StringComparing;

public interface ComparingController {

    void getComparingFirstStr();

    void setComparingFirstStr(String string);

    void getComparingSecondStr();

    void setComparingSecondStr(String string);

    void compareFirstString(StringComparing anotherComparing);

    void compareSecondString(StringComparing firstComparing, StringComparing secondComparing);
}
