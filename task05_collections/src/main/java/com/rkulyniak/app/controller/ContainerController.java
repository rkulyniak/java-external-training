package com.rkulyniak.app.controller;

public interface ContainerController {

    void addContainerElement(String element);

    void getContainerElement(int index);

    void removeContainerElement(String value);

    void removeContainerElement(int index);

    void containsInContainer(String element);

    void setContainerElement(int index, String element);

    void clearContainer();

    void sortContainer();

    void printContainer();
}
