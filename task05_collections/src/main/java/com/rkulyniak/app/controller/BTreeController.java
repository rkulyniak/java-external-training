package com.rkulyniak.app.controller;

public interface BTreeController {

    void addBTreeItem(String key, String value);

    void removeBTreeItem(String key);

    void findBTreeItem(String key);

    void minBTreeValue();

    void maxBTreeValue();
}
