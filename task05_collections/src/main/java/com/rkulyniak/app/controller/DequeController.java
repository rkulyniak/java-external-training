package com.rkulyniak.app.controller;

public interface DequeController {

    void addDequeFirst(String element);

    void addDequeLast(String element);

    void removeDequeFirst();

    void removeDequeLast();

    void getDequeFirst();

    void getDequeLast();

    void printDeque();

    void clearDeque();

    void containsInDeque(String element);
}
