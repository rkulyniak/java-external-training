package com.rkulyniak.app.model.entity;

public class TouristVoucher {

    private int id;
    private String type;
    private String country;
    private int nDays;
    private int nNights;
    private Transport transport;
    private Hotel hotel;

    public TouristVoucher() {}

    public TouristVoucher(int id, String type, String country,
                          int nDays, int nNights, Transport transport, Hotel hotel) {
        this.id = id;
        this.type = type;
        this.country = country;
        this.nDays = nDays;
        this.nNights = nNights;
        this.transport = transport;
        this.hotel = hotel;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getCountry() {
        return country;
    }

    public int getnDays() {
        return nDays;
    }

    public int getnNights() {
        return nNights;
    }

    public Transport getTransport() {
        return transport;
    }

    public Hotel getHotel() {
        return hotel;
    }

    @Override
    public String toString() {
        return "TouristVoucher id: " + id;
    }
}
