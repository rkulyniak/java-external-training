package com.rkulyniak.app.model.entity;

public enum Transport {
    car, bus, train, airplane, ship
}
