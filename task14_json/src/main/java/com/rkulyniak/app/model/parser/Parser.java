package com.rkulyniak.app.model.parser;

import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.gson.GsonParser;
import com.rkulyniak.app.model.parser.jackson.JacksonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public abstract class Parser {

    private File json;

    public Parser(String fileName) throws FileNotFoundException {
        setJson(fileName);
    }

    public File getJson() {
        return json;
    }

    public void setJson(String fileName) throws FileNotFoundException {
        json = new FileLoader().getFile("json" + "/" + fileName);
    }

    protected abstract List<TouristVoucher> parseFromFile() throws Exception;

    public static List<TouristVoucher> parserFromFile(String parserType, String fileName) throws Exception {
        String jsonFileName = fileName + ".json";

        switch (parserType.toUpperCase()) {
            case "JACKSON":
                return new JacksonParser(jsonFileName).parseFromFile();
            case "GSON":
                return new GsonParser(jsonFileName).parseFromFile();
            default:
                throw new IllegalArgumentException("invalid parser type");
        }
    }
}
