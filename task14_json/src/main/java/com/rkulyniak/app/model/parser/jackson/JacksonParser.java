package com.rkulyniak.app.model.parser.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser extends Parser {

    public JacksonParser(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws Exception {
        return Arrays.asList(new ObjectMapper().readValue(getJson(), TouristVoucher[].class));
    }
}
