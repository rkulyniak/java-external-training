package com.rkulyniak.app.model.parser.gson;

import com.google.gson.Gson;
import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class GsonParser extends Parser {

    public GsonParser(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws Exception {
        Optional<String> data = Files.lines(getJson().toPath()).reduce(String::concat);
        return Arrays.asList(new Gson().fromJson(data.orElseThrow(), TouristVoucher[].class));
    }
}
