package com.rkulyniak.app.model.entity;

public class Hotel {

    private String name;
    private double rating;
    private Apartment apartment;
    private HotelService services;

    public Hotel() {}

    public Hotel(String name, double rating, Apartment apartment, HotelService services) {
        this.name = name;
        this.rating = rating;
        this.apartment = apartment;
        this.services = services;
    }

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public HotelService getServices() {
        return services;
    }
}
