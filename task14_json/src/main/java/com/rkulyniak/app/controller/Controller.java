package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;
import com.rkulyniak.app.model.parser.jackson.JacksonValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    public void getTouristVouchersJackson(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = Parser.parserFromFile("Jackson", fileName);
            logger.info("returned list of TouristVouchers (Jackson parser)");
            logTouristVouchers(touristVouchers);
        } catch (Exception e) {
            logger.info("unable to get list of TouristVouchers (Jackson parser); " + e.getMessage());
        }
    }

    public void getTouristVouchersGson(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = Parser.parserFromFile("GSON", fileName);
            logger.info("returned list of TouristVouchers (GSON parser)");
            logTouristVouchers(touristVouchers);
        } catch (Exception e) {
            logger.info("unable to get list of TouristVouchers (GSON parser); " + e.getMessage());
        }
    }

    public void checkSchemaValidation(String fileName, String schemaName) {
        try {
            boolean result = JacksonValidator.validate(fileName, schemaName);
            logger.info("JSON file " + fileName + " has been validated by " + schemaName + "; " +
                    "result [" + result + "]");
        } catch (Exception e) {
            logger.info("unable to check schema validation; " + e.getMessage());
        }
    }

    private void logTouristVouchers(List<TouristVoucher> touristVouchers) {
        touristVouchers.sort(Comparator.comparingInt(TouristVoucher::getId));
        touristVouchers.forEach(touristVoucher -> {
            logger.info("TouristVoucher ID: " + touristVoucher.getId());
            logger.info("Type: " + touristVoucher.getType());
            logger.info("Country: " + touristVoucher.getCountry());
            logger.info("nDays: " + touristVoucher.getnDays());
            logger.info("nNights: " + touristVoucher.getnNights());
            logger.info("Transport: " + touristVoucher.getTransport());
            logger.info("Hotel");
            logger.info(" Name: " + touristVoucher.getHotel().getName());
            logger.info(" Rating: " + touristVoucher.getHotel().getRating());
            logger.info(" Apartment");
            logger.info("  idNumber: " + touristVoucher.getHotel().getApartment().getIdNumber());
            logger.info("  nPersons: " + touristVoucher.getHotel().getApartment().getnPersons());
            logger.info(" Services");
            logger.info("  Meal: " + touristVoucher.getHotel().getServices().isMeal());
            logger.info("  TV: " + touristVoucher.getHotel().getServices().isTv());
            logger.info("  Conditioner: " + touristVoucher.getHotel().getServices().isConditioner());
        });
    }
}
