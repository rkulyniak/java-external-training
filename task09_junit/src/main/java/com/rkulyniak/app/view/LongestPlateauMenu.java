package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.LongestPlateauController;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.rkulyniak.app.view.UserInterface.inputInteger;
import static com.rkulyniak.app.view.UserInterface.inputIntegers;

class LongestPlateauMenu implements Menu {

    private LongestPlateauController controller;

    LongestPlateauMenu(LongestPlateauController controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "Longest-Plateau";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Print array");
            put("2", "2 - Set array random");
            put("3", "3 - Set array custom");
            put("4", "4 - Print sequence");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.printArray());
            put("2", () -> controller.setArrayRandom(
                    inputInteger("size"),
                    inputInteger("min value"),
                    inputInteger("max value")
            ));
            put("3", () -> controller.setArrayCustom(inputIntegers()));
            put("4", () -> controller.printLongestContiguousSequence());
            put("Q", () -> System.out.println("\nback to main menu"));
        }};
    }
}
