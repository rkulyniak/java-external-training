package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.LongestPlateauController;
import com.rkulyniak.app.controller.MinesweeperController;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {

    private LongestPlateauController longestPlateauController;
    private MinesweeperController minesweeperController;

    public MainMenu() {
        longestPlateauController = new LongestPlateauController();
        minesweeperController = new MinesweeperController();
    }

    @Override
    public String initMenuName() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - \u25BA Longest Plateau");
            put("2", "2 - \u25BA Minesweeper");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> new LongestPlateauMenu(longestPlateauController).launch());
            put("2", () -> new MinesweeperMenu(minesweeperController).launch());
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
