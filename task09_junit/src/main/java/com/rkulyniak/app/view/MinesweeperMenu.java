package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.MinesweeperController;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.rkulyniak.app.view.UserInterface.inputDouble;
import static com.rkulyniak.app.view.UserInterface.inputInteger;

class MinesweeperMenu implements Menu {

    private MinesweeperController controller;

    MinesweeperMenu(MinesweeperController controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "Minesweeper";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Create new board");
            put("2", "2 - Print board [- solution]");
            put("3", "3 - Print board [+ solution]");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.createNewBoard(
                    inputInteger("m"),
                    inputInteger("n"),
                    inputDouble("p")
            ));
            put("2", () -> controller.printBoardWithoutSolution());
            put("3", () -> controller.printBoardWithSolution());
            put("Q", () -> System.out.println("\nback to main menu"));
        }};
    }
}
