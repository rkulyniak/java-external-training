package com.rkulyniak.app.view;

import java.util.Scanner;
import java.util.stream.IntStream;

class UserInterface {

    private static Scanner input = new Scanner(System.in);

    static int[] inputIntegers() {
        IntStream.Builder intBuilder = IntStream.builder();
        int index = 0;

        System.out.println("-- enter empty line for stopping input numbers --");
        while (true) {
            try {
                System.out.print("[" + index + "]: ");
                String userInput = input.nextLine();

                if (!userInput.isEmpty()) {
                    intBuilder.add(Integer.parseInt(userInput));
                    index++;
                } else {
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
            }
        }
        return intBuilder.build().toArray();
    }

    static int inputInteger(String text) {
        while (true) {
            try {
                System.out.print(text + ": ");
                return Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
            }
        }
    }

    static double inputDouble(String text) {
        while (true) {
            try {
                System.out.print(text + ": ");
                return Double.parseDouble(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
            }
        }
    }
}
