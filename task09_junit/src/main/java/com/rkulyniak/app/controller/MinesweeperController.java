package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.Minesweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class MinesweeperController {

    private static Logger logger = LogManager.getLogger(LongestPlateauController.class);

    private Minesweeper minesweeper;

    public MinesweeperController() {
        minesweeper = new Minesweeper();
    }

    public void createNewBoard(int m, int n, double percentage) {
        try {
            minesweeper.createNewBoard(m, n, percentage);
            logger.info("created new board; " + Arrays.deepToString(minesweeper.getBoard()));
        } catch (IllegalArgumentException e) {
            logger.info("unable to create new board; error msg[" + e.getMessage() + "]");
        }
    }

    public void printBoardWithoutSolution() {
        char[][] board = minesweeper.getBoardWithoutSolution();
        logger.info("received board without solution;");
        for (char[] chars : board) {
            logger.info(Arrays.toString(chars));
        }
    }

    public void printBoardWithSolution() {
        char[][] board = minesweeper.getBoardWithSolution();
        logger.info("received board with solution;");
        for (char[] chars : board) {
            logger.info(Arrays.toString(chars));
        }
    }
}
