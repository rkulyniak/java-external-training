package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.IntArrayRandomGenerator;
import com.rkulyniak.app.model.LongestPlateau;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Map;
import java.util.Random;

public class LongestPlateauController {

    private static Logger logger = LogManager.getLogger(LongestPlateauController.class);

    private LongestPlateau longestPlateau;
    private IntArrayRandomGenerator arrayRandomGenerator;

    public LongestPlateauController() {
        longestPlateau = new LongestPlateau();
        arrayRandomGenerator = (size, min, max) -> new Random().ints(size, min, max).toArray();
    }

    public void printArray() {
        int[] array = longestPlateau.getNumbers();
        logger.info("received array " + Arrays.toString(array));
    }

    public void setArrayRandom(int size, int minValue, int maxValue) {
        try {
            setArrayCustom(arrayRandomGenerator.generate(size, minValue, maxValue));
        } catch (IllegalArgumentException e) {
            logger.error("unable to set array; error msg[" + e.getMessage() + "]");
        }
    }

    public void setArrayCustom(int... numbers) {
        longestPlateau.setNumbers(numbers);
        logger.info("array has been updated " + Arrays.toString(numbers));
    }

    public void printLongestContiguousSequence() {
        Map<Integer, Integer> sequence = longestPlateau.getLongestContiguousSequence();
        logger.info("received longest contiguous sequence; " +
                "length=" + sequence.size() + ", " + sequence);
    }
}
