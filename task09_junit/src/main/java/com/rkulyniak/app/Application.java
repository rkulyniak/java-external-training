package com.rkulyniak.app;

import com.rkulyniak.app.view.MainMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info("the application has started execution");
        new MainMenu().launch();
        logger.info("the application has finished execution");
    }
}
