package com.rkulyniak.app.model;

@FunctionalInterface
public interface IntArrayRandomGenerator {

    int[] generate(int limit, int low, int high);
}