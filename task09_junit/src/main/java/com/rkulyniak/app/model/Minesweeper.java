package com.rkulyniak.app.model;

import java.util.Random;

public class Minesweeper {

    private int m;
    private int n;
    private boolean[][] board;

    public Minesweeper() {
        m = 0;
        n = 0;
        board = new boolean[m][n];
    }

    public boolean[][] getBoard() {
        return board;
    }

    public void createNewBoard(int m, int n, double percentage) {
        if (m <= 0 || n <= 0) {
            throw new IllegalArgumentException("M and N must be positive; m=" + m + ", n=" + n);
        }
        if (percentage < 0 || percentage > 100) {
            throw new IllegalArgumentException("percentage must be in range [0-100]; p=" + percentage);
        }
        this.m = m;
        this.n = n;

        board = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] = occupiedWithProbability(percentage);
            }
        }
    }

    private boolean occupiedWithProbability(double percentage) {
        return new Random().nextDouble() * 100 < percentage;
    }

    public char[][] getBoardWithoutSolution() {
        char[][] boardWithoutSolution = new char[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                boardWithoutSolution[i][j] = board[i][j] ? '*' : '.';
            }
        }
        return boardWithoutSolution;
    }

    public char[][] getBoardWithSolution() {
        char[][] boardWithSolution = new char[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                boardWithSolution[i][j] = board[i][j] ? '*' : getNumberOfNeighboringBombs(i, j);
            }
        }
        return boardWithSolution;
    }

    private char getNumberOfNeighboringBombs(int i, int j) {
        int bombs = 0;
        // ................................
        if (cellHasBomb(i - 1, j - 1))
            bombs++;
        if (cellHasBomb(i - 1, j))
            bombs++;
        if (cellHasBomb(i - 1, j + 1))
            bombs++;
        // ................................
        if (cellHasBomb(i, j - 1))
            bombs++;
        if (cellHasBomb(i, j + 1))
            bombs++;
        // ................................
        if (cellHasBomb(i + 1, j - 1))
            bombs++;
        if (cellHasBomb(i + 1, j))
            bombs++;
        if (cellHasBomb(i + 1, j + 1))
            bombs++;
        // ................................
        return (char) (bombs + '0');
    }

    private boolean cellHasBomb(int i, int j) {
        return cellExists(i, j) && board[i][j];
    }

    private boolean cellExists(int i, int j) {
        return (i >= 0 && i < m) && (j >= 0 && j < n);
    }
}
