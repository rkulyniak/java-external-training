package com.rkulyniak.app.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MinesweeperTest {

    @Test
    void createNewBoard_with_invalid_m_argument_test() {
        Minesweeper minesweeper = new Minesweeper();
        assertThrows(IllegalArgumentException.class,
                () -> minesweeper.createNewBoard(-100, 10, 50));
    }

    @Test
    void createNewBoard_with_invalid_n_argument_test() {
        Minesweeper minesweeper = new Minesweeper();
        assertThrows(IllegalArgumentException.class,
                () -> minesweeper.createNewBoard(10, -100, 50));
    }

    @Test
    void createNewBoard_with_invalid_percentage_argument_test() {
        Minesweeper minesweeper = new Minesweeper();
        assertThrows(IllegalArgumentException.class,
                () -> minesweeper.createNewBoard(10, 10, 150));
    }

    @Test
    void createNewBoard_updates_board_test() {
        Minesweeper minesweeper = new Minesweeper();
        boolean[][] oldBoard = minesweeper.getBoard();
        minesweeper.createNewBoard(10, 10, 50);
        assertFalse(Arrays.equals(oldBoard, minesweeper.getBoard()));
    }

    @Test
    void getBoardWithoutSolution_test() {
        Minesweeper minesweeper = new Minesweeper();
        boolean[][] board = minesweeper.getBoard();
        char[][] boardWithoutSolution = minesweeper.getBoardWithoutSolution();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j]) {
                    assertEquals(boardWithoutSolution[i][j], '*');
                } else {
                    assertEquals(boardWithoutSolution[i][j], '.');
                }
            }
        }
    }

    @Test
    void getBoardWithSolution_test() {
        Minesweeper minesweeper = new Minesweeper();
        boolean[][] board = minesweeper.getBoard();
        char[][] boardWithSolution = minesweeper.getBoardWithSolution();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j]) {
                    assertEquals(boardWithSolution[i][j], '*');
                } else {
                    assertTrue(boardWithSolution[i][j] >= '1' && boardWithSolution[i][j] <= '8');
                }
            }
        }
    }
}