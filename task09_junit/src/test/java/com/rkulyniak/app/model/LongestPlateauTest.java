package com.rkulyniak.app.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LongestPlateauTest {

    @Test
    void getNumbers_setNumbers_test() {
        LongestPlateau plateau = new LongestPlateau();
        int[] numbers = new int[0];
        plateau.setNumbers(numbers);
        assertArrayEquals(numbers, plateau.getNumbers());
    }

    @Test
    void getLongestContiguousSequence_when_array_of_numbers_is_null() {
        LongestPlateau plateau = new LongestPlateau();
        plateau.setNumbers(null);
        assertThrows(IllegalArgumentException.class, plateau::getLongestContiguousSequence);
    }

    @Test
    void getLongestContiguousSequence_when_array_of_numbers_is_empty() {
        LongestPlateau plateau = new LongestPlateau();
        plateau.setNumbers(new int[0]);
        assertThrows(IllegalArgumentException.class, plateau::getLongestContiguousSequence);
    }

    @ParameterizedTest
    @MethodSource("intArrayProvider")
    void getLongestContiguousSequence_test(int[] numbers, int[] resultSequence) {
        LongestPlateau plateau = new LongestPlateau();
        plateau.setNumbers(numbers);
        Map<Integer, Integer> sequence = plateau.getLongestContiguousSequence();
        int[] receivedSequence = sequence.values().stream()
                .mapToInt(Integer::intValue)
                .toArray();
        assertArrayEquals(receivedSequence, resultSequence);
    }

    private static Stream<Arguments> intArrayProvider() {
        return Stream.of(
                Arguments.of(new int[]{1}, new int[0]),
                Arguments.of(new int[]{1, 1, 1}, new int[0]),
                Arguments.of(new int[]{1, 0, 1}, new int[0]),
                Arguments.of(new int[]{0, 1, 0}, new int[]{0, 1, 0}),
                Arguments.of(new int[]{0, 23, 23, 0}, new int[]{0, 23, 23, 0}),
                Arguments.of(new int[]{5, 4, 34, 0, 23, 23, 0, 34}, new int[]{0, 23, 23, 0}),
                Arguments.of(new int[]{6, 6, 45, 5, 34, 34, 6, 7, 7, 7, 4}, new int[]{6, 7, 7, 7, 4}),
                Arguments.of(new int[]{1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1}, new int[]{0, 1, 1, 0})
        );
    }
}