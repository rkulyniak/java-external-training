package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.IntArrayRandomGenerator;
import com.rkulyniak.app.model.LongestPlateau;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class LongestPlateauControllerTest {

    @InjectMocks
    private LongestPlateauController controller;
    @Mock
    private LongestPlateau plateau;
    @Mock
    private IntArrayRandomGenerator generator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void printArray_invokes_getNumbers_test() {
        controller.printArray();
        verify(plateau, times(1)).getNumbers();
    }

    @Test
    void setArrayRandom_invokes_generate_and_setArrayCustom_test() {
        controller.setArrayRandom(anyInt(), anyInt(), anyInt());
        verify(generator, times(1)).generate(anyInt(), anyInt(), anyInt());
        verify(plateau, times(1)).setNumbers(any());
    }

    @Test
    void setArrayCustom_invokes_setArray_test() {
        controller.setArrayCustom(any());
        verify(plateau, times(1)).setNumbers(any());
    }

    @Test
    void printLongestContiguousSequence_invokes_getLongestContiguousSequence_test() {
        controller.printLongestContiguousSequence();
        verify(plateau, times(1)).getLongestContiguousSequence();
    }
}