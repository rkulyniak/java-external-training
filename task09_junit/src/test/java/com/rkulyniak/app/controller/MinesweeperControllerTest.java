package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.Minesweeper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class MinesweeperControllerTest {

    @InjectMocks
    private MinesweeperController controller;
    @Mock
    private Minesweeper minesweeper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createNewBoard() {
        controller.createNewBoard(anyInt(), anyInt(), anyDouble());
        verify(minesweeper, times(1)).createNewBoard(anyInt(), anyInt(), anyDouble());
    }

    @Test
    void printBoardWithoutSolution() {
        when(minesweeper.getBoardWithoutSolution()).thenReturn(new char[0][0]);
        controller.printBoardWithoutSolution();
        verify(minesweeper, times(1)).getBoardWithoutSolution();
    }

    @Test
    void printBoardWithSolution() {
        when(minesweeper.getBoardWithSolution()).thenReturn(new char[0][0]);
        controller.printBoardWithSolution();
        verify(minesweeper, times(1)).getBoardWithSolution();
    }
}