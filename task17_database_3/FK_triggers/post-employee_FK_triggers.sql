delimiter //
create trigger employee_postFK_inserting
    before insert
    on employee
    for each row
begin
    if (select count(1) from post where post.post = new.post) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t insert record. Foreign parent key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger employee_postFK_updating
    before update
    on employee
    for each row
begin
    if (select count(1) from post where post.post = new.post) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t update record. Foreign parent key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger post_restrict_updating
    before update
    on post
    for each row
begin
    if (new.post != old.post and (select count(1) from employee where post = old.post) = 0) then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t update record. Foreign key updates to employee table restricted!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger post_restrict_deleting
    before delete
    on post
    for each row
begin
    if (select count(1) from employee where post = old.post) != 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t delete record. Foreign key exists in employee table!';
    end if;
end //
delimiter ;
