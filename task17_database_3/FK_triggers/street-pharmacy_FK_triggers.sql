delimiter //
create trigger pharmacy_streetFK_inserting
    before insert
    on pharmacy
    for each row
begin
    if (select count(1) from street where street.street = new.street) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t insert record. Foreign parent key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger pharmacy_streetFK_updating
    before update
    on pharmacy
    for each row
begin
    if (select count(1) from street where street.street = new.street) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t update record. Foreign parent key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger street_cascade_updating
    after update
    on street
    for each row
begin
    update pharmacy set street = new.street where street = old.street;
end //
delimiter ;

# ...................................

delimiter //
create trigger street_restrict_deleting
    before delete
    on street
    for each row
begin
    if (select count(1) from pharmacy where street = old.street) != 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t delete record. Foreign key exists in pharmacy table!';
    end if;
end //
delimiter ;
