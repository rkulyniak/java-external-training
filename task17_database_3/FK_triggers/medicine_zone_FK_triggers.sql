delimiter //
create trigger medicine_zone_inserting
    before insert
    on medicine_zone
    for each row
begin
    if (select count(1) from medicine where id = new.medicine_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t insert record. Foreign medicine key does not exist!';
    elseif (select count(1) from zone where id = new.zone_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t insert record. Foreign zone key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger medicine_zone_updating
    before update
    on medicine_zone
    for each row
begin
    if (select count(1) from medicine where id = new.medicine_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t update record. Foreign medicine key does not exist!';
    elseif (select count(1) from zone where id = new.zone_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t update record. Foreign zone key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger medicine_cascade_updating_for_medicine_zone
    after update
    on medicine
    for each row
begin
    update medicine_zone set medicine_id = new.id where medicine_id = old.id;
end //
delimiter ;

# ...................................

delimiter //
create trigger medicine_cascade_deleting_for_medicine_zone
    after delete
    on medicine
    for each row
begin
    delete from medicine_zone where medicine_id = old.id;
end //
delimiter ;

# ...................................

delimiter //
create trigger zone_cascade_updating_for_medicine_zone
    after update
    on zone
    for each row
begin
    update medicine_zone set zone_id = new.id where zone_id = old.id;
end //
delimiter ;

# ...................................

delimiter //
create trigger zone_cascade_deleting_for_medicine_zone
    after delete
    on zone
    for each row
begin
    delete from medicine_zone where zone_id = old.id;
end //
delimiter ;
