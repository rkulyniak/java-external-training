delimiter //
create trigger employee_pharmacyFK_inserting
    before insert
    on employee
    for each row
begin
    if (select count(1) from pharmacy where id = new.pharmacy_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t insert record. Foreign parent key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger employee_pharmacyFK_updating
    before update
    on employee
    for each row
begin
    if (select count(1) from pharmacy where id = new.pharmacy_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t update record. Foreign parent key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger pharmacy_cascade_updating
    after update
    on pharmacy
    for each row
begin
    update employee set pharmacy_id = new.id where pharmacy_id = old.id;
end //
delimiter ;

# ...................................

delimiter //
create trigger pharmacy_setnull_deleting
    after delete
    on pharmacy
    for each row
begin
    update employee set pharmacy_id = null where pharmacy_id = old.id;
end //
delimiter ;

