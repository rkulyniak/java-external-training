delimiter //
create trigger pharmacy_medicine_inserting
    before insert
    on pharmacy_medicine
    for each row
begin
    if (select count(1) from pharmacy where id = new.pharmacy_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t insert record. Foreign pharmacy key does not exist!';
    elseif (select count(1) from medicine where id = new.medicine_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t insert record. Foreign medicine key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger pharmacy_medicine_updating
    before update
    on pharmacy_medicine
    for each row
begin
    if (select count(1) from pharmacy where id = new.pharmacy_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t update record. Foreign pharmacy key does not exist!';
    elseif (select count(1) from medicine where id = new.medicine_id) = 0 then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Can\'t update record. Foreign medicine key does not exist!';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger pharmacy_cascade_updating_for_pharmacy_medicine
    after update
    on pharmacy
    for each row
    follows pharmacy_cascade_updating
begin
    update pharmacy_medicine set pharmacy_id = new.id where pharmacy_id = old.id;
end //
delimiter ;

# ...................................

delimiter //
create trigger pharmacy_cascade_deleting_for_pharmacy_medicine
    after delete
    on pharmacy
    for each row
    follows pharmacy_setnull_deleting
begin
    delete from pharmacy_medicine where pharmacy_id = old.id;
end //
delimiter ;

# ...................................

delimiter //
create trigger medicine_cascade_updating_for_pharmacy_medicine
    after update
    on medicine
    for each row
begin
    update pharmacy_medicine set medicine_id = new.id where medicine_id = old.id;
end //
delimiter ;

# ...................................

delimiter //
create trigger medicine_cascade_deleting_for_pharmacy_medicine
    after delete
    on medicine
    for each row
begin
    delete from pharmacy_medicine where medicine_id = old.id;
end //
delimiter ;
