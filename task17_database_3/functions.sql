delimiter //
create function MIN_EXPERIENCE(id_employee int)
    returns int
begin
    return (select MIN(experience) from employee where id = id_employee);
end //
delimiter ;

# ...................................

delimiter //
create function PHARMACY_ADDRESS(pharmacy_id int)
    returns varchar(50)
begin
    return (select CONCAT(name, ' ', building_number) from pharmacy where id = pharmacy_id);
end //
delimiter ;
