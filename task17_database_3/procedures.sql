delimiter //
create procedure employee_inserting(IN first_name varchar(30), IN last_name varchar(30), IN employee_post varchar(15), IN pharmacy int)
begin
    insert into employee (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)
    values (first_name, last_name, null, null, null, null, null, employee_post, pharmacy);
end //
delimiter ;

# ...................................

delimiter //
create procedure medicine_zone_inserting(IN medicine_id int, IN zone_id int)
begin
    if ((select 1 from medicine where id = medicine_id) != 0 and (select 1 from zone where id = zone_id)) then
        insert into medicine_zone(medicine_id, zone_id) values (medicine_id, zone_id);
    end if;
end //
delimiter ;

# ...................................

drop procedure creating_table_with_employee_names;

delimiter //
create procedure creating_table_with_employee_names()
begin
    declare done int default false;
    declare firstName, lastName varchar(30);
    declare nColumns int;

    declare myCursor cursor for select name, surname from employee;
    declare continue handler for not found set done = true;

    open myCursor;
    myLoop:
        loop
            fetch myCursor into firstName, lastName;
            if done = true then
                leave myLoop;
            end if;

            set nColumns = FLOOR(RAND() * (9 - 1) + 1);

            if nColumns = 1 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int)');
            elseif nColumns = 2 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int)');
            elseif nColumns = 3 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int, c3 int)');
            elseif nColumns = 4 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int, c3 int, c4 int)');
            elseif nColumns = 5 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int, c3 int, c4 int, c5 int)');
            elseif nColumns = 6 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int)');
            elseif nColumns = 7 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int, c7 int)');
            elseif nColumns = 8 then
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int, c7 int, c8 int)');
            else
                set @temp_query := CONCAT('create table ', firstName, '_', lastName,
                                          '(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int, c7 int, c8 int, c9 int)');
            end if;

            prepare myQuery from @temp_query;
            execute myQuery;
            deallocate prepare myQuery;
        end loop;
    close myCursor;
end //
delimiter ;
