delimiter //
create trigger identity_number_inserting
    before insert
    on employee
    for each row
    follows employee_pharmacyFK_inserting
begin
    if (new.identity_number like '%00') then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Unable to insert record; identity_number cannot end with two zeros';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger medicine_ministry_code_inserting
    before insert
    on medicine
    for each row
begin
    if (new.ministry_code not rlike '[A-LN-OQ-Z]-\d\d\d-\d\d') then
        SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
                'Unable to insert record; invalid ministry_code';
    end if;
end //
delimiter ;

# ...................................

delimiter //
create trigger forbid_post_modifying
    before update
    on post
    for each row
    precedes post_restrict_updating
begin
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT =
            'Unable to update post; operation is forbidden';
end //
delimiter ;
