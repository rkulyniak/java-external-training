package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    private Task task;

    public Controller() {
        task = new Task();
    }

    public List<String> getUserAnnotatedFields() {
        List<String> userAnnotatedFields = task.getUserAnnotatedFields();
        logger.info("annotated fields " + userAnnotatedFields);
        return userAnnotatedFields;
    }

    public List<String> getAnnotationValueInfo() {
        List<String> annotationValueInfo = task.getAnnotationValueInfo();
        logger.info("annotation value " + annotationValueInfo);
        return annotationValueInfo;
    }

    public void invokeMethods() {
        invokeFirstMethod();
        invokeSecondMethod();
        invokeThirdMethod();
    }

    private void invokeFirstMethod() {
        String name = "say1";
        var arg = 10;

        String result;
        try {
            result = task.invokeMethod(name, arg);
        } catch (InvocationTargetException | IllegalAccessException e) {
            result = "unable to invoke method " + name + "[" + e.getMessage() + "]";
        }
        logger.info(name + " invoked; result [" + result + "]");
    }

    private void invokeSecondMethod() {
        String name = "say2";
        var arg = 80.23;

        String result;
        try {
            result = task.invokeMethod(name, arg);
        } catch (InvocationTargetException | IllegalAccessException e) {
            result = "unable to invoke method " + name + "[" + e.getMessage() + "]";
        }
        logger.info(name + " invoked; result [" + result + "]");
    }

    private void invokeThirdMethod() {
        String name = "say3";
        var arg = "word";

        String result;
        try {
            result = task.invokeMethod(name, arg);
        } catch (InvocationTargetException | IllegalAccessException e) {
            result = "unable to invoke method " + name + "[" + e.getMessage() + "]";
        }
        logger.info(name + " invoked; result [" + result + "]");
    }

    public void resetUnknowingField() {
        String result;
        try {
            result = task.resetUnknowingField();
        } catch (IllegalAccessException e) {
            result = "unable to set unknowing field [" + e.getMessage() + "]";
        }
        logger.info(result);
    }

    public void invokeMyMethod1() {
        String name = "myMethod1";
        String s = "string";
        int number = 10;

        String result;
        try {
            result = task.invokeMethod(name, s, new int[]{number});
        } catch (InvocationTargetException | IllegalAccessException e) {
            result = "unable to invoke method " + name + "[" + e.getMessage() + "]";
        }
        logger.info(result);
    }

    public void invokeMyMethod2() {
        String name = "myMethod2";
        String s = "string";

        String result;
        try {
            result = task.invokeMethod(name, new Object[]{new String[]{s}});
        } catch (InvocationTargetException | IllegalAccessException e) {
            result = "unable to invoke method " + name + "[" + e.getMessage() + "]";
        }
        logger.info(result);
    }

    public List<String> getInfoAboutTaskClass() {
        List<String> infoAboutTask = task.getInfoAboutTaskClass();
        logger.info("received info about Task.class");
        infoAboutTask.forEach(s -> logger.info(s));
        return infoAboutTask;
    }
}
