package com.rkulyniak.app.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class ClassInfo<T> {

    private Class<T> clazz;

    ClassInfo(Class<T> clazz) {
        this.clazz = clazz;
    }

    String getName() {
        return clazz.getName();
    }

    String getClassModifiers() {
        return Modifier.toString(clazz.getModifiers());
    }

    List<String> getFieldsInfo() {
        Field[] fields = clazz.getDeclaredFields();
        List<String> result = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            result.add(field.getType() + " " + field.getName() + " " + Modifier.toString(field.getModifiers()));
        }
        return Collections.unmodifiableList(result);
    }

    List<String> getMethodsInfo() {
        Method[] methods = clazz.getDeclaredMethods();
        List<String> result = new ArrayList<>();
        for (Method method : methods) {
            method.setAccessible(true);
            result.add(method.getName() + " " + Modifier.toString(method.getModifiers()) + "\n" +
                    "return type: " + method.getReturnType() + "\n" +
                    "parameters: " + Arrays.toString(method.getParameterTypes()) + "\n" +
                    "exceptions: " + Arrays.toString(method.getExceptionTypes()) + "\n\n");
        }
        return Collections.unmodifiableList(result);
    }
}
