package com.rkulyniak.app.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class Task {

    private ClassInfo<Task> informator;
    private User user;

    public Task() {
        informator = new ClassInfo<>(Task.class);
        user = new User("roman.kulyniak@gmail.com", "rkulyniak", "0000");
    }

    public List<String> getUserAnnotatedFields() {
        return Arrays.stream(user.getClass().getDeclaredFields())
                .peek(field -> field.setAccessible(true))
                .filter(field -> field.isAnnotationPresent(Definition.class))
                .map(Field::getName)
                .collect(Collectors.toUnmodifiableList());
    }

    public List<String> getAnnotationValueInfo() {
        return Arrays.stream(user.getClass().getDeclaredFields())
                .peek(field -> field.setAccessible(true))
                .filter(field -> field.isAnnotationPresent(Definition.class))
                .map(field -> field.getAnnotation(Definition.class))
                .map(Annotation::toString)
                .collect(Collectors.toUnmodifiableList());
    }

    public String invokeMethod(String name, Object... arg)
            throws InvocationTargetException, IllegalAccessException {
        Optional<Method> method = Arrays.stream(user.getClass().getDeclaredMethods())
                .filter(m -> m.getName().equals(name))
                .findFirst();
        return method.isPresent() ? (String) method.get().invoke(user, arg) : "method has not found";
    }

    public String resetUnknowingField() throws IllegalAccessException {
        Optional<Field> field = Arrays.stream(user.getClass().getDeclaredFields())
                .peek(f -> f.setAccessible(true))
                .collect(Collectors.collectingAndThen(Collectors.toList(), collected -> {
                    Collections.shuffle(collected);
                    return collected.stream();
                }))
                .findFirst();
        String result;
        if (field.isPresent()) {
            field.get().set(user, "");
            result = field.get().getName() + " has been reset to empty";
        } else {
            result = "field has not found";
        }
        return result;
    }

    public List<String> getInfoAboutTaskClass() {
        return new ArrayList<>() {{
            add("Name: " + informator.getName());
            add("Class modifiers: " + informator.getClassModifiers());
            add("Fields: " + informator.getFieldsInfo());
            add("Methods: \n" + informator.getMethodsInfo());
        }};
    }
}
