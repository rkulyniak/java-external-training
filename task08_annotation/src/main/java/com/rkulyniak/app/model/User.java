package com.rkulyniak.app.model;

class User {

    private String email;
    @Definition(info = "a username is a name that uniquely identifies someone on a computer system")
    private String username;
    @Definition(info = "a string of characters that allows access to a computer system or service")
    private String password;

    User(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    String say1(int number) {
        return username + " says " + number;
    }

    String say2(double percentage) {
        return username + " says " + percentage;
    }

    String say3(String word) {
        return username + " says " + word;
    }

    String myMethod1(String s, int... args) {
        return "myMethod(String s, int... args) invoked";
    }

    String myMethod2(String... args) {
        return "myMethod(String... args) invoked";
    }
}
