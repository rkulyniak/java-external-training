package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Print annotated fields");
            put("2", "2 - Print annotation value");
            put("3", "3 - Invoke methods");
            put("4", "4 - Reset unknowing field");
            put("5", "5 - Invoke MyMethod(String s, int... args)");
            put("6", "6 - Invoke MyMethod(String... args)");
            put("7", "7 - Print all info about 'Task' class");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", controller::getUserAnnotatedFields);
            put("2", controller::getAnnotationValueInfo);
            put("3", controller::invokeMethods);
            put("4", controller::resetUnknowingField);
            put("5", controller::invokeMyMethod1);
            put("6", controller::invokeMyMethod2);
            put("7", controller::getInfoAboutTaskClass);
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
