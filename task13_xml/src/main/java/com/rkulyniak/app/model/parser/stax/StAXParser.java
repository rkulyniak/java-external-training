package com.rkulyniak.app.model.parser.stax;

import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.util.List;

public class StAXParser extends Parser {

    private StAXReader staxReader;

    public StAXParser(String xmlFileName, String xsdFileName) throws Exception {
        super(xmlFileName, xsdFileName);
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        FileInputStream fileInputStream = new FileInputStream(getXml());
        staxReader = new StAXReader(inputFactory.createXMLEventReader(fileInputStream));
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws XMLStreamException {
        staxReader.readData();
        return staxReader.getTouristVouchers();
    }
}
