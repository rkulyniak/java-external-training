package com.rkulyniak.app.model.entity;

public class HotelService {

    private boolean meal;
    private boolean TV;
    private boolean conditioner;

    public HotelService() {}

    public HotelService(boolean meal, boolean TV, boolean conditioner) {
        this.meal = meal;
        this.TV = TV;
        this.conditioner = conditioner;
    }

    public boolean isMeal() {
        return meal;
    }

    public void setMeal(boolean meal) {
        this.meal = meal;
    }

    public boolean isTV() {
        return TV;
    }

    public void setTV(boolean TV) {
        this.TV = TV;
    }

    public boolean isConditioner() {
        return conditioner;
    }

    public void setConditioner(boolean conditioner) {
        this.conditioner = conditioner;
    }
}
