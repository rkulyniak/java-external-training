package com.rkulyniak.app.model.parser.sax;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

class SAXValidator {

    static Schema createSchema(File xsd) throws SAXException {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        return factory.newSchema(xsd);
    }
}
