package com.rkulyniak.app.model.parser;

import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.entity.Transport;
import com.rkulyniak.app.model.parser.dom.DOMParser;
import com.rkulyniak.app.model.parser.sax.SAXParser;
import com.rkulyniak.app.model.parser.stax.StAXParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public abstract class Parser {

    private File xml;
    private File xsd;

    public Parser(String xmlFileName, String xsdFileName) throws FileNotFoundException {
        setXml(xmlFileName);
        setXsd(xsdFileName);
    }

    public File getXml() {
        return xml;
    }

    public void setXml(String xmlFileName) throws FileNotFoundException {
        xml = new FileLoader().getFile("xml" + "/" + xmlFileName);
    }

    public File getXsd() {
        return xsd;
    }

    public void setXsd(String xsdFileName) throws FileNotFoundException {
        xsd = new FileLoader().getFile("xml" + "/" + xsdFileName);
    }

    protected abstract List<TouristVoucher> parseFromFile() throws Exception;

    public static List<TouristVoucher> parserFromFile(String parserType, String fileName) throws Exception {
        String xmlFileName = fileName + ".xml";
        String xsdFileName = fileName + ".xsd";

        switch (parserType.toUpperCase()) {
            case "DOM":
                return new DOMParser(xmlFileName, xsdFileName).parseFromFile();
            case "SAX":
                return new SAXParser(xmlFileName, xsdFileName).parseFromFile();
            case "STAX":
                return new StAXParser(xmlFileName, xsdFileName).parseFromFile();
            default:
                throw new IllegalArgumentException("invalid parser type");
        }
    }

    public static Transport getTransport(String transport) {
        switch (transport) {
            case "car":
                return Transport.CAR;
            case "bus":
                return Transport.BUS;
            case "train":
                return Transport.TRAIN;
            case "airplane":
                return Transport.AIRPLANE;
            case "ship":
                return Transport.SHIP;
            default:
                throw new IllegalArgumentException("unsupported transport instance [" + transport + "]");
        }
    }
}
