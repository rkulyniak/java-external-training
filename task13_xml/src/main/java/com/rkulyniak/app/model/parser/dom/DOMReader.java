package com.rkulyniak.app.model.parser.dom;


import com.rkulyniak.app.model.entity.*;
import com.rkulyniak.app.model.parser.Parser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

class DOMReader {

    private Document document;
    private List<TouristVoucher> touristVouchers;

    DOMReader(Document document) {
        this.document = document;
        touristVouchers = new ArrayList<>();
    }

    List<TouristVoucher> getTouristVouchers() {
        return touristVouchers;
    }

    void readData() {
        NodeList nodeList = document.getElementsByTagName("touristVoucher");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                int id = Integer.parseInt(element.getAttribute("id"));
                String type = getTextContext(element, "type", 0);
                String country = getTextContext(element, "country", 0);
                int nDays = getIntegerContext(element, "nDays", 0);
                int nNights = getIntegerContext(element, "nNights", 0);
                Transport transport = Parser.getTransport(getTextContext(element, "transport", 0));
                Hotel hotel = getHotel(getElement(element.getElementsByTagName("hotel")));

                touristVouchers.add(new TouristVoucher(id, type, country, nDays, nNights, transport, hotel));
            }
        }
    }

    private Hotel getHotel(Element element) {
        String name = getTextContext(element, "name", 0);
        double rating = getDoubleContext(element, "rating", 0);
        Apartment apartment = getApartment(getElement(element.getElementsByTagName("apartment")));
        HotelService services = getHotelService(getElement(element.getElementsByTagName("services")));
        return new Hotel(name, rating, apartment, services);
    }

    private Apartment getApartment(Element element) {
        int idNumber = getIntegerContext(element, "idNumber", 0);
        int nPersons = getIntegerContext(element, "nPersons", 0);
        return new Apartment(idNumber, nPersons);
    }

    private HotelService getHotelService(Element element) {
        boolean meal = getBooleanContext(element, "meal", 0);
        boolean TV = getBooleanContext(element, "TV", 0);
        boolean conditioner = getBooleanContext(element, "conditioner", 0);
        return new HotelService(meal, TV, conditioner);
    }

    private int getIntegerContext(Element element, String tag, int item) {
        return Integer.parseInt(getTextContext(element, tag, item));
    }

    private double getDoubleContext(Element element, String tag, int item) {
        return Double.parseDouble(getTextContext(element, tag, item));
    }

    private boolean getBooleanContext(Element element, String tag, int item) {
        return Boolean.parseBoolean(getTextContext(element, tag, item));
    }

    private String getTextContext(Element element, String tag, int item) {
        return element.getElementsByTagName(tag).item(item).getTextContent();
    }

    private Element getElement(NodeList node) {
        return (Element) node.item(0);
    }
}
