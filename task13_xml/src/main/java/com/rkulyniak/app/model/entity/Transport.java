package com.rkulyniak.app.model.entity;

public enum Transport {
    CAR, BUS, TRAIN, AIRPLANE, SHIP
}
