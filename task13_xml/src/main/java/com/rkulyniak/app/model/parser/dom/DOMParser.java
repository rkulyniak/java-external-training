package com.rkulyniak.app.model.parser.dom;

import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class DOMParser extends Parser {

    private DOMReader domReader;

    public DOMParser(String xmlFileName, String xsdFileName)
            throws IOException, SAXException, ParserConfigurationException {
        super(xmlFileName, xsdFileName);
        DOMCreator domCreator = new DOMCreator(getXml());
        domReader = new DOMReader(domCreator.getDocument());
    }

    @Override
    public List<TouristVoucher> parseFromFile() {
        domReader.readData();
        return domReader.getTouristVouchers();
    }
}
