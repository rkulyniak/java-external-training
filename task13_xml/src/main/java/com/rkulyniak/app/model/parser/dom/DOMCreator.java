package com.rkulyniak.app.model.parser.dom;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DOMCreator {

    private DocumentBuilderFactory builderFactory;
    private DocumentBuilder documentBuilder;
    private Document document;

    DOMCreator(File xml) throws ParserConfigurationException, IOException, SAXException {
        builderFactory = DocumentBuilderFactory.newInstance();
        documentBuilder = builderFactory.newDocumentBuilder();
        document = documentBuilder.parse(xml);
    }

    public DocumentBuilderFactory getBuilderFactory() {
        return builderFactory;
    }

    public DocumentBuilder getDocumentBuilder() {
        return documentBuilder;
    }

    Document getDocument() {
        return document;
    }
}
