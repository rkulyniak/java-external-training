package com.rkulyniak.app.model.entity;

public class Apartment {

    private int idNumber;
    private int nPersons;

    public Apartment() {}

    public Apartment(int idNumber, int nPersons) {
        this.idNumber = idNumber;
        this.nPersons = nPersons;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }

    public int getnPersons() {
        return nPersons;
    }

    public void setnPersons(int nPersons) {
        this.nPersons = nPersons;
    }
}
