package com.rkulyniak.app.model.parser.sax;

import com.rkulyniak.app.model.entity.Apartment;
import com.rkulyniak.app.model.entity.Hotel;
import com.rkulyniak.app.model.entity.HotelService;
import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

class SAXHandler extends DefaultHandler {

    private List<TouristVoucher> touristVouchers;

    private TouristVoucher touristVoucher;
    private boolean type;
    private boolean country;
    private boolean nDays;
    private boolean nNights;
    private boolean transport;
    private boolean hName;
    private boolean hRating;
    private boolean aIdNumber;
    private boolean aNPersons;
    private boolean meal;
    private boolean TV;
    private boolean conditioner;

    SAXHandler() {
        touristVouchers = new ArrayList<>();
    }

    List<TouristVoucher> getTouristVouchers() {
        return touristVouchers;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("touristVoucher")) {
            touristVoucher = new TouristVoucher();
            touristVoucher.setHotel(new Hotel());
            touristVoucher.getHotel().setApartment(new Apartment());
            touristVoucher.getHotel().setServices(new HotelService());

            touristVoucher.setId(Integer.parseInt(attributes.getValue("id")));
        } else if (qName.equalsIgnoreCase("type")) {
            type = true;
        } else if (qName.equalsIgnoreCase("country")) {
            country = true;
        } else if (qName.equalsIgnoreCase("nDays")) {
            nDays = true;
        } else if (qName.equalsIgnoreCase("nNights")) {
            nNights = true;
        } else if (qName.equalsIgnoreCase("transport")) {
            transport = true;
        } else if (qName.equalsIgnoreCase("name")) {
            hName = true;
        } else if (qName.equalsIgnoreCase("rating")) {
            hRating = true;
        } else if (qName.equalsIgnoreCase("idNumber")) {
            aIdNumber = true;
        } else if (qName.equalsIgnoreCase("nPersons")) {
            aNPersons = true;
        } else if (qName.equalsIgnoreCase("meal")) {
            meal = true;
        } else if (qName.equalsIgnoreCase("TV")) {
            TV = true;
        } else if (qName.equalsIgnoreCase("conditioner")) {
            conditioner = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("touristVoucher")) {
            touristVouchers.add(touristVoucher);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (type) {
            touristVoucher.setType(new String(ch, start, length));
            type = false;
        } else if (country) {
            touristVoucher.setCountry(new String(ch, start, length));
            country = false;
        } else if (nDays) {
            touristVoucher.setDays(Integer.parseInt(new String(ch, start, length)));
            nDays = false;
        } else if (nNights) {
            touristVoucher.setNights(Integer.parseInt(new String(ch, start, length)));
            nNights = false;
        } else if (transport) {
            touristVoucher.setTransport(Parser.getTransport(new String(ch, start, length)));
            transport = false;
        } else if (hName) {
            touristVoucher.getHotel().setName(new String(ch, start, length));
            hName = false;
        } else if (hRating) {
            touristVoucher.getHotel().setRating(Double.parseDouble(new String(ch, start, length)));
            hRating = false;
        } else if (aIdNumber) {
            touristVoucher.getHotel().getApartment().setIdNumber(Integer.parseInt(new String(ch, start, length)));
            aIdNumber = false;
        } else if (aNPersons) {
            touristVoucher.getHotel().getApartment().setnPersons(Integer.parseInt(new String(ch, start, length)));
            aNPersons = false;
        } else if (meal) {
            touristVoucher.getHotel().getServices().setMeal(Boolean.parseBoolean(new String(ch, start, length)));
            meal = false;
        } else if (TV) {
            touristVoucher.getHotel().getServices().setTV(Boolean.parseBoolean(new String(ch, start, length)));
            TV = false;
        } else if (conditioner) {
            touristVoucher.getHotel().getServices().setConditioner(Boolean.parseBoolean(new String(ch, start, length)));
            conditioner = false;
        }
    }
}
