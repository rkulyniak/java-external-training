package com.rkulyniak.app.model.parser.sax;

import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;

import javax.xml.parsers.SAXParserFactory;
import java.util.List;

public class SAXParser extends Parser {

    private SAXParserFactory parserFactory;
    private SAXHandler handler;
    private javax.xml.parsers.SAXParser parser;

    public SAXParser(String xmlFileName, String xsdFileName) throws Exception {
        super(xmlFileName, xsdFileName);
        parserFactory = SAXParserFactory.newInstance();
        parserFactory.setSchema(SAXValidator.createSchema(getXsd()));
        parser = parserFactory.newSAXParser();
        handler = new SAXHandler();
    }

    @Override
    public List<TouristVoucher> parseFromFile() throws Exception {
        parser.parse(getXml(), handler);
        return handler.getTouristVouchers();
    }
}
