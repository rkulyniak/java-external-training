package com.rkulyniak.app.model.parser.stax;

import com.rkulyniak.app.model.entity.Apartment;
import com.rkulyniak.app.model.entity.Hotel;
import com.rkulyniak.app.model.entity.HotelService;
import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

class StAXReader {

    private XMLEventReader eventReader;
    private List<TouristVoucher> touristVouchers;

    StAXReader(XMLEventReader eventReader) {
        this.eventReader = eventReader;
        touristVouchers = new ArrayList<>();
    }

    List<TouristVoucher> getTouristVouchers() {
        return touristVouchers;
    }

    void readData() throws XMLStreamException {
        TouristVoucher touristVoucher = new TouristVoucher();
        touristVoucher.setHotel(new Hotel());
        touristVoucher.getHotel().setApartment(new Apartment());
        touristVoucher.getHotel().setServices(new HotelService());

        while (eventReader.hasNext()) {
            XMLEvent xmlEvent = eventReader.nextEvent();
            if (xmlEvent.isStartElement()) {

                StartElement startElement = xmlEvent.asStartElement();
                String name = startElement.getName().getLocalPart();
                switch (name) {
                    case "touristVoucher":
                        Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                        if (idAttr != null) {
                            touristVoucher.setId(Integer.parseInt(idAttr.getValue()));
                        }
                        break;
                    case "type":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.setType(xmlEvent.asCharacters().getData());
                        break;
                    case "country":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.setCountry(xmlEvent.asCharacters().getData());
                        break;
                    case "nDays":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.setDays(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "nNights":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.setNights(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "transport":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.setTransport(Parser.getTransport(xmlEvent.asCharacters().getData()));
                        break;
                    case "name":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.getHotel().setName(xmlEvent.asCharacters().getData());
                        break;
                    case "rating":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.getHotel().setRating(Double.parseDouble(xmlEvent.asCharacters().getData()));
                        break;
                    case "idNumber":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.getHotel().getApartment().setIdNumber(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "nPersons":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.getHotel().getApartment().setnPersons(Integer.parseInt(xmlEvent.asCharacters().getData()));
                        break;
                    case "meal":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.getHotel().getServices().setMeal(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                        break;
                    case "TV":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.getHotel().getServices().setTV(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                        break;
                    case "conditioner":
                        xmlEvent = eventReader.nextEvent();
                        touristVoucher.getHotel().getServices().setConditioner(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                        break;
                }
            } else if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("touristVoucher")) {
                    touristVouchers.add(touristVoucher);
                }
            }
        }
    }
}
