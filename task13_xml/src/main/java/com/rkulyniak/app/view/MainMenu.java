package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Parse from file DOM");
            put("2", "2 - Parse from file SAX");
            put("3", "3 - Parse from file StAX");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.getTouristVouchersDOM("touristVouchers"));
            put("2", () -> controller.getTouristVouchersSAX("touristVouchers"));
            put("3", () -> controller.getTouristVouchersStAX("touristVouchers"));
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
