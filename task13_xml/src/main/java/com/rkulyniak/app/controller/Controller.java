package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.entity.TouristVoucher;
import com.rkulyniak.app.model.parser.Parser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    public void getTouristVouchersDOM(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = Parser.parserFromFile("DOM", fileName);
            logger.info("returned list of TouristVouchers (DOM parser)");
            logTouristVouchers(touristVouchers);
        } catch (Exception e) {
            logger.info("unable to get list of TouristVouchers (DOM parser); " + e.getMessage());
        }
    }

    public void getTouristVouchersSAX(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = Parser.parserFromFile("SAX", fileName);

            logger.info("returned list of TouristVouchers (SAX parser)");
            logTouristVouchers(touristVouchers);
        } catch (Exception e) {
            logger.info("unable to get list of TouristVouchers (SAX parser); " + e.getMessage());
        }
    }

    public void getTouristVouchersStAX(String fileName) {
        try {
            List<TouristVoucher> touristVouchers = Parser.parserFromFile("StAX", fileName);
            logger.info("returned list of TouristVouchers (StAX parser)");
            logTouristVouchers(touristVouchers);
        } catch (Exception e) {
            logger.info("unable to get list of TouristVouchers (StAX parser); " + e.getMessage());
        }
    }

    private void logTouristVouchers(List<TouristVoucher> touristVouchers) {
        touristVouchers.sort(Comparator.comparingInt(TouristVoucher::getId));
        touristVouchers.forEach(touristVoucher -> {
            logger.info("TouristVoucher ID: " + touristVoucher.getId());
            logger.info("Type: " + touristVoucher.getType());
            logger.info("Country: " + touristVoucher.getCountry());
            logger.info("nDays: " + touristVoucher.getDays());
            logger.info("nNights: " + touristVoucher.getNights());
            logger.info("Transport: " + touristVoucher.getTransport());
            logger.info("Hotel");
            logger.info(" Name: " + touristVoucher.getHotel().getName());
            logger.info(" Rating: " + touristVoucher.getHotel().getRating());
            logger.info(" Apartment");
            logger.info("  idNumber: " + touristVoucher.getHotel().getApartment().getIdNumber());
            logger.info("  nPersons: " + touristVoucher.getHotel().getApartment().getnPersons());
            logger.info(" Services");
            logger.info("  Meal: " + touristVoucher.getHotel().getServices().isMeal());
            logger.info("  TV: " + touristVoucher.getHotel().getServices().isTV());
            logger.info("  Conditioner: " + touristVoucher.getHotel().getServices().isConditioner());
        });
    }
}
