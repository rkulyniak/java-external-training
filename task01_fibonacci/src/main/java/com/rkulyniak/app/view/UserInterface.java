package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.Arrays;
import java.util.Scanner;

/**
 * The {@code UserInterface} class displays data from the model to the user
 * and also enables them to modify the data.
 */
public class UserInterface {

    /**
     * The scanner object for keyboard input.
     */
    private Scanner input;
    /**
     * The controller object.
     */
    private Controller controller;

    /**
     * Constructs a new object.
     */
    public UserInterface() {
        input = new Scanner(System.in);
        controller = new Controller();
    }

    /**
     * This method prints the application main menu for user.
     * According to the selected item calls the method to generate
     * the request to the controller.
     */
    public void launch() {
        String userInput;

        do {
            System.out.println("\n---------------------------------------------------------");
            System.out.println("1. Input the interval");
            System.out.println("2. Print odd and even numbers");
            System.out.println("3. Print the sum of odd and even numbers");
            System.out.println("4. Build Fibonacci sequence");
            System.out.println("5. Print percentage of odd and even Fibonacci numbers");
            System.out.println("6. Exit");

            System.out.print("\nEnter your choice: ");
            userInput = input.nextLine();
            System.out.println();

            switch (userInput) {
                case "1":
                    makeSetNumbersRequest();
                    break;
                case "2":
                    makeGetOddNumsRequest();
                    makeGetEvenNumsRequest();
                    break;
                case "3":
                    makeGetSumOfOddNumsRequest();
                    makeGetSumOfEvenNumsRequest();
                    break;
                case "4":
                    makeBuildFibonacciSequenceRequest();
                    break;
                case "5":
                    makeGetPercentageOfOddFibonacciNumsRequest();
                    makeGetPercentageOfEvenFibonacciNumsRequest();
                    break;
                case "6":
                    System.out.println("Bye!");
                    break;
                default:
                    System.out.println("invalid input; please re-enter");
            }
        } while (!userInput.equals("6"));
    }

    /**
     * This method makes request for setting the array of numbers.
     */
    private void makeSetNumbersRequest() {
        int tail = inputInteger("Tail: ");
        int head = inputInteger("Head: ");

        int[] numbers = controller.setNumbers(tail, head);

        if (numbers.length != 0) {
            System.out.println("\nNumbers: " + Arrays.toString(numbers));
        } else {
            System.out.println("\nunable to set array of numbers");
        }
    }

    /**
     * This method makes request for getting odd numbers from array.
     */
    private void makeGetOddNumsRequest() {
        int[] oddNumbers = controller.getOddNumbers();
        System.out.println("Odd numbers: " + Arrays.toString(oddNumbers));
    }

    /**
     * This method makes request for getting even numbers from array.
     */
    private void makeGetEvenNumsRequest() {
        int[] evenNumbers = controller.getEvenNumbers();
        System.out.println("Even numbers: " + Arrays.toString(evenNumbers));
    }

    /**
     * This method makes request for getting the sum of odd numbers.
     */
    private void makeGetSumOfOddNumsRequest() {
        int sum = controller.getSumOfOddNumbers();
        System.out.println("Sum of odd numbers: " + sum);
    }

    /**
     * This method makes request for getting the sum of even numbers.
     */
    private void makeGetSumOfEvenNumsRequest() {
        int sum = controller.getSumOfEvenNumbers();
        System.out.println("Sum of even numbers: " + sum);
    }

    /**
     * This method makes request for building Fibonacci sequence.
     */
    private void makeBuildFibonacciSequenceRequest() {
        int N = inputInteger("Size of set: ");

        int[] fibonacciSequence = controller.buildFibonacciSequence(N);

        if (fibonacciSequence.length != 0) {
            System.out.println("\nFibonacci sequence: " + Arrays.toString(fibonacciSequence));
        } else {
            System.out.println("\nunable to build fibonacci sequence");
        }
    }

    /**
     * This method makes request for getting percentage of odd Fibonacci numbers.
     */
    private void makeGetPercentageOfOddFibonacciNumsRequest() {
        double percentage = controller.getPercentageOfOddFibonacciNumbers();
        System.out.printf("Percentage of odd fibonacci numbers: %.2f%n", percentage);
    }

    /**
     * This method makes request for getting percentage of even Fibonacci numbers.
     */
    private void makeGetPercentageOfEvenFibonacciNumsRequest() {
        double percentage = controller.getPercentageOfEvenFibonacciNumbers();
        System.out.printf("Percentage of even fibonacci numbers: %.2f%n", percentage);
    }

    /**
     * This method provides functionality to input an integer number.
     *
     * @param message string message which prints before inputting
     * @return integer number
     */
    private int inputInteger(String message) {
        int number = 0;

        do {
            System.out.print(message);
            try {
                number = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
                continue;
            }
            if (!isNumberValid(number)) {
                System.out.println("invalid value; please re-enter");
            }
        } while (!isNumberValid(number));

        return number;
    }

    /**
     * This method checks if the number is valid or not.
     *
     * @param number number which will be checked
     * @return {@code true} if number is valid; {@code false} if number isn't valid.
     */
    private boolean isNumberValid(int number) {
        return number > 0;
    }
}
