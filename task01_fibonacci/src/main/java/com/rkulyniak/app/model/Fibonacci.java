package com.rkulyniak.app.model;

/**
 * The {@code Fibonacci} class provides methods for handling numbers in Fibonacci sequence
 * according to the conditions of the task.
 */
class Fibonacci {

    /**
     * The Fibonacci sequence.
     */
    private int[] sequence;

    /**
     * Percentage of odd numbers in sequence.
     */
    private double percentageOfOddNumbers;
    /**
     * Percentage of even numbers in sequence.
     */
    private double percentageOfEvenNumbers;

    /**
     * Constructs a new object.
     */
    Fibonacci() {
        sequence = new int[0];

        percentageOfOddNumbers = 0.0;
        percentageOfEvenNumbers = 0.0;
    }

    /**
     * This method builds the Fibonacci sequence.
     *
     * @param F1 first number in sequence
     * @param F2 second number in sequence
     * @param N  length of sequence
     */
    void buildSequence(int F1, int F2, int N) {
        sequence = new int[N];

        sequence[0] = F1;
        sequence[1] = F2;

        for (int i = 2; i < sequence.length; i++) {
            int F3 = F1 + F2;
            sequence[i] = F3;

            F1 = F2;
            F2 = F3;
        }
        calculatePercentageOfOddAndEvenNumbers();
    }

    /**
     * This method returns the Fibonacci sequence.
     *
     * @return the Fibonacci sequence
     */
    int[] getSequence() {
        return sequence;
    }

    /**
     * This method calculates percentage of odd and even numbers.
     */
    private void calculatePercentageOfOddAndEvenNumbers() {
        int countOfOddNumbers = 0;
        int countOfEvenNumbers = 0;

        for (int number : sequence) {
            if (number % 2 != 0) {
                countOfOddNumbers++;
            } else {
                countOfEvenNumbers++;
            }
        }

        percentageOfOddNumbers = (double) (countOfOddNumbers * 100) / sequence.length;
        percentageOfEvenNumbers = (double) (countOfEvenNumbers * 100) / sequence.length;
    }

    /**
     * This method return percentage of odd numbers.
     *
     * @return percentage of odd numbers
     */
    double getPercentageOfOddNumbers() {
        return percentageOfOddNumbers;
    }

    /**
     * This method return percentage of even numbers.
     *
     * @return percentage of even numbers
     */
    double getPercentageOfEvenNumbers() {
        return percentageOfEvenNumbers;
    }
}
