package com.rkulyniak.app.model;

import java.util.Arrays;

/**
 * The {@code Task} class provides methods for handling numbers according to the conditions of the task.
 */
public class Task {

    /**
     * The array of numbers.
     */
    private int[] numbers;
    /**
     * The fibonacci object;
     */
    private Fibonacci fibonacci;

    /**
     * Constructs a new object.
     */
    public Task() {
        numbers = new int[0];
        fibonacci = new Fibonacci();
    }

    /**
     * This method checks if tail and head are valid or not;
     *
     * @param tail tail number
     * @param head head number
     * @return {@code true} if tail and head are valid; {@code false} if tail and head aren't valid.
     */
    private boolean isTailAndHeadValid(int tail, int head) {
        return (tail > 0) && ((tail + 1) < head);
    }

    /**
     * This method fills array.
     *
     * @param tail tail number
     * @param head head number
     */
    public void setNumbers(int tail, int head) {
        if (!isTailAndHeadValid(tail, head)) {
            return;
        }

        int size = (head - tail) + 1;
        numbers = new int[size];

        for (int i = 0, j = tail; i < numbers.length; i++, j++) {
            numbers[i] = j;
        }
    }

    /**
     * This method returns the array of numbers.
     *
     * @return the array of numbers
     */
    public int[] getNumbers() {
        return Arrays.copyOf(numbers, numbers.length);
    }

    /**
     * This method returns the array of odd numbers.
     *
     * @return the array of odd numbers
     */
    public int[] getOddNumbers() {
        int size = getCountOfOddNumbers();
        int[] oddNumbers = new int[size];

        if (oddNumbers.length == 0) {
            return oddNumbers;
        }

        int firstOddNumIndex = numbers[0] % 2 != 0 ? 0 : 1;
        for (int i = 0, j = firstOddNumIndex; i < oddNumbers.length; i++, j += 2) {
            oddNumbers[i] = numbers[j];
        }

        return oddNumbers;
    }

    /**
     * This method counts and return count of odd numbers.
     *
     * @return count of even numbers
     */
    private int getCountOfOddNumbers() {
        if (numbers.length % 2 == 0) {
            return numbers.length / 2;
        }

        return numbers[0] % 2 != 0 ? (numbers.length / 2 + 1) : (numbers.length / 2);
    }

    /**
     * This method returns the array of even numbers.
     *
     * @return the array of even numbers
     */
    public int[] getEvenNumbers() {
        int size = getCountOfEvenNumbers();
        int[] evenNumbers = new int[size];

        if (evenNumbers.length == 0) {
            return evenNumbers;
        }

        int firstEvenNumIndex = numbers[0] % 2 == 0 ? 0 : 1;
        for (int i = 0, j = firstEvenNumIndex; i < evenNumbers.length; i++, j += 2) {
            evenNumbers[i] = numbers[j];
        }

        return evenNumbers;
    }

    /**
     * This method counts and return count of even numbers.
     *
     * @return count of even numbers
     */
    private int getCountOfEvenNumbers() {
        if (numbers.length % 2 == 0) {
            return numbers.length / 2;
        }

        return numbers[0] % 2 == 0 ? (numbers.length / 2 + 1) : (numbers.length / 2);
    }

    /**
     * This method calculates and returns the sum of odd numbers.
     *
     * @return the sum of odd numbers
     */
    public int getSumOfOddNumbers() {
        int[] oddNumbers = getOddNumbers();
        int sum = 0;

        for (int oddNumber : oddNumbers) {
            sum += oddNumber;
        }

        return sum;
    }

    /**
     * This method calculates and returns the sum of even numbers.
     *
     * @return the sum of even numbers
     */
    public int getSumOfEvenNumbers() {
        int[] evenNumbers = getEvenNumbers();
        int sum = 0;

        for (int evenNumber : evenNumbers) {
            sum += evenNumber;
        }

        return sum;
    }

    /**
     * This method checks if the length of the Fibonacci sequence is valid or not.
     *
     * @param N length of sequence
     * @return {@code true} if length is valid; {@code false} if length isn't valid.
     */
    private boolean isLengthOfFibonacciSqnValid(int N) {
        return N > 2;
    }

    /**
     * This method builds the Fibonacci sequence.
     *
     * @param N length of sequence
     */
    public void buildFibonacciSequence(int N) {
        if (numbers.length == 0 || !isLengthOfFibonacciSqnValid(N)) {
            return;
        }

        int F1 = getBiggestOddNumber();
        int F2 = getBiggestEvenNumber();

        fibonacci.buildSequence(F1, F2, N);
    }

    /**
     * This method returns the Fibonacci sequence.
     *
     * @return the Fibonacci sequence
     */
    public int[] getFibonacciSequence() {
        return fibonacci.getSequence();
    }

    /**
     * This method calculates and returns the biggest odd Fibonacci number.
     *
     * @return the biggest odd Fibonacci number
     */
    private int getBiggestOddNumber() {
        return numbers[numbers.length - 1] % 2 != 0 ?
                numbers[numbers.length - 1] : numbers[numbers.length - 2];
    }

    /**
     * This method calculates and returns the biggest even Fibonacci number.
     *
     * @return the biggest even Fibonacci number
     */
    private int getBiggestEvenNumber() {
        return numbers[numbers.length - 1] % 2 == 0 ?
                numbers[numbers.length - 1] : numbers[numbers.length - 2];
    }

    /**
     * This method returns percentage of odd Fibonacci numbers.
     *
     * @return percentage of odd Fibonacci numbers
     */
    public double getPercentageOfOddFibonacciNumbers() {
        return fibonacci.getPercentageOfOddNumbers();
    }

    /**
     * This method returns percentage of even Fibonacci numbers.
     *
     * @return percentage of even Fibonacci numbers
     */
    public double getPercentageOfEvenFibonacciNumbers() {
        return fibonacci.getPercentageOfEvenNumbers();
    }
}
