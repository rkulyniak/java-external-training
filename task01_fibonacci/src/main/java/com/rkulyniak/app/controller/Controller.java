package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.Task;

/**
 * The {@code Controller} class is responsible for controlling the flow of the application execution.
 */
public class Controller {

    /**
     * The task object.
     */
    private Task task;

    /**
     * Constructs a new object.
     */
    public Controller() {
        task = new Task();
    }

    /**
     * This method handles request for setting the array of numbers.
     *
     * @param tail tail number
     * @param head head number
     * @return the response to the request
     */
    public int[] setNumbers(int tail, int head) {
        task.setNumbers(tail, head);
        return task.getNumbers();
    }

    /**
     * This method handles request for getting odd numbers from array.
     *
     * @return the response to the request
     */
    public int[] getOddNumbers() {
        return task.getOddNumbers();
    }

    /**
     * This method handle request for getting even numbers from array.
     *
     * @return the response to the request
     */
    public int[] getEvenNumbers() {
        return task.getEvenNumbers();
    }

    /**
     * This method handles request for getting the sum of odd numbers.
     *
     * @return the response to the request
     */
    public int getSumOfOddNumbers() {
        return task.getSumOfOddNumbers();
    }

    /**
     * This method makes request for getting the sum of even numbers.
     *
     * @return the response to the request
     */
    public int getSumOfEvenNumbers() {
        return task.getSumOfEvenNumbers();
    }

    /**
     * This method handles request for building Fibonacci sequence.
     *
     * @param N length of the Fibonacci sequence
     * @return the response to the request
     */
    public int[] buildFibonacciSequence(int N) {
        task.buildFibonacciSequence(N);
        return task.getFibonacciSequence();
    }

    /**
     * This method handles request for getting percentage of odd Fibonacci numbers.
     *
     * @return the response to the request
     */
    public double getPercentageOfOddFibonacciNumbers() {
        return task.getPercentageOfOddFibonacciNumbers();
    }

    /**
     * This method handles request for getting percentage of even Fibonacci numbers.
     *
     * @return the response to the request
     */
    public double getPercentageOfEvenFibonacciNumbers() {
        return task.getPercentageOfEvenFibonacciNumbers();
    }
}
