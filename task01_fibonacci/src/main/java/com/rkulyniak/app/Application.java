package com.rkulyniak.app;

import com.rkulyniak.app.view.UserInterface;

/**
 * The {@code Application} class contains entry point method
 * for starting a program's execution.
 */
public class Application {

    /**
     * This method is the entry point of java program.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        new UserInterface().launch();
    }
}
