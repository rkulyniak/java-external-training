#1
select * from PC where model like '%1%1%';

#2
select * from Outcome where MONTH(date) = 3;

#3
select * from Outcome_o where DAY(date) = 14;

#4
select name
from Ships
where name rlike '^W'
  and name rlike 'n$';

#5
select name
from Ships
where name like '%e%e';

#6
select name, launched
from Ships
where name not rlike 'a$';

#7
select name
from Battles
where name like '% %'
  and name not rlike 'c$';

#8
select *
from Trip
where HOUR(time_out) >= 12
  and HOUR(time_out) <= 17;

#9
select *
from Trip
where HOUR(time_in) >= 17
  and HOUR(time_in) <= 23;

#10
select date
from Pass_in_trip
where place rlike '^1';

#11
select date
from Pass_in_trip
where place rlike 'c$';

#12
select name
from Passenger
where name like '% C%';

#13
select name
from Passenger
where name not like '% J%';
