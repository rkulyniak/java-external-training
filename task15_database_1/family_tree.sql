create table person_gender
(
    gender varchar(10),
    primary key (gender)
);

create table family_tree
(
    id                 int auto_increment,
    first_name         varchar(30) not null,
    last_name          varchar(30) not null,
    gender             varchar(10) not null,
    birth_date         date        not null,
    death_date         date,
    birth_place        varchar(30) not null,
    death_place        varchar(30),
    credit_card_number char(16),
    family_tree_id     int,
    primary key (id),
    foreign key (gender) references person_gender (gender),
    foreign key (family_tree_id) references family_tree (id)
);

create table family_satellite
(
    family_tree_id int,
    first_name     varchar(30) not null,
    last_name      varchar(30) not null,
    gender         varchar(10) not null,
    birth_date     date        not null,
    death_date     date,
    marriage_date  date        not null,
    birth_place    varchar(30) not null,
    death_place    varchar(30),
    unique key (family_tree_id),
    foreign key (family_tree_id) references family_tree (id),
    foreign key (gender) references person_gender (gender)
);

create table family_value
(
    id               int auto_increment,
    value_name       varchar(50) not null,
    approximate_cost double,
    max_cost         double,
    min_cost         double,
    catalog_code     int,
    primary key (id)
);

create table family_tree_family_value
(
    id              int auto_increment,
    family_tree_id  int not null,
    family_value_id int not null,
    primary key (id),
    foreign key (family_tree_id) references family_tree (id),
    foreign key (family_value_id) references family_value (id)
);
