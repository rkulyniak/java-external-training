#1
select maker, type, speed, hd from PC
join Product P on PC.model = P.model
where hd <= 8;

#2
select DISTINCT maker
from PC
         join Product P on PC.model = P.model
where speed >= 600;

#3
select DISTINCT maker
from Laptop
         join Product P on Laptop.model = P.model
where speed <= 500;

#4
select L1.model as 'L1 model', L2.model as 'L2 model', L1.hd as 'hd', L1.ram as 'L1 ram', L2.ram as 'L2 ram'
from Laptop L1,
     Laptop L2
where L1.hd = L2.hd
  and L1.code > L2.code;

#5
select country, type
from Classes
where type = 'bb'
   or type = 'bc'
order by type;

#6
select distinct P.model, maker
from PC
         join Product P on PC.model = P.model
where price < 600;

#7
select distinct P.model, maker from Printer
join Product P on Printer.model = P.model
where price > 300;

#8
select maker, P.model, price
from PC
         join Product P on PC.model = P.model
union
select maker, P2.model, price
from Laptop
         join Product P2 on Laptop.model = P2.model;

#9
select maker, P.model, price from PC
join Product P on PC.model = P.model;

#10
select maker, type, P.model, speed
from Laptop
         join Product P on Laptop.model = P.model
where speed > 600;

#11
select name, launched
from Ships;

#12
select ship, battle, date
from Outcomes
         join Battles B on Outcomes.battle = B.name
where result = 'OK';

#13
select name, country
from Ships
         join Classes C on Ships.class = C.class;

#14
select plane, name
from Trip
        join Company C on Trip.ID_comp = C.ID_comp
where plane = 'Boeing';

#15
select name, date
from Passenger
         join Pass_in_trip Pit on Passenger.ID_psg = Pit.ID_psg;
