# 1
select maker, type
from Product
where type = 'Laptop'
order by maker;

#2
select model, ram, screen, price
from Laptop
where price > 1000
order by ram asc, price desc;

#3
select *
from Printer
where color = 'y'
order by price desc;

#4
select model, speed, hd, cd, price
from PC
where (cd = '12x' or cd = '24x')
  and (price < 600)
order by speed desc;

#5
select name, class
from Ships
order by name;

#6
select *
from PC
where speed >= 500
  and price < 800
order by price desc;

#7
select *
from Printer
where type != 'Matrix'
  and price < 300
order by type desc;

#8
select model, speed
from PC
where price >= 400
  and price <= 600
order by hd;

#9
select PC.model, speed, hd
from PC
         join Product P on PC.model = P.model
where (hd = 10
    or hd = 20)
  and maker = 'A'
order by speed;

#10
select model, speed, hd, price
from Laptop
where screen >= 12
order by price desc;

#11
select model, type, price
from Printer
where price >= 300
order by type desc;

#12
select model, ram, price
from Laptop
where ram = 64
order by screen;

#13
select model, ram, price
from PC
where ram > 64
order by hd;

#14
select model, speed, price
from PC
where speed >= 500
  and speed <= 750
order by hd desc;

#15
select *
from Outcome_o
where `out` > 2000
order by date desc;

#16
select *
from Income_o
where inc >= 5000
  and inc <= 10000
order by inc;

#17
select *
from Income
where point = 1
order by inc;

#18
select *
from Outcome
where point = 2
order by `out`;

#19
select *
from Ships
         join Classes C on Ships.class = C.class
where country = 'Japan'
order by type desc;

#20
select name, launched
from Ships
where launched >= 1920
  and launched <= 1942
order by launched desc;

#21
select ship, battle
from Battles
         join Outcomes O on Battles.name = O.battle
where battle = 'Guadalcanal'
  and result != 'sunk'
order by ship desc;

#22
select ship, battle, result
from Battles
         join Outcomes O on Battles.name = O.battle
where result = 'sunk'
order by ship desc;

#23
select class, displacement
from Classes
where displacement >= 40000
order by type;

#24
select trip_no, town_from, town_to
from Trip
where town_from = 'London'
order by time_out;

#25
select trip_no, town_from, town_to
from Trip
where plane = 'TU-134'
order by time_out desc;

#26
select trip_no, town_from, town_to
from Trip
where plane = 'IL-86'
order by plane;

#27
select trip_no, town_from, town_to
from Trip
where town_from != 'Rostov' and town_to != 'Rostov'
order by plane;
