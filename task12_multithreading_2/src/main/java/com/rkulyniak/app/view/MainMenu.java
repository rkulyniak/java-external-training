package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Money transferring service");
            put("2", "2 - Blocking Queue");
            put("3", "3 - Read/Write lock implementation");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.startMoneyTransferringService());
            put("2", () -> controller.startBlockingQueueCommunication());
            put("3", () -> {});
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
