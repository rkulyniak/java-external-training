package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.BankAccount;
import com.rkulyniak.app.model.BlockingQueueCommunication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    public void startMoneyTransferringService() {
        BankAccount bankAccount = new BankAccount();
        double balance;

        balance = bankAccount.runTransferringServices();
        logger.info("bank account after running transferring services = " + balance);
    }

    public void startBlockingQueueCommunication() {
        BlockingQueueCommunication bqCommunication = new BlockingQueueCommunication();
        bqCommunication.startCommunication();
        logger.info("");
    }
}
