package com.rkulyniak.app.model;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {

    private static double balance = 0;
    private final Lock lock = new ReentrantLock();

    private void deposit(double amount) {
        lock.lock();
        balance += amount;
        lock.unlock();
    }

    private void withdraw(double amount) {
        lock.lock();
        balance -= amount;
        lock.unlock();
    }

    public double runTransferringServices() {
        Thread t1 = null;
        Thread t2 = null;

        for (int i = 0; i < 100_000; i++) {
            t1 = new Thread(() -> deposit(100));
            t2 = new Thread(() -> withdraw(100));
            t1.start();
            t2.start();
        }

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException ignored) { }

        return balance;
    }
}
