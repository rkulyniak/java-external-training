package com.rkulyniak.app.model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueueCommunication {

    private BlockingQueue<Character> blockingQueue;
    private Thread sendThread;
    private Thread receiveThread;

    public BlockingQueueCommunication() {
        blockingQueue = new PriorityBlockingQueue<>();
        sendThread = new Thread(this::send);
        receiveThread = new Thread(this::receive);
    }

    public void startCommunication() {
        sendThread.start();
        receiveThread.start();

        try {
            sendThread.join();

            receiveThread.interrupt();
            receiveThread.join();
        } catch (InterruptedException ignored) {}
    }

    private void send() {
        try {
            for (char i = 'A'; i <= 'Z'; i++) {
                Thread.sleep(250);
                blockingQueue.put(i);
                System.out.println("sent " + i);
            }
        } catch (InterruptedException ignored) { }
        System.out.println("sender has finished work");
    }

    private void receive() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Thread.sleep(250);
                System.out.println("received " + blockingQueue.take());
            }
        } catch (InterruptedException ignored) { }
        System.out.println("receiver has finished work");
    }
}
