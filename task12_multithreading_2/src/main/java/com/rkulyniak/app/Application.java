package com.rkulyniak.app;

import com.rkulyniak.app.controller.Controller;
import com.rkulyniak.app.view.MainMenu;
import com.rkulyniak.app.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Controller controller = new Controller();
        Menu mainMenu = new MainMenu(controller);

        logger.info("the application has started execution");
        mainMenu.launch();
        logger.info("the application has finished execution");
    }
}
