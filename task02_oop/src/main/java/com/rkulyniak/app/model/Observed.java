package com.rkulyniak.app.model;

import com.rkulyniak.app.view.Observer;

public interface Observed {

    void addSubscriber(Observer subscriber);

    void removeSubscriber(Observer subscriber);

    void notifySubscribers(String message);
}
