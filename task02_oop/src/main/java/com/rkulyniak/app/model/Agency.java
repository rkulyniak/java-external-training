package com.rkulyniak.app.model;

import com.rkulyniak.app.view.Observer;

import java.util.ArrayList;
import java.util.List;

public class Agency implements Observed {

    private List<Party> parties;
    private List<Observer> subscribers;

    public Agency() {
        parties = new ArrayList<>();
        subscribers = new ArrayList<>();
    }

    public List<Party> getParties() {
        return new ArrayList<>(parties);
    }

    public List<String> getPartiesInfo() {
        List<String> partiesInfo = new ArrayList<>();

        for (Party party : parties) {
            partiesInfo.add(party.toString());
        }

        return partiesInfo;
    }

    static List<String> partiesListToPartiesInfoList(List<Party> parties) {
        List<String> partiesInfo = new ArrayList<>();

        for (Party party : parties) {
            partiesInfo.add(party.toString());
        }

        return partiesInfo;
    }

    public void addParty(Party party) {
        parties.add(party);
        notifySubscribers("added new party [id:" + party.getId() + "]");
    }

    public void updateParty(int id, Party updatedParty) {
        if (!doesPartyExist(id)) {
            notifySubscribers("Unable to update party\nParty [id:" + id + "] doesn't exist");
            return;
        }

        for (Party party : parties) {
            if (party.getId() == id) {
                party.setTitle(updatedParty.getTitle());
                party.setLocation(updatedParty.getLocation());
                party.setDuration(updatedParty.getDuration());
                party.setPrice(updatedParty.getPrice());
                break;
            }
        }
        notifySubscribers("party [id:" + id + "] has just been updated");
    }

    public void removeParty(int id) {
        if (!doesPartyExist(id)) {
            notifySubscribers("Unable to remove party\nParty [id:" + id + "] doesn't exist");
            return;
        }

        for (Party party : parties) {
            if (party.getId() == id) {
                parties.remove(party);
                break;
            }
        }
        notifySubscribers("party [id:" + id + "] is deleted");
    }

    public boolean doesPartyExist(int id) {
        for (Party party : parties) {
            if (party.getId() == id) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void addSubscriber(Observer subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void removeSubscriber(Observer subscriber) {
        subscribers.remove(subscriber);
    }

    @Override
    public void notifySubscribers(String message) {
        subscribers.forEach(subscriber -> subscriber.printUpdateMessage(message));
    }
}
