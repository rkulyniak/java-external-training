package com.rkulyniak.app.model;

import java.util.ArrayList;
import java.util.List;

public class Filter {

    public List<String> filterPartiesWithIdCondition(int id, List<Party> parties) {
        List<Party> filteredParties = new ArrayList<>();

        for (Party party : parties) {
            if (party.getId() == id) {
                filteredParties.add(party);
            }
        }

        return Agency.partiesListToPartiesInfoList(filteredParties);
    }

    public List<String> filterPartiesWithTitleCondition(String title, List<Party> parties) {
        List<Party> filteredParties = new ArrayList<>();

        for (Party party : parties) {
            if (party.getTitle().equals(title)) {
                filteredParties.add(party);
            }
        }

        return Agency.partiesListToPartiesInfoList(filteredParties);
    }

    public List<String> filterPartiesWithLocationCondition(String location, List<Party> parties) {
        List<Party> filteredParties = new ArrayList<>();

        for (Party party : parties) {
            if (party.getLocation().equals(location)) {
                filteredParties.add(party);
            }
        }

        return Agency.partiesListToPartiesInfoList(filteredParties);
    }

    public List<String> filterPartiesWithDurationCondition(double duration, List<Party> parties) {
        List<Party> filteredParties = new ArrayList<>();

        for (Party party : parties) {
            if (party.getDuration() == duration) {
                filteredParties.add(party);
            }
        }

        return Agency.partiesListToPartiesInfoList(filteredParties);
    }

    public List<String> filterPartiesWithPriceCondition(double price, List<Party> parties) {
        List<Party> filteredParties = new ArrayList<>();

        for (Party party : parties) {
            if (party.getPrice() == price) {
                filteredParties.add(party);
            }
        }

        return Agency.partiesListToPartiesInfoList(filteredParties);
    }
}
