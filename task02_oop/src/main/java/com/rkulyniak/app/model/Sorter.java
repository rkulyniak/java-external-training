package com.rkulyniak.app.model;

import java.util.Comparator;
import java.util.List;

public class Sorter {

    public List<String> sortPartiesById(List<Party> parties) {
        parties.sort(Comparator.comparingInt(Party::getId));
        return Agency.partiesListToPartiesInfoList(parties);
    }

    public List<String> sortPartiesByTitle(List<Party> parties) {
        parties.sort(Comparator.comparing(Party::getTitle));
        return Agency.partiesListToPartiesInfoList(parties);
    }

    public List<String> sortPartiesByLocation(List<Party> parties) {
        parties.sort(Comparator.comparing(Party::getLocation));
        return Agency.partiesListToPartiesInfoList(parties);
    }

    public List<String> sortPartiesByDuration(List<Party> parties) {
        parties.sort(Comparator.comparingDouble(Party::getDuration));
        return Agency.partiesListToPartiesInfoList(parties);
    }

    public List<String> sortPartiesByPrice(List<Party> parties) {
        parties.sort(Comparator.comparingDouble(Party::getPrice));
        return Agency.partiesListToPartiesInfoList(parties);
    }
}
