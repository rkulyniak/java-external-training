package com.rkulyniak.app.model;

public class Party {

    private static int partyCounter = 0;

    private int id;
    private String title;

    private String location;
    private double duration;
    private double price;

    public Party(String title, String location, double duration, double price) {
        this.id = ++partyCounter;
        this.title = title;

        this.location = location;
        this.duration = duration;
        this.price = price;
    }

    int getId() {
        return id;
    }

    String getTitle() {
        return title;
    }

    String getLocation() {
        return location;
    }

    double getDuration() {
        return duration;
    }

    double getPrice() {
        return price;
    }

    void setTitle(String title) {
        this.title = title;
    }

    void setLocation(String location) {
        this.location = location;
    }

    void setDuration(double duration) {
        this.duration = duration;
    }

    void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ID: " + id + "\n" +
                "Title: " + title + "\n" +
                "Location: " + location + "\n" +
                "Duration: " + duration + "\n" +
                "Price: " + price;
    }

    @Override
    public boolean equals(Object otherObj) {
        if (this == otherObj) return true;
        if (otherObj == null) return false;
        if (getClass() != otherObj.getClass()) return false;

        Party obj = (Party) otherObj;
        return id == obj.id &&
                title.equals(obj.title) &&
                location.equals(obj.location) &&
                duration == obj.duration &&
                price == obj.price;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(id) +
                title.hashCode() +
                location.hashCode() +
                Double.hashCode(duration) +
                Double.hashCode(price);
    }
}
