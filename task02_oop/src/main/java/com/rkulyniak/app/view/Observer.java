package com.rkulyniak.app.view;

public interface Observer {

    void printUpdateMessage(String message);
}
