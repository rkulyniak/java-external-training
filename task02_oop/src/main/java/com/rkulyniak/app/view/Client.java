package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.ClientController;

import java.util.List;

class Client extends UserInterface {

    private ClientController controller;

    Client(ClientController controller) {
        this.controller = controller;
    }

    void startClientMenu() {
        String userInput;

        do {
            System.out.println("\n-----------------------------------------");
            System.out.println("1. Print parties info");
            System.out.println("2. Filter the parties");
            System.out.println("3. LogOut");

            System.out.print("\nEnter your choice: ");
            userInput = UserInterface.inputLine();

            switch (userInput) {
                case "1":
                case "print":
                    startPrintingMenu();
                    break;
                case "2":
                case "select":
                    startSelectingMenu();
                    break;
                case "3":
                case "logout":
                    break;
                default:
                    System.out.println("\ninvalid input; please re-enter");
            }
        } while (!userInput.equals("3") && !userInput.equals("logout"));
    }

    private void startPrintingMenu() {
        String userInput;

        do {
            System.out.println("\nChoose a way to sort parties");

            printPartyAttributes();

            System.out.print("\nEnter your choice: ");
            userInput = UserInterface.inputLine();

            switch (userInput) {
                case "1":
                case "id":
                    makePrintRequestWithSortingById();
                    break;
                case "2":
                case "title":
                    makePrintRequestWithSortingByTitle();
                    break;
                case "3":
                case "location":
                    makePrintRequestWithSortingByLocation();
                    break;
                case "4":
                case "duration":
                    makePrintRequestWithSortingByDuration();
                    break;
                case "5":
                case "price":
                    makePrintRequestWithSortingByPrice();
                    break;
                case "6":
                case "back":
                    break;
                default:
                    System.out.println("\ninvalid input; please re-enter");
            }
        } while (!userInput.equals("6") && !userInput.equals("back"));
    }

    private void makePrintRequestWithSortingById() {
        List<String> partiesInfo = controller.getPartiesInfoSortedById();
        printPartiesInfo(partiesInfo);
    }

    private void makePrintRequestWithSortingByTitle() {
        List<String> partiesInfo = controller.getPartiesInfoSortedByTitle();
        printPartiesInfo(partiesInfo);
    }

    private void makePrintRequestWithSortingByLocation() {
        List<String> partiesInfo = controller.getPartiesInfoSortedByLocation();
        printPartiesInfo(partiesInfo);
    }

    private void makePrintRequestWithSortingByDuration() {
        List<String> partiesInfo = controller.getPartiesInfoSortedByDuration();
        printPartiesInfo(partiesInfo);
    }

    private void makePrintRequestWithSortingByPrice() {
        List<String> partiesInfo = controller.getPartiesInfoSortedByPrice();
        printPartiesInfo(partiesInfo);
    }

    private void startSelectingMenu() {
        String userInput;

        do {
            System.out.println("\nSelect the party attribute for making condition");

            printPartyAttributes();

            System.out.print("\nEnter your choice: ");
            userInput = UserInterface.inputLine();

            switch (userInput) {
                case "1":
                case "id":
                    makeSelectRequestWithIdCondition();
                    break;
                case "2":
                case "title":
                    makeSelectRequestWithTitleCondition();
                    break;
                case "3":
                case "location":
                    makeSelectRequestWithLocationCondition();
                    break;
                case "4":
                case "duration":
                    makeSelectRequestWithDurationCondition();
                    break;
                case "5":
                case "price":
                    makeSelectRequestWithPriceCondition();
                    break;
                case "6":
                case "back":
                    break;
                default:
                    System.out.println("\ninvalid input; please re-enter");
            }
        } while (!userInput.equals("6") && !userInput.equals("back"));
    }

    private void makeSelectRequestWithIdCondition() {
        int id = UserInterface.inputInteger("\nID: ");
        List<String> partiesInfo = controller.getPartiesInfoWithIdCondition(id);
        printPartiesInfo(partiesInfo);
    }

    private void makeSelectRequestWithTitleCondition() {
        String title = UserInterface.inputLine("\nTitle: ");
        List<String> partiesInfo = controller.getPartiesInfoWithTitleCondition(title);
        printPartiesInfo(partiesInfo);
    }

    private void makeSelectRequestWithLocationCondition() {
        String location = UserInterface.inputLine("\nLocation: ");
        List<String> partiesInfo = controller.getPartiesInfoWithLocationCondition(location);
        printPartiesInfo(partiesInfo);
    }

    private void makeSelectRequestWithDurationCondition() {
        double duration = UserInterface.inputDouble("\nDuration: ");
        List<String> partiesInfo = controller.getPartiesInfoWithDurationCondition(duration);
        printPartiesInfo(partiesInfo);
    }

    private void makeSelectRequestWithPriceCondition() {
        double price = UserInterface.inputDouble("\nPrice: ");
        List<String> partiesInfo = controller.getPartiesInfoWithPriceCondition(price);
        printPartiesInfo(partiesInfo);
    }

    private void printPartyAttributes() {
        System.out.println();

        System.out.println("1. id");
        System.out.println("2. title");
        System.out.println("3. location");
        System.out.println("4. duration");
        System.out.println("5. price");
        System.out.println("6. go back");
    }
}
