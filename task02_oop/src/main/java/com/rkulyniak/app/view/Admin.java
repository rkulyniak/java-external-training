package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.AdminController;
import com.rkulyniak.app.model.Party;

import java.util.List;

class Admin implements Observer {

    private AdminController controller;

    Admin(AdminController controller) {
        this.controller = controller;
    }

    @Override
    public void printUpdateMessage(String message) {
        System.out.println("\n[UPDATE] " + message);
    }

    void startAdminMenu() {
        String userInput;

        do {
            System.out.println("\n-----------------------------------------");
            System.out.println("1. Print parties info");
            System.out.println("2. Create party");
            System.out.println("3. Update party");
            System.out.println("4. Delete party");
            System.out.println("5. LogOut");

            System.out.print("\nEnter your choice: ");
            userInput = UserInterface.inputLine();

            switch (userInput) {
                case "1":
                case "print":
                    makePrintRequest();
                    break;
                case "2":
                case "create":
                    makeCreateRequest();
                    break;
                case "3":
                case "update":
                    makeUpdateRequest();
                    break;
                case "4":
                case "delete":
                    makeDeleteRequest();
                    break;
                case "5":
                case "logout":
                    break;
                default:
                    System.out.println("\ninvalid input; please re-enter");
            }
        } while (!userInput.equals("5") && !userInput.equals("logout"));
    }

    private void makePrintRequest() {
        List<String> partiesInfo = controller.getPartiesInfo();
        UserInterface.printPartiesInfo(partiesInfo);
    }

    private void makeCreateRequest() {
        String title = UserInterface.inputLine("\nTitle: ");

        String location = UserInterface.inputLine("Location: ");
        double duration = UserInterface.inputDouble("Duration: ");
        double price = UserInterface.inputDouble("Price: ");

        controller.createParty(new Party(title, location, duration, price));
    }

    private void makeUpdateRequest() {
        int id = UserInterface.inputInteger("\nID: ");
        if (!controller.doesPartyExist(id)) {
            System.out.println("\nparty [id:" + id + "] doesn't exist");
            return;
        }

        String title = UserInterface.inputLine("Title: ");

        String location = UserInterface.inputLine("Location: ");
        double duration = UserInterface.inputDouble("Duration: ");
        double price = UserInterface.inputDouble("Price: ");

        controller.updateParty(id, new Party(title, location, duration, price));
    }

    private void makeDeleteRequest() {
        int id = UserInterface.inputInteger("\nID: ");
        if (!controller.doesPartyExist(id)) {
            System.out.println("\nparty [id:" + id + "] doesn't exist");
            return;
        }

        controller.deleteParty(id);
    }
}
