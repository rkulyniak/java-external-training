package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.List;
import java.util.Scanner;

public class UserInterface {

    private static Scanner input = new Scanner(System.in);

    private Controller controller;

    public UserInterface() {
        controller = new Controller();
        controller.loadDefaultParties();
    }

    public void launch() {
        String userInput;

        System.out.println("Welcome to Lviv Party Agency!");
        do {
            System.out.println("\n-----------------------------------------");
            System.out.println("1. Login as an admin");
            System.out.println("2. Login as a client");
            System.out.println("3. Exit");

            System.out.print("\nEnter your choice: ");
            userInput = inputLine();

            switch (userInput) {
                case "1":
                case "admin":
                    executeAdminLogging();
                    break;
                case "2":
                case "client":
                    executeClientLogging();
                    break;
                case "3":
                case "exit":
                    System.out.println("\nBye!");
                    break;
                default:
                    System.out.println("\ninvalid value; please re-enter");
            }
        } while (!userInput.equals("3") && !userInput.equals("exit"));
    }

    private void executeAdminLogging() {
        Admin admin = new Admin(controller);
        controller.addAgencySubscriber(admin);
        admin.startAdminMenu();
        controller.removeAgencySubscriber(admin);
    }

    private void executeClientLogging() {
        Client client = new Client(controller);
        client.startClientMenu();
    }

    static String inputLine() {
        return input.nextLine();
    }

    static String inputLine(String message) {
        if (message == null) {
            throw new IllegalArgumentException();
        }

        String line;

        do {
            System.out.print(message);
            line = input.nextLine();

            if (!isValueValid(line)) {
                System.out.println("invalid value; please re-enter");
            }
        } while (!isValueValid(line));

        return line;
    }

    static int inputInteger(String message) {
        if (message == null) {
            throw new IllegalArgumentException();
        }

        int number = -1;

        do {
            System.out.print(message);
            try {
                number = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
                continue;
            }
            if (!isValueValid(number)) {
                System.out.println("invalid value; please re-enter");
            }
        } while (!isValueValid(number));

        return number;
    }

    static double inputDouble(String message) {
        if (message == null) {
            throw new IllegalArgumentException();
        }

        double number = -1;

        do {
            System.out.print(message);
            try {
                number = Double.parseDouble(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
                continue;
            }
            if (!isValueValid(number)) {
                System.out.println("invalid value; please re-enter");
            }
        } while (!isValueValid(number));

        return number;
    }

    private static boolean isValueValid(String line) {
        return line != null && !line.isEmpty();
    }

    private static boolean isValueValid(int number) {
        return number > 0;
    }

    private static boolean isValueValid(double number) {
        return number > 0.0;
    }

    static void printPartiesInfo(List<String> partiesInfo) {
        if (partiesInfo == null) {
            System.err.println("invalid parties info");
            return;
        }
        if (partiesInfo.size() == 0) {
            System.out.println("\nNo one element");
            return;
        }

        System.out.println("\n" + partiesInfo.get(0));

        for (int i = 1; i < partiesInfo.size(); i++) {
            System.out.println("\n" + partiesInfo.get(i));
        }
    }
}
