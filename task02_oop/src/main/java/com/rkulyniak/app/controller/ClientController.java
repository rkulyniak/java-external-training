package com.rkulyniak.app.controller;

import java.util.List;

public interface ClientController {

    List<String> getPartiesInfoSortedById();

    List<String> getPartiesInfoSortedByTitle();

    List<String> getPartiesInfoSortedByLocation();

    List<String> getPartiesInfoSortedByDuration();

    List<String> getPartiesInfoSortedByPrice();

    // ---------------------------------------------------------

    List<String> getPartiesInfoWithIdCondition(int id);

    List<String> getPartiesInfoWithTitleCondition(String title);

    List<String> getPartiesInfoWithLocationCondition(String location);

    List<String> getPartiesInfoWithDurationCondition(double duration);

    List<String> getPartiesInfoWithPriceCondition(double price);
}
