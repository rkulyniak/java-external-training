package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.Agency;
import com.rkulyniak.app.model.Filter;
import com.rkulyniak.app.model.Party;
import com.rkulyniak.app.model.Sorter;
import com.rkulyniak.app.view.Observer;

import java.util.List;

public class Controller implements AdminController, ClientController {

    private Agency agency;

    private Sorter sorter;
    private Filter filter;

    public Controller() {
        agency = new Agency();

        sorter = new Sorter();
        filter = new Filter();
    }

    public void loadDefaultParties() {
        agency.addParty(new Party("DiscoParty",
                "Volodymyra Vynnychenka St, 6",
                2.00, 3500));
        agency.addParty(new Party("ClownParty",
                "Katedralna Square, 7",
                1.50, 5000));
        agency.addParty(new Party("ScienceParty",
                "Mitskevycha Square, 10",
                2.00, 2300));
        agency.addParty(new Party("FaceParty",
                "Pavla Kovzhuna Street, 4",
                1.00, 2500));
        agency.addParty(new Party("DeluxeParty",
                "Shevchenka Ave, 7",
                3.50, 4200));
    }

    public void addAgencySubscriber(Observer subscriber) {
        agency.addSubscriber(subscriber);
    }

    public void removeAgencySubscriber(Observer subscriber) {
        agency.removeSubscriber(subscriber);
    }

    // ---------------------------------------------------------

    @Override
    public List<String> getPartiesInfo() {
        return agency.getPartiesInfo();
    }

    @Override
    public void createParty(Party party) {
        agency.addParty(party);
    }

    @Override
    public boolean doesPartyExist(int id) {
        return agency.doesPartyExist(id);
    }

    @Override
    public void updateParty(int id, Party party) {
        agency.updateParty(id, party);
    }

    @Override
    public void deleteParty(int id) {
        agency.removeParty(id);
    }

    // ---------------------------------------------------------

    @Override
    public List<String> getPartiesInfoSortedById() {
        return sorter.sortPartiesById(agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoSortedByTitle() {
        return sorter.sortPartiesByTitle(agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoSortedByLocation() {
        return sorter.sortPartiesByLocation(agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoSortedByDuration() {
        return sorter.sortPartiesByDuration(agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoSortedByPrice() {
        return sorter.sortPartiesByPrice(agency.getParties());
    }

    // ---------------------------------------------------------

    @Override
    public List<String> getPartiesInfoWithIdCondition(int id) {
        return filter.filterPartiesWithIdCondition(id, agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoWithTitleCondition(String title) {
        return filter.filterPartiesWithTitleCondition(title, agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoWithLocationCondition(String location) {
        return filter.filterPartiesWithLocationCondition(location, agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoWithDurationCondition(double duration) {
        return filter.filterPartiesWithDurationCondition(duration, agency.getParties());
    }

    @Override
    public List<String> getPartiesInfoWithPriceCondition(double price) {
        return filter.filterPartiesWithPriceCondition(price, agency.getParties());
    }
}
