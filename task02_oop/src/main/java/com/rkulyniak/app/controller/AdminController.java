package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.Party;

import java.util.List;

public interface AdminController {

    List<String> getPartiesInfo();

    void createParty(Party party);

    boolean doesPartyExist(int id);

    void updateParty(int id, Party party);

    void deleteParty(int id);
}
