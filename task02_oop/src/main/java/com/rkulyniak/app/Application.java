package com.rkulyniak.app;

import com.rkulyniak.app.view.UserInterface;

public class Application {

    public static void main(String[] args) {
        new UserInterface().launch();
    }
}
