package com.rkulyniak.app.controller;

public interface TaskController {

    void executeTaskA();

    void executeTaskB();

    void executeTaskC();

    void executeTaskD();
}
