package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.*;
import com.rkulyniak.app.model.tasks.*;
import com.rkulyniak.app.view.Subscriber;

public class Controller implements TaskController {

    private Publisher publisher;

    private Task taskA;
    private Task taskB;
    private Task taskC;
    private Task taskD;

    public Controller() {
        publisher = new Publisher();

        taskA = new TaskA(publisher);
        taskB = new TaskB(publisher);
        taskC = new TaskC(publisher);
        taskD = new TaskD(publisher);
    }

    public void addSubscriber(Subscriber ui) {
        publisher.subscribe(ui);
    }

    public void removeSubscriber(Subscriber ui) {
        publisher.unsubscribe(ui);
    }

    @Override
    public void executeTaskA() {
        taskA.executeTask();
    }

    @Override
    public void executeTaskB() {
        taskB.executeTask();
    }

    @Override
    public void executeTaskC() {
        taskC.executeTask();
    }

    @Override
    public void executeTaskD() {
        taskD.executeTask();
    }
}
