package com.rkulyniak.app.view;

public interface Subscriber {

    void update(String msg);
}
