package com.rkulyniak.app.view;

interface Executable {

    void execute();
}
