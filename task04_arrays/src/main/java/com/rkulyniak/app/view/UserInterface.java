package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.TaskController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class UserInterface implements Subscriber {

    private TaskController controller;

    private Map<String, String> menu;
    private Map<String, Executable> menuItems;

    private Logger logger;
    private Scanner input;

    public UserInterface(TaskController controller) {
        this.controller = controller;

        menu = new LinkedHashMap<>();
        menuItems = new LinkedHashMap<>();

        logger = LogManager.getLogger(UserInterface.class);
        input = new Scanner(System.in);
    }

    public void launch() {
        logger.info("the application has started execution");
        // .............
        initMainMenu();
        startMainMenu();
        // .............
        logger.info("the application has finished execution");
    }

    private void initMainMenu() {
        menu.put("1", "1 - Execute TaskA");
        menu.put("2", "2 - Execute TaskB");
        menu.put("3", "3 - Execute TaskC");
        menu.put("4", "4 - Execute TaskD");
        menu.put("Q", "Q - exit");

        menuItems.put("1", controller::executeTaskA);
        menuItems.put("2", controller::executeTaskB);
        menuItems.put("3", controller::executeTaskC);
        menuItems.put("4", controller::executeTaskD);
        menuItems.put("Q", () -> System.out.println("Bye!"));
    }

    private void startMainMenu() {
        String keyMenu;
        do {
            System.out.println();
            for (String item : menu.values()) {
                System.out.println(item);
            }

            System.out.print("\nSelect menu item: ");
            keyMenu = input.nextLine().toUpperCase();

            System.out.println();
            try {
                menuItems.get(keyMenu).execute();
            } catch (NullPointerException e) {
                logger.info("user inputted invalid value; inputted [" + keyMenu + "]");
                System.out.println("invalid input; please re-enter");
            }
        } while (!keyMenu.equals("Q"));
    }

    @Override
    public void update(String msg) {
        System.out.print(msg);
    }

    @Override
    public String toString() {
        return "console user interface";
    }
}
