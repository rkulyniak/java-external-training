package com.rkulyniak.app.model.game.calculators;

import com.rkulyniak.app.model.game.Door;

import java.util.ArrayList;
import java.util.List;

public class WaysCalculator {

    private DeathWaysCalculator deathCalculator;
    private SurviveWayCalculator surviveCalculator;

    public WaysCalculator() {
        deathCalculator = new DeathWaysCalculator();
        surviveCalculator = new SurviveWayCalculator();
    }

    public List<List<Integer>> getDeathWays(List<Door> doors, int health) {
        deathCalculator.calculateWays(copyDoors(doors), health, new ArrayList<>());
        return deathCalculator.getDeathWays();
    }

    public List<Integer> getBestSurviveWay(List<Door> doors, int health) {
        surviveCalculator.calculateWay(copyDoors(doors), health);
        return surviveCalculator.getSurviveWay();
    }

    static List<Door> copyDoors(List<Door> doors) {
        List<Door> copy = new ArrayList<>(doors.size());
        for (Door item : doors) {
            copy.add(item.copy());
        }
        return copy;
    }

    static List<Integer> copyWays(List<Integer> ways) {
        List<Integer> copy = new ArrayList<>(ways.size());
        copy.addAll(ways);
        return copy;
    }
}
