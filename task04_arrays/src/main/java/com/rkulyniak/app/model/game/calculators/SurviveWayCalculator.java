package com.rkulyniak.app.model.game.calculators;

import com.rkulyniak.app.model.game.Door;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

class SurviveWayCalculator {

    private List<Integer> surviveWay;

    SurviveWayCalculator() {
        surviveWay = new ArrayList<>();
    }

    List<Integer> getSurviveWay() {
        return surviveWay;
    }

    void calculateWay(List<Door> doors, int health) {
        Door nextDoor;
        try {
            nextDoor = getNextDoor(doors);
        } catch (NoSuchElementException e) {
            return;
        }

        health += nextDoor.getEntity().getPower();

        if (health < 0) {
            surviveWay.clear();
        } else {
            surviveWay.add(nextDoor.getId());
            nextDoor.setUsed(true);
            calculateWay(doors, health);
        }
    }


    private Door getNextDoor(List<Door> doors) {
        int unusedIndex = getUnusedDoorIndex(doors);
        Door nextDoor = doors.get(unusedIndex);
        int damage = nextDoor.getEntity().getPower();

        for (int i = unusedIndex + 1; i < doors.size(); i++) {
            if (doors.get(i).isUsed()) {
                continue;
            }
            if (doors.get(i).getEntity().getPower() > damage) {
                nextDoor = doors.get(i);
                damage = nextDoor.getEntity().getPower();
            }
        }

        return nextDoor;
    }

    private int getUnusedDoorIndex(List<Door> doors) {
        for (int i = 0; i < doors.size(); i++) {
            if (!doors.get(i).isUsed()) {
                return i;
            }
        }
        throw new NoSuchElementException("not found unused door");
    }
}
