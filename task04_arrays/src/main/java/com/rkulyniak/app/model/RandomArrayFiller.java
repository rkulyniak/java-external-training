package com.rkulyniak.app.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class RandomArrayFiller {

    private Logger logger;

    public RandomArrayFiller() {
        logger = LogManager.getLogger(RandomArrayFiller.class);
    }

    public int[] fillArrayRandomly(int length, int bound) {
        if (!isLengthValid(length)) {
            logger.error("invalid length value; received [" + length + "]");
            return null;
        }
        if (!isBoundValid(bound)) {
            logger.error("invalid bound value; received [" + bound + "]");
            return null;
        }

        int[] array = new int[length];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(bound);
        }

        return array;
    }

    private boolean isLengthValid(int length) {
        return length > 0;
    }

    private boolean isBoundValid(int bound) {
        return bound > 0;
    }
}
