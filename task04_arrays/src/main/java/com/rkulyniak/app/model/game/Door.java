package com.rkulyniak.app.model.game;

import com.rkulyniak.app.model.game.interactable.Interactable;

public class Door {

    private static int doorCounter = 0;

    private int id;
    private Interactable entity;
    private boolean used;

    public Door(Interactable entity) {
        id = doorCounter++;
        this.entity = entity;
        used = false;
    }

    private Door(int id, Interactable entity, boolean used) {
        this.id = id;
        this.entity = entity;
        this.used = used;
    }

    static void resetDoorCounter() {
        doorCounter = 0;
    }

    public int getId() {
        return id;
    }

    public Interactable getEntity() {
        return entity;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Door copy() {
        return new Door(id, entity, used);
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(id);
    }

    @Override
    public boolean equals(Object anotherObj) {
        if (this == anotherObj) return true;
        if (anotherObj == null) return false;
        if (getClass() != anotherObj.getClass()) return false;

        Door obj = (Door) anotherObj;
        return this.id == obj.id;
    }

    @Override
    public String toString() {
        return "Door #" + id + ", entity=[" + entity + "], used=" + used;
    }
}
