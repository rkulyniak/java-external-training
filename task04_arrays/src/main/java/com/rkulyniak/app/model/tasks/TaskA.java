package com.rkulyniak.app.model.tasks;

import com.rkulyniak.app.model.Observer;
import com.rkulyniak.app.model.RandomArrayFiller;
import com.rkulyniak.app.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Random;

public class TaskA implements Task {

    private Observer observer;
    private Logger logger;

    private int[] array1;
    private int[] array2;

    private Random random;

    public TaskA(Observer observer) {
        this.observer = observer;
        logger = LogManager.getLogger(TaskA.class);

        array1 = null;
        array1 = null;

        random = new Random();
    }

    @Override
    public void executeTask() {
        fillArrays();
        logger.info("array1 filled randomly");
        logger.info("array1 " + Arrays.toString(array1));
        logger.info("array2 filled randomly");
        logger.info("array2 " + Arrays.toString(array2));
        printArrays();

        int[] newArray;
        int length = 10;

        newArray = buildArrayWithValuesFromArrays(length);
        logger.info("built new array with values from two other arrays");
        logger.info(Arrays.toString(newArray));

        observer.notifySubscribers("\narray with values from two arrays\n");
        printArray(newArray);

        newArray = buildArrayWithValuesFromOnlyOneArray(length);
        logger.info("built new array with values which exist in only one array");
        logger.info(Arrays.toString(newArray));

        observer.notifySubscribers("\narray with values from only one array\n");
        printArray(newArray);
    }

    private void fillArrays() {
        RandomArrayFiller filler = new RandomArrayFiller();
        final int length = 10;
        final int bound = 30;

        array1 = filler.fillArrayRandomly(length, bound);
        array2 = filler.fillArrayRandomly(length, bound);
    }

    private void printArrays() {
        observer.notifySubscribers("array1 ");
        printArray(array1);
        observer.notifySubscribers("array2 ");
        printArray(array2);
    }

    private void printArray(int[] array) {
        observer.notifySubscribers(Arrays.toString(array) + '\n');
    }

    private int[] buildArrayWithValuesFromArrays(int length) {
        if (!isLengthValid(length)) {
            logger.error("invalid length value; received [" + length + "]");
            throw new IllegalArgumentException("invalid length");
        }

        int[] newArray = new int[length];

        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = getRandomValueFromArrays();
        }

        return newArray;
    }

    private int getRandomValueFromArrays() {
        int[] array = random.nextBoolean() ? array1 : array2;
        return getRandomValueFromArray(array);
    }

    private int getRandomValueFromArray(int[] array) {
        int randomIndex = random.nextInt(array.length);
        return array[randomIndex];
    }

    private int[] buildArrayWithValuesFromOnlyOneArray(int length) {
        if (!isLengthValid(length)) {
            logger.error("invalid length value; received [" + length + "]");
            throw new IllegalArgumentException("invalid length");
        }
        final int defaultValue = 0;

        int[] newArray = new int[length];
        int value;
        int j = -1;
        int k;

        for (int i = 0; i < newArray.length; i++) {
            value = defaultValue;
            for (k = 0; k < array1.length; k++) {
                j = (j == array1.length - 1) ? 0 : j + 1;

                if (!valueExistsInArray(array1[j], array2)) {
                    value = array1[j];
                    break;
                }
            }

            newArray[i] = value;
        }

        return newArray;
    }

    private boolean valueExistsInArray(int value, int[] array) {
        for (int i : array) {
            if (i == value) {
                return true;
            }
        }
        return false;
    }

    private boolean isLengthValid(int length) {
        return length > 0;
    }
}
