package com.rkulyniak.app.model.game.interactable;

public interface Interactable {

    int getPower();
}
