package com.rkulyniak.app.model.tasks;

import com.rkulyniak.app.model.Observer;
import com.rkulyniak.app.model.RandomArrayFiller;
import com.rkulyniak.app.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class TaskC implements Task {

    private Observer observer;
    private Logger logger;

    private int[] array;

    public TaskC(Observer observer) {
        this.observer = observer;
        logger = LogManager.getLogger(TaskC.class);

        array = null;
    }

    @Override
    public void executeTask() {
        fillArray();
        logger.info("array filled randomly");
        logger.info("array " + Arrays.toString(array));
        printArray(array);

        int length = countUniqueArrayValues();
        int[] newArray = new int[length];

        fillArrayUniqueValuesAnotherArray(newArray, array);
        logger.info("filled new array unique values");
        logger.info(Arrays.toString(newArray));

        observer.notifySubscribers("\narray with unique values\n");
        printArray(newArray);
    }

    private void fillArray() {
        final int length = 15;
        final int bound = 7;

        array = new RandomArrayFiller().fillArrayRandomly(length, bound);
    }

    private void printArray(int[] array) {
        observer.notifySubscribers(Arrays.toString(array) + '\n');
    }

    private int countUniqueArrayValues() {
        int k = 1;

        for (int i = 1; i < array.length; i++) {
            if (array[i] != array[i - 1]) {
                k++;
            }
        }

        return k;
    }

    private void fillArrayUniqueValuesAnotherArray(int[] newArray, int[] anotherArray) {
        newArray[0] = anotherArray[0];
        for (int i = 1, j = 1; i < newArray.length; i++, j++) {
            while (anotherArray[j] == anotherArray[j - 1]) {
                j++;
            }
            newArray[i] = anotherArray[j];
        }
    }
}
