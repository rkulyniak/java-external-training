package com.rkulyniak.app.model.game.interactable;

import java.util.concurrent.ThreadLocalRandom;

class Artifact implements Interactable {

    private int power;

    Artifact() {
        int powerMin = 10;
        int powerMax = 80;
        power = ThreadLocalRandom.current().nextInt(powerMin, powerMax + 1);
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "Artifact [" + power + "]";
    }
}
