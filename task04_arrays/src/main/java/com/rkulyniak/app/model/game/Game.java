package com.rkulyniak.app.model.game;

import com.rkulyniak.app.model.Observer;
import com.rkulyniak.app.model.game.calculators.WaysCalculator;
import com.rkulyniak.app.model.game.interactable.Interactable;
import com.rkulyniak.app.model.game.interactable.InteractableEntitiesFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {

    private Observer observer;
    private Logger logger;
    private Scanner input;

    private Hero hero;
    private List<Door> doors;

    public Game(Observer observer) {
        this.observer = observer;
        logger = LogManager.getLogger(Game.class);
        input = new Scanner(System.in);

        hero = null;
        doors = new ArrayList<>();
    }

    public void start() {
        initHero();
        initDoors();

        playGame();
        Door.resetDoorCounter();

        printGameInfo();
    }

    private void initHero() {
        String name;
        int power;

        observer.notifySubscribers("\nHero name: ");
        name = input.nextLine();

        observer.notifySubscribers("Power (default 25): ");
        try {
            power = Integer.parseInt(input.nextLine());
        } catch (NumberFormatException e) {
            power = 25;
        }

        hero = new Hero(name, power);
        logger.info("Set Hero [name=" + hero.getName() + ", power=" + hero.getPower() + "]");
    }

    private void initDoors() {
        InteractableEntitiesFactory factory = new InteractableEntitiesFactory();
        final int doorAmount = 5;

        for (int i = 0; i < doorAmount; i++) {
            doors.add(new Door(factory.getInteractableEntity()));
        }
        logger.info("List Doors [" + doors + "]");
    }

    private void playGame() {
        while (hero.isAlive() && !everyDoorIsUsed()) {
            int selectedDoor = inputDoor();
            Door door = doors.get(selectedDoor);
            Interactable entity = door.getEntity();
            logger.info("Selected " + door);

            observer.notifySubscribers("\n" + entity + " behind the door!\n");
            hero.setCurrentPower(hero.getCurrentPower() + entity.getPower());
            door.setUsed(true);

            if (entity.getPower() > 0) {
                observer.notifySubscribers("You received " + entity.getPower() + " points!\n");
                logger.info("User received " + entity.getPower() + " power points");
            } else {
                observer.notifySubscribers("You lost " + entity.getPower() + " points!\n");
                logger.info("User lost " + entity.getPower() + " power points");
            }
            observer.notifySubscribers("\n" + hero.getName() + "\n");
            observer.notifySubscribers("power: " + hero.getCurrentPower() + " points\n");
            logger.info(hero);
        }
        if (hero.isAlive()) {
            observer.notifySubscribers("\nCongratulations! You won the game!\n");
            logger.info("User won the game");
        } else {
            observer.notifySubscribers("\nSorry! You lost the game!\n");
            logger.info("User lost the game");
        }
    }

    private int inputDoor() {
        int door;

        while (true) {
            printDoors();

            observer.notifySubscribers("\nEnter door: ");
            try {
                door = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                observer.notifySubscribers("invalid input; please re-enter\n");
                continue;
            }
            if (!isDoorValid(door)) {
                observer.notifySubscribers("invalid value; please re-enter\n");
                continue;
            }
            if (isDoorUsed(door)) {
                observer.notifySubscribers("door has already used; select another one\n");
                continue;
            }

            break;
        }

        return door;
    }

    private void printDoors() {
        observer.notifySubscribers("\n");
        observer.notifySubscribers("---------\n");
        observer.notifySubscribers("| # | ? |\n");
        observer.notifySubscribers("---------\n");
        for (Door door : doors) {
            observer.notifySubscribers("| ");
            observer.notifySubscribers(Integer.toString(door.getId()));
            observer.notifySubscribers(" | ");
            observer.notifySubscribers(door.isUsed() ? "-" : "?");
            observer.notifySubscribers(" |\n");
        }
        observer.notifySubscribers("---------\n");
    }

    private boolean isDoorValid(int door) {
        return door >= 0 && door < doors.size();
    }

    private boolean isDoorUsed(int door) {
        return doors.get(door).isUsed();
    }

    private boolean everyDoorIsUsed() {
        for (Door door : doors) {
            if (!door.isUsed()) {
                return false;
            }
        }
        return true;
    }

    private void printGameInfo() {
        observer.notifySubscribers("\n");
        for (Door door : doors) {
            observer.notifySubscribers(door.getId() + " | " + door.getEntity().getPower() + "\n");
        }

        printBestSurviveWay();
        observer.notifySubscribers("\n");
        printDeathWays();
    }

    private void printBestSurviveWay() {
        WaysCalculator calculator = new WaysCalculator();

        List<Door> copyDoors = new ArrayList<>(doors);
        copyDoors.forEach(door -> door.setUsed(false));

        List<Integer> surviveWay = calculator.getBestSurviveWay(copyDoors, hero.getPower());

        observer.notifySubscribers("\nBest survive way\n");
        observer.notifySubscribers(surviveWay.toString());
    }

    private void printDeathWays() {
        WaysCalculator calculator = new WaysCalculator();

        List<Door> copyDoors = new ArrayList<>(doors);
        copyDoors.forEach(door -> door.setUsed(false));

        List<List<Integer>> deathWays = calculator.getDeathWays(copyDoors, hero.getPower());

        observer.notifySubscribers("\nThere are " + deathWays.size() + " death ways!\n");
        observer.notifySubscribers("Print all death ways (y/n)? ");
        if (new Scanner(System.in).nextLine().equals("y")) {
            for (List<Integer> deathWay : deathWays) {
                observer.notifySubscribers(deathWay.toString() + "\n");
            }
        }
    }
}
