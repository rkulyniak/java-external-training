package com.rkulyniak.app.model;

import com.rkulyniak.app.view.Subscriber;

public interface Observer {

    void subscribe(Subscriber subscriber);

    void unsubscribe(Subscriber subscriber);

    void notifySubscribers(String msg);
}
