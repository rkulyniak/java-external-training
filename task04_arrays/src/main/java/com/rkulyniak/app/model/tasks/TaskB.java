package com.rkulyniak.app.model.tasks;

import com.rkulyniak.app.model.Observer;
import com.rkulyniak.app.model.RandomArrayFiller;
import com.rkulyniak.app.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TaskB implements Task {

    private Observer observer;
    private Logger logger;

    private int[] array;

    public TaskB(Observer observer) {
        this.observer = observer;
        logger = LogManager.getLogger(TaskB.class);

        array = null;
    }

    @Override
    public void executeTask() {
        fillArray();
        logger.info("array filled randomly");
        logger.info("array " + Arrays.toString(array));
        printArray(array);

        Map<Integer, Integer> duplicateValues = countDuplicateValues();

        int length = countArrayValuesHaveLessTwoDuplicates(duplicateValues);
        int[] newArray = new int[length];

        fillArrayValuesHaveLessTwoDuplicates(duplicateValues, newArray);
        logger.info("filled new array values which have less than two duplicates");
        logger.info(Arrays.toString(newArray));

        observer.notifySubscribers("\narray with values which have less than two duplicates\n");
        printArray(newArray);
    }

    private void fillArray() {
        final int length = 20;
        final int bound = 10;

        array = new RandomArrayFiller().fillArrayRandomly(length, bound);
    }

    private void printArray(int[] array) {
        observer.notifySubscribers(Arrays.toString(array) + '\n');
    }

    private Map<Integer, Integer> countDuplicateValues() {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i : array) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }

        return map;
    }

    private int countArrayValuesHaveLessTwoDuplicates(Map<Integer, Integer> duplicateValues) {
        int k = 0;

        for (int i : duplicateValues.values()) {
            if (i <= 2) {
                k++;
            }
        }

        return k;
    }

    private void fillArrayValuesHaveLessTwoDuplicates(
            Map<Integer, Integer> duplicateValues, int[] array) {
        Iterator<Integer> kIterator = duplicateValues.keySet().iterator();
        Iterator<Integer> vIterator = duplicateValues.values().iterator();

        for (int i = 0; i < array.length; i++) {
            while (kIterator.hasNext()) {
                int k = kIterator.next();
                int v = vIterator.next();

                if (v <= 2) {
                    array[i] = k;
                    break;
                }
            }
        }
    }
}
