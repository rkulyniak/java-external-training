package com.rkulyniak.app.model.tasks;

import com.rkulyniak.app.model.Observer;
import com.rkulyniak.app.model.Task;
import com.rkulyniak.app.model.game.Game;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskD implements Task {

    private Observer observer;
    private Logger logger;

    public TaskD(Observer observer) {
        this.observer = observer;
        logger = LogManager.getLogger(TaskD.class);
    }

    @Override
    public void executeTask() {
        observer.notifySubscribers("Welcome to the Game!\n");
        logger.info("Game has been started");
        new Game(observer).start();
        logger.info("Game has been finished");
    }
}
