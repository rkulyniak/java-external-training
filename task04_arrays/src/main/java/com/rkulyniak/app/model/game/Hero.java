package com.rkulyniak.app.model.game;

class Hero {

    private String name;
    private final int power;
    private int currentPower;

    Hero(String name, int power) {
        this.name = name;
        this.power = power;
        currentPower = power;
    }

    String getName() {
        return name;
    }

    int getPower() {
        return power;
    }

    int getCurrentPower() {
        return currentPower;
    }

    void setCurrentPower(int currentPower) {
        this.currentPower = currentPower;
    }

    boolean isAlive() {
        return currentPower >= 0;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object anotherObj) {
        if (this == anotherObj) return true;
        if (anotherObj == null) return false;
        if (getClass() != anotherObj.getClass()) return false;

        Hero obj = (Hero) anotherObj;
        return this.name.equals(obj.name);
    }

    @Override
    public String toString() {
        return "Hero name=\'" + name + "\', power=" + currentPower;
    }
}
