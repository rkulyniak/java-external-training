package com.rkulyniak.app.model.game.interactable;

import java.util.concurrent.ThreadLocalRandom;

class Monster implements Interactable {

    private int power;

    Monster() {
        int powerMin = 5;
        int powerMax = 100;
        power = ThreadLocalRandom.current().nextInt(powerMin, powerMax + 1);
    }

    @Override
    public int getPower() {
        return -power;
    }

    @Override
    public String toString() {
        return "Monster [" + power + "]";
    }
}
