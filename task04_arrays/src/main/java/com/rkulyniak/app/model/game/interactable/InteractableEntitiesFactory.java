package com.rkulyniak.app.model.game.interactable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InteractableEntitiesFactory {

    private Random random;

    public InteractableEntitiesFactory() {
        random = new Random();
    }

    public Interactable getInteractableEntity() {
        List<Interactable> entities = getListOfEntities();
        return entities.get(random.nextInt(entities.size()));
    }

    private List<Interactable> getListOfEntities() {
        return new ArrayList<>() {{
            add(new Artifact());
            add(new Monster());
        }};
    }
}
