package com.rkulyniak.app.model.game.calculators;

import com.rkulyniak.app.model.game.Door;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.rkulyniak.app.model.game.calculators.WaysCalculator.copyDoors;
import static com.rkulyniak.app.model.game.calculators.WaysCalculator.copyWays;

class DeathWaysCalculator {

    private List<List<Integer>> deathWays;

    DeathWaysCalculator() {
        deathWays = new ArrayList<>();
    }

    List<List<Integer>> getDeathWays() {
        return deathWays;
    }

    void calculateWays(List<Door> doors, int health, List<Integer> way) {
        List<Door> checkedDoors = new ArrayList<>();

        while (true) {
            Door nextDoor;
            try {
                nextDoor = getNextDoor(doors, checkedDoors);
            } catch (NoSuchElementException e) {
                break;
            }
            checkedDoors.add(nextDoor);

            if (nextDoor.getEntity().getPower() + health < 0) {
                List<Integer> newWay = copyWays(way);
                newWay.add(nextDoor.getId());
                deathWays.add(newWay);
            } else {
                List<Door> newDoors = copyDoors(doors);
                newDoors.remove(nextDoor);
                int newHealth = health + nextDoor.getEntity().getPower();
                List<Integer> newWay = copyWays(way);
                newWay.add(nextDoor.getId());

                calculateWays(newDoors, newHealth, newWay);
            }
        }
    }

    private Door getNextDoor(List<Door> doors, List<Door> checkedDoors) {
        int unusedIndex = getUnusedDoorIndex(doors, checkedDoors);
        Door nextDoor = doors.get(unusedIndex);
        int damage = nextDoor.getEntity().getPower();

        for (int i = unusedIndex + 1; i < doors.size(); i++) {
            if (doors.get(i).isUsed() || checkedDoors.contains(doors.get(i))) {
                continue;
            }
            if (doors.get(i).getEntity().getPower() < damage) {
                nextDoor = doors.get(i);
                damage = nextDoor.getEntity().getPower();
            }
        }

        return nextDoor;
    }

    private int getUnusedDoorIndex(List<Door> doors, List<Door> checkedDoors) {
        for (int i = 0; i < doors.size(); i++) {
            if (!doors.get(i).isUsed() && !checkedDoors.contains(doors.get(i))) {
                return i;
            }
        }
        throw new NoSuchElementException("not found unused door");
    }
}
