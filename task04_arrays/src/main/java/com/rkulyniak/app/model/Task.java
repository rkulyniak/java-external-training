package com.rkulyniak.app.model;

public interface Task {

    void executeTask();
}
