package com.rkulyniak.app.model;

import com.rkulyniak.app.view.Subscriber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class Publisher implements Observer {

    private Set<Subscriber> subscribers;
    private Logger logger;

    public Publisher() {
        subscribers = new HashSet<>();
        logger = LogManager.getLogger(Publisher.class);
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        if (subscriber == null) {
            logger.error("unable to add subscriber; received null");
        } else {
            subscribers.add(subscriber);
        }
    }

    @Override
    public void unsubscribe(Subscriber subscriber) {
        if (subscriber == null) {
            logger.error("unable to remove subscriber; received null");
        } else {
            subscribers.remove(subscriber);
        }
    }

    @Override
    public void notifySubscribers(String msg) {
        if (msg == null) {
            logger.warn("unable to notify subscribers; received null");
        } else {
            subscribers.forEach(subscriber -> subscriber.update(msg));
        }
    }
}
