EPAM University Program: 'Java core external training' homework projects.

Teamwork
- Florist
- PizzaCelentano
- Scrum_Planner

Individual work
- task01_fibonacci
- task02_oop
- task03_log4j
- task04_arrays
- task05_collections
- task06_lambdas-streams
- task07_string
- task08_annotation
- task09_junit
- task10_io-nio
- task11_multithreading_1
- task12_multithreading_2
- task13_xml
- task14_json
- task15_database_1
- task16_database_2
- task17_database_3
- task18_jdbc
- task19_hibernate
- task20_spring-data-jpa 
