package com.rkulyniak.app.model.first;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.IntStream;

public class FirstTask {

    private Logger logger = LogManager.getLogger(FirstTask.class);

    private int n1;
    private int n2;
    private int n3;

    private IntTernaryOperator max;
    private IntTernaryOperator average;

    public FirstTask() {
        max = (a, b, c) -> IntStream.of(a, b, c).max().getAsInt();
        average = (a, b, c) -> (int) IntStream.of(a, b, c).average().orElseThrow();
    }

    public void setNumbers(int n1, int n2, int n3) {
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
        logger.info("numbers have been set to " +
                "n1=" + n1 + ", n2=" + n2 + ", n3=" + n3);
    }

    public int getMax() {
        int result = max.applyAsInt(n1, n2, n3);
        logger.info("returned max value " + result);
        return result;
    }

    public int getAverage() {
        int result = average.applyAsInt(n1, n2, n3);
        logger.info("returned average value " + result);
        return result;
    }
}
