package com.rkulyniak.app.model.third;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class ThirdTask {

    private static Logger logger = LogManager.getLogger(ThirdTask.class);

    private int[] numbers;

    public ThirdTask() {
        numbers = new int[10];
    }

    public void generateNumbers(int size, int tail, int head) {
        numbers = new NumbersGenerator().generateNumbers(size, tail, head);
        logger.info("generated new array of numbers " + Arrays.toString(numbers));
    }

    public int getMax() {
        int max = Arrays.stream(numbers)
                .max()
                .orElseThrow();
        logger.info("returned max value " + max);
        return max;
    }

    public int getMin() {
        int min = Arrays.stream(numbers)
                .min()
                .orElseThrow();
        logger.info("returned min value " + min);
        return min;
    }

    public int getSum() {
        int sum = Arrays.stream(numbers)
                .sum();
        logger.info("returned sum of numbers " + sum);
        return sum;
    }

    public double getAverage() {
        double average = Arrays.stream(numbers)
                .average()
                .orElseThrow();
        logger.info("returned average value " + average);
        return average;
    }

    public long getBiggerAverage() {
        double average = getAverage();
        long count = Arrays.stream(numbers)
                .filter(num -> num > average)
                .count();
        logger.info("returned count of values that are bigger than average " + count);
        return count;
    }
}
