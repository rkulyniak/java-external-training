package com.rkulyniak.app.model.second;

@FunctionalInterface
public interface Command {

    void execute(String data);
}
