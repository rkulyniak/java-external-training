package com.rkulyniak.app.model.first;

@FunctionalInterface
public interface IntTernaryOperator {

    int applyAsInt(int n1, int n2, int n3);
}
