package com.rkulyniak.app.model.fourth;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FourthTask {

    private Logger logger = LogManager.getLogger(FourthTask.class);

    private List<String> text;

    public FourthTask() {
        text = new ArrayList<>();
    }

    public void setText(List<String> text) {
        this.text = text;
        logger.info("text was updated " + text);
    }

    public List<String> getSortedUniqueWords() {
        List<String> sortedUniqueWords = text.stream()
                .flatMap(line -> Arrays.stream(line.trim().split("\\W+")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        logger.info("returned list of sorted unique words from text " + sortedUniqueWords);
        return sortedUniqueWords;
    }

    public long getNumOfUniqueWords() {
        long numOfUniqueWords = text.stream()
                .flatMap(line -> Arrays.stream(line.trim().split("\\W+")))
                .distinct()
                .count();
        logger.info("returned number of unique words from text " + numOfUniqueWords);
        return numOfUniqueWords;
    }

    public Map<String, Long> getWordsCount() {
        Map<String, Long> wordsCount = text.stream()
                .flatMap(line -> Arrays.stream(line.trim().split("\\W+")))
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
        logger.info("returned words count " + wordsCount);
        return wordsCount;
    }

    public long getNumOfLowerCaseSymbols() {
        long numOfLowerCaseSymbols = text.stream()
                .flatMap(line -> Arrays.stream(line.trim().split("[a-z]")))
                .count();
        logger.info("returned number of lower case symbols in text " + numOfLowerCaseSymbols);
        return numOfLowerCaseSymbols;
    }
}
