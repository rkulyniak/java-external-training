package com.rkulyniak.app.model.second;

class DeleteCommand implements Command {

    private Database database;

    DeleteCommand(Database database) {
        this.database = database;
    }

    @Override
    public void execute(String data) {
        database.delete(data);
    }
}
