package com.rkulyniak.app.model.second;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SecondTask {

    private static Logger logger = LogManager.getLogger(SecondTask.class);

    private Database database;

    private Command create;
    private Command read;
    private Command update;
    private Command delete;

    public SecondTask() {
        database = new Database();

        create = s -> database.create(s);
        read = database::read;
        update = new Command() {
            @Override
            public void execute(String str) {
                database.update(str);
            }
        };
        delete = new DeleteCommand(database);
    }

    public boolean executeCommand(String command, String data) {
        switch (command.toUpperCase()) {
            case "CREATE":
                create.execute(data);
                logger.info("executed create command; " +
                        "which is implemented by using lambda");
                return true;
            case "READ":
                read.execute(data);
                logger.info("executed read command; " +
                        "which is implemented by using method reference");
                return true;
            case "UPDATE":
                update.execute(data);
                logger.info("executed update command; " +
                        "which is implemented by using anonymous class");
                return true;
            case "DELETE":
                delete.execute(data);
                logger.info("executed delete command; " +
                        "which is implemented by using object of Command class");
                return true;
            default:
                logger.info("invoked invalid command [" + command + "]");
                return false;
        }
    }
}
