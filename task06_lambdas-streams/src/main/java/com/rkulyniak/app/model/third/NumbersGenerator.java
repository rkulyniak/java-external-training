package com.rkulyniak.app.model.third;

import java.util.Random;
import java.util.stream.IntStream;

class NumbersGenerator {

    private Random random;
    private int size;
    private int tail;
    private int head;

    NumbersGenerator() {
        random = new Random();
    }

    int[] generateNumbers(int size, int tail, int head) {
        this.size = size;
        this.tail = tail;
        this.head = head;
        int percentage = random.nextInt(100);

        if (percentage < 20) {
            return generateNumbers1();
        }
        if (percentage < 40) {
            return generateNumbers2();
        }
        if (percentage < 60) {
            return generateNumbers3();
        }
        if (percentage < 80) {
            return generateNumbers4();
        }
        return generateNumbers5();
    }

    private int[] generateNumbers1() {
        return IntStream.generate(this::getRandomNum)
                .limit(size)
                .toArray();
    }

    private int[] generateNumbers2() {
        int randomNum = getRandomNum();
        return IntStream.range(randomNum, randomNum + size)
                .toArray();
    }

    private int[] generateNumbers3() {
        return IntStream.iterate(getRandomNum(), i -> getRandomNum())
                .limit(size)
                .toArray();
    }

    private int[] generateNumbers4() {
        IntStream.Builder builder = IntStream.builder();
        for (int i = 0; i < size; i++) {
            builder.add(getRandomNum());
        }
        return builder.build()
                .toArray();
    }

    private int[] generateNumbers5() {
        IntStream stream = IntStream.concat(
                IntStream.generate(this::getRandomNum),
                IntStream.generate(this::getRandomNum));
        return stream
                .limit(size)
                .toArray();
    }

    private int getRandomNum() {
        return random.nextInt(head - tail + 1) + tail;
    }
}
