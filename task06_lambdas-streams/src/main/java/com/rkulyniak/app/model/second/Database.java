package com.rkulyniak.app.model.second;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Database {

    private static Logger logger = LogManager.getLogger(Database.class);

    void create(String str) {
        logger.info("creating '" + str + "' ...");
    }

    void read(String str) {
        logger.info("reading '" + str + "' ...");
    }

    void update(String str) {
        logger.info("updating '" + str + "' ...");
    }

    void delete(String str) {
        logger.info("deleting '" + str + "' ...");
    }
}
