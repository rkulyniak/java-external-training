package com.rkulyniak.app.view.taskmenu;

import com.rkulyniak.app.controller.FirstTaskController;
import com.rkulyniak.app.view.Menu;
import com.rkulyniak.app.view.UserInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class FirstTaskMenu implements Menu {

    private static Logger logger = LogManager.getLogger(FirstTaskMenu.class);

    private FirstTaskController controller;

    public FirstTaskMenu(FirstTaskController controller) {
        this.controller = controller;
    }

    @Override
    public void launch() {
        logger.info("user switched to FirstTask menu");
        Menu.super.launch();
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - set values");
            put("2", "3 - max");
            put("3", "3 - average");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.setNumbers(
                    UserInterface.inputNumber("n1"),
                    UserInterface.inputNumber("n2"),
                    UserInterface.inputNumber("n3")));
            put("2", controller::getMax);
            put("3", controller::getAverage);
            put("Q", () -> {});
        }};
    }
}
