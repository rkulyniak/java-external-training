package com.rkulyniak.app.view.taskmenu;

import com.rkulyniak.app.controller.SecondTaskController;
import com.rkulyniak.app.view.Menu;
import com.rkulyniak.app.view.UserInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class SecondTaskMenu implements Menu {

    private static Logger logger = LogManager.getLogger(SecondTaskMenu.class);

    private SecondTaskController controller;

    public SecondTaskMenu(SecondTaskController controller) {
        this.controller = controller;
    }

    @Override
    public void launch() {
        logger.info("user switched to SecondTask menu");
        Menu.super.launch();
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Create");
            put("2", "2 - Read");
            put("3", "3 - Update");
            put("4", "4 - Delete");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.executeCommand(
                    "create", UserInterface.inputString("Data")));
            put("2", () -> controller.executeCommand(
                    "read", UserInterface.inputString("Data")));
            put("3", () -> controller.executeCommand(
                    "update", UserInterface.inputString("Data")));
            put("4", () -> controller.executeCommand(
                    "delete", UserInterface.inputString("Data")));
            put("Q", () -> {});
        }};
    }
}
