package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.FirstTaskController;
import com.rkulyniak.app.controller.FourthTaskController;
import com.rkulyniak.app.controller.SecondTaskController;
import com.rkulyniak.app.controller.ThirdTaskController;
import com.rkulyniak.app.view.taskmenu.FirstTaskMenu;
import com.rkulyniak.app.view.taskmenu.FourthTaskMenu;
import com.rkulyniak.app.view.taskmenu.SecondTaskMenu;
import com.rkulyniak.app.view.taskmenu.ThirdTaskMenu;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu implements Menu {

    private Menu firstTask;
    private Menu secondTask;
    private Menu thirdTask;
    private Menu fourthTask;

    public MainMenu() {
        firstTask = new FirstTaskMenu(new FirstTaskController());
        secondTask = new SecondTaskMenu(new SecondTaskController());
        thirdTask = new ThirdTaskMenu(new ThirdTaskController());
        fourthTask = new FourthTaskMenu(new FourthTaskController());
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - First task");
            put("2", "2 - Second task");
            put("3", "3 - Third task");
            put("4", "4 - Fourth task");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> firstTask.launch());
            put("2", () -> secondTask.launch());
            put("3", () -> thirdTask.launch());
            put("4", () -> fourthTask.launch());
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
