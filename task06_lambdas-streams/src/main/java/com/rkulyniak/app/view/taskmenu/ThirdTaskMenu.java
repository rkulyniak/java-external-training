package com.rkulyniak.app.view.taskmenu;

import com.rkulyniak.app.controller.ThirdTaskController;
import com.rkulyniak.app.view.Menu;
import com.rkulyniak.app.view.UserInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class ThirdTaskMenu implements Menu {

    private static Logger logger = LogManager.getLogger(ThirdTaskMenu.class);

    private ThirdTaskController controller;

    public ThirdTaskMenu(ThirdTaskController controller) {
        this.controller = controller;
    }

    @Override
    public void launch() {
        logger.info("user switched to ThirdTask menu");
        Menu.super.launch();
    }


    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Generate numbers");
            put("2", "2 - Get max number");
            put("3", "3 - Get min number");
            put("4", "4 - Get sum of numbers");
            put("5", "5 - Get average value");
            put("6", "6 - Get bigger average count");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.generateNumbers(
                    UserInterface.inputNumber("size"),
                    UserInterface.inputNumber("tail"),
                    UserInterface.inputNumber("head")));
            put("2", controller::getMax);
            put("3", controller::getMin);
            put("4", controller::getSum);
            put("5", controller::getAverage);
            put("6", controller::getBiggerAverage);
            put("Q", () -> {});
        }};
    }
}
