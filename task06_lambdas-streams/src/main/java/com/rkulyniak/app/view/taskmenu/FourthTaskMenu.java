package com.rkulyniak.app.view.taskmenu;

import com.rkulyniak.app.controller.FourthTaskController;
import com.rkulyniak.app.view.Menu;
import com.rkulyniak.app.view.UserInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class FourthTaskMenu implements Menu {

    private static Logger logger = LogManager.getLogger(FourthTaskMenu.class);

    private FourthTaskController controller;

    public FourthTaskMenu(FourthTaskController controller) {
        this.controller = controller;
    }

    @Override
    public void launch() {
        logger.info("user switched to FourthTask menu");
        Menu.super.launch();
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Input text");
            put("2", "2 - Get sorted unique words");
            put("3", "3 - Get num of unique words");
            put("4", "4 - Get words count");
            put("5", "5 - Get num of lowercase symbols");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.setText(UserInterface.inputText()));
            put("2", controller::getSortedUniqueWords);
            put("3", controller::getNumOfUniqueWords);
            put("4", controller::getWordsCount);
            put("5", controller::getNumOfLowerCaseSymbols);
            put("Q", () -> {});
        }};
    }
}
