package com.rkulyniak.app.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UserInterface {

    private static Scanner input = new Scanner(System.in);

    public static int inputNumber(String text) {
        int number;

        while (true) {
            System.out.print(text + ": ");
            try {
                number = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
                continue;
            }
            break;
        }

        return number;
    }

    public static String inputString(String text) {
        System.out.print(text + ": ");
        return input.nextLine();
    }

    public static List<String> inputText() {
        List<String> text = new ArrayList<>();
        String line;

        do {
            line = input.nextLine();
            if (!line.isEmpty()) {
                text.add(line);
            }
        } while (!line.isEmpty());

        return text;
    }
}
