package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.second.SecondTask;

public class SecondTaskController {

    private SecondTask secondTask;

    public SecondTaskController() {
        secondTask = new SecondTask();
    }

    public boolean executeCommand(String command, String data) {
        return secondTask.executeCommand(command, data);
    }
}
