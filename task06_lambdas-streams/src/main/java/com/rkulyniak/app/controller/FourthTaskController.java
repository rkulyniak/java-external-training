package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.fourth.FourthTask;

import java.util.List;
import java.util.Map;

public class FourthTaskController {

    private FourthTask fourthTask;

    public FourthTaskController() {
        fourthTask = new FourthTask();
    }

    public void setText(List<String> text) {
        fourthTask.setText(text);
    }

    public List<String> getSortedUniqueWords() {
        return fourthTask.getSortedUniqueWords();
    }

    public long getNumOfUniqueWords() {
        return fourthTask.getNumOfUniqueWords();
    }

    public Map<String, Long> getWordsCount() {
        return fourthTask.getWordsCount();
    }

    public long getNumOfLowerCaseSymbols() {
        return fourthTask.getNumOfLowerCaseSymbols();
    }
}