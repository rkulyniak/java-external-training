package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.first.FirstTask;

public class FirstTaskController {

    private FirstTask firstTask;

    public FirstTaskController() {
        firstTask = new FirstTask();
    }

    public void setNumbers(int n1, int n2, int n3) {
        firstTask.setNumbers(n1, n2, n3);
    }

    public int getMax() {
        return firstTask.getMax();
    }

    public int getAverage() {
        return firstTask.getAverage();
    }
}
