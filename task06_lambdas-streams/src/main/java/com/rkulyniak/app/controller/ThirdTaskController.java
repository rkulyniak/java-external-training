package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.third.ThirdTask;

public class ThirdTaskController {

    private ThirdTask thirdTask;

    public ThirdTaskController() {
        thirdTask = new ThirdTask();
    }

    public void generateNumbers(int size, int tail, int head) {
        thirdTask.generateNumbers(size, tail, head);
    }

    public int getMax() {
        return thirdTask.getMax();
    }

    public int getMin() {
        return thirdTask.getMin();
    }

    public int getSum() {
        return thirdTask.getSum();
    }

    public double getAverage() {
        return thirdTask.getAverage();
    }

    public long getBiggerAverage() {
        return thirdTask.getBiggerAverage();
    }
}
