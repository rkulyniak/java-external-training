package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import static com.rkulyniak.app.view.UserInterface.inputLine;

public class MainMenu implements Menu {

    private static Logger logger = LogManager.getLogger(MainMenu.class);

    private Controller controller;
    private Locale locale;
    private ResourceBundle bundle;

    public MainMenu(Controller controller) {
        this.controller = controller;
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menu");
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", bundle.getString("1"));
            put("2", bundle.getString("2"));
            put("3", bundle.getString("3"));
            put("4", bundle.getString("4"));
            put("5", bundle.getString("5"));
            put("6", bundle.getString("6"));
            put("7", bundle.getString("7"));
            put("Q", bundle.getString("Q"));
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        Map<String, Runnable> execution = new LinkedHashMap<>();
        execution.put("1", () -> controller.concatStrings(inputLine("str1"), inputLine("str2")));
        execution.put("2", () -> controller.checkLineBeginCapitalEndPeriod(inputLine("line")));
        execution.put("3", () -> controller.splitLineByTheOrYou(inputLine("line")));
        execution.put("4", () -> controller.replaceVowelToUnderscore(inputLine("line")));
        execution.put("5", this::internationalizeMenuEnglish);
        execution.put("6", this::internationalizeMenuUkrainian);
        execution.put("7", this::internationalizeMenuJapanese);
        execution.put("Q", () -> System.out.println("\nBye!"));
        return execution;
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menu", locale);
        logger.info("user switched menu language to English");
        launch();
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        logger.info("user switched menu language to Ukrainian");
        launch();
    }

    private void internationalizeMenuJapanese() {
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("menu", locale);
        logger.info("user switched menu language to Japanese");
        launch();
    }
}
