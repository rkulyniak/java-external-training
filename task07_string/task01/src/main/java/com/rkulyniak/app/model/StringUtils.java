package com.rkulyniak.app.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public <T> String concat(T... objects) {
        StringBuilder builder = new StringBuilder();
        for (T obj : objects) {
            builder.append(obj.toString());
        }
        return builder.toString();
    }

    public boolean isLineStartCapitalEndPeriod(String line) {
        Matcher matcher = Pattern.compile("^[A-Z].*\\.$").matcher(line);
        return matcher.matches();
    }

    public String[] splitLine(String line, String regex) {
        return line.split(regex);
    }

    public String replaceVowelToUnderscore(String line) {
        return line.replaceAll("[aeiou]", "_");
    }
}
