package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Random;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    private StringUtils stringUtils;

    public Controller() {
        stringUtils = new StringUtils();
    }

    public String concatStrings(String str1, String str2) {
        String result = stringUtils.concat(str1, str2);
        logger.info("str1=" + str1 + ", str2=" + str2 + " were concatenated [" + result + "]");
        return result;
    }

    public boolean checkLineBeginCapitalEndPeriod(String line) {
        boolean result = stringUtils.isLineStartCapitalEndPeriod(line);
        logger.info("checked line begins with a capital and ends with a period [" + result + "]");
        return result;
    }

    public String[] splitLineByTheOrYou(String line) {
        String regex = new Random().nextBoolean() ? "the" : "you";
        String[] result = stringUtils.splitLine(line, regex);
        logger.info("line split regex=" + regex + Arrays.toString(result));
        return result;
    }

    public String replaceVowelToUnderscore(String line) {
        String result = stringUtils.replaceVowelToUnderscore(line);
        logger.info("all vowels replaced to underscore [" + result + "]");
        return result;
    }
}
