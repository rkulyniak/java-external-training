package com.rkulyniak.app.view;

import java.util.Scanner;

class UserInterface {

    private static Scanner input = new Scanner(System.in);

    static String inputLine(String text) {
        System.out.print(text + ": ");
        return input.nextLine();
    }
}
