package com.rkulyniak.app.model;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TextHandler {

    private List<Sentence> sentences;

    public TextHandler() {
        sentences = new ArrayList<>();
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public long countMaxSentencesWithIdentityWord() {
        Map<String, Long> map = sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream().distinct())
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return map.values().stream()
                .mapToLong(Long::longValue)
                .max()
                .orElse(0);
    }

    public List<Sentence> getLengthSortedSentences() {
        return sentences.stream()
                .map(this::getNewSortedWordsSentence)
                .collect(Collectors.toList());
    }

    private Sentence getNewSortedWordsSentence(Sentence unsortedSentence) {
        List<String> words = new ArrayList<>(unsortedSentence.getWords());
        words.sort(Comparator.comparingInt(String::length).reversed());
        return new Sentence(words, unsortedSentence.getType());
    }

    public Optional<String> getUniqueWordFromFirstSentence() {
        List<String> wordsFromListsExceptFirst = sentences.stream()
                .skip(1)
                .flatMap(sentence -> sentence.getWords().stream())
                .collect(Collectors.toList());
        for (String word : sentences.get(0).getWords()) {
            if (!wordsFromListsExceptFirst.contains(word)) {
                return Optional.of(word);
            }
        }
        return Optional.empty();
    }

    public List<String> getWordsFromInterrogativeSentences(int length) {
        return sentences.stream()
                .filter(sentence -> sentence.getType().equals(Sentence.SentenceType.INTERROGATIVE))
                .flatMap(sentence -> sentence.getWords().stream())
                .filter(word -> word.length() == length)
                .map(String::toLowerCase)
                .distinct()
                .collect(Collectors.toList());
    }

    public List<Sentence> replaceVowelBeginningWordWithLongestWord() {
        List<Sentence> result = new ArrayList<>();
        for (Sentence sentence : sentences) {
            Optional<String> vowelBeginningWord = getVowelBeginningWord(sentence);
            Optional<String> longestWord = getLongestWord(sentence);

            if (vowelBeginningWord.isPresent() && longestWord.isPresent()) {
                Sentence newSentence = getSentenceWithReplacedWords(
                        sentence, vowelBeginningWord.get(), longestWord.get());
                result.add(newSentence);
            } else {
                result.add(sentence);
            }
        }
        return result;
    }

    private Optional<String> getVowelBeginningWord(Sentence sentence) {
        return sentence.getWords().stream()
                .filter(this::isWordBeginningVowel)
                .findFirst();
    }

    private boolean isWordBeginningVowel(String word) {
        return word.startsWith("a") || word.startsWith("e") ||
                word.startsWith("i") || word.startsWith("o") ||
                word.startsWith("u");
    }

    private Optional<String> getLongestWord(Sentence sentence) {
        return sentence.getWords().stream()
                .max(Comparator.comparingInt(String::length));
    }

    private Sentence getSentenceWithReplacedWords(Sentence sentence, String w1, String w2) {
        List<String> words = new ArrayList<>(sentence.getWords());
        int wIndex1 = words.indexOf(w1);
        int wIndex2 = words.indexOf(w2);

        String temp = words.get(wIndex1);
        words.set(wIndex1, words.get(wIndex2));
        words.set(wIndex2, temp);

        return new Sentence(words, sentence.getType());
    }

    public List<String> getSortedWords() {
        List<String> sortedWords = sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .sorted()
                .collect(Collectors.toList());
        makeTabForNewLetterInSortedWordsList(sortedWords);
        return sortedWords;
    }

    private void makeTabForNewLetterInSortedWordsList(List<String> sortedWords) {
        StringBuilder tabulation = new StringBuilder();
        for (int i = 1; i < sortedWords.size(); i++) {
            String word1 = sortedWords.get(i - 1);
            String word2 = sortedWords.get(i);
            if (word2.charAt(0) != word1.charAt(getIndexSkippedTabulation(word1))) {
                tabulation.append("\t");
            }
            String word = sortedWords.get(i);
            sortedWords.set(i, tabulation + word);
        }
    }

    private int getIndexSkippedTabulation(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != '\t') {
                return i;
            }
        }
        throw new NoSuchElementException("another symbol not found");
    }

    public List<String> getSortedWordsByVowelsPercentage() {
        return sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(String::toLowerCase)
                .sorted(this::compareByVowelsPercentage)
                .collect(Collectors.toList());
    }

    private int compareByVowelsPercentage(String word1, String word2) {
        return Double.compare(getVowelsPercentage(word1), getVowelsPercentage(word2));
    }

    private double getVowelsPercentage(String word) {
        long vowels = word.chars()
                .filter(character -> character == 'a' || character == 'e' || character == 'i' ||
                        character == 'o' || character == 'u')
                .count();
        return (vowels * 100) / word.length();
    }

    public List<String> getVowelBeginningWordsSortedByFirstConsonant() {
        return sentences.stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .map(String::toLowerCase)
                .filter(this::isWordBeginningVowel)
                .sorted(this::compareByFirstConsonantLetter)
                .collect(Collectors.toList());
    }

    private int compareByFirstConsonantLetter(String word1, String word2) {
        return Character.compare(getFirstConsonant(word1), getFirstConsonant(word2));
    }

    private char getFirstConsonant(String word) {
        return (char) word.chars()
                .filter(this::isCharacterConsonant)
                .findFirst()
                .orElse('z' + 1);
    }

    private boolean isCharacterConsonant(int character) {
        return character == 'q' || character == 'w' || character == 'r' ||
                character == 't' || character == 'y' || character == 'p' ||
                character == 's' || character == 'd' || character == 'f' ||
                character == 'g' || character == 'h' || character == 'j' ||
                character == 'k' || character == 'l' || character == 'z' ||
                character == 'x' || character == 'c' || character == 'v' ||
                character == 'b' || character == 'n' || character == 'm';
    }
}
