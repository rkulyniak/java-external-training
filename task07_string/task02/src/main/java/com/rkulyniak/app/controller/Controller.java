package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.Sentence;
import com.rkulyniak.app.model.TextHandler;
import com.rkulyniak.app.model.TextLoader;
import com.rkulyniak.app.model.TextSplitter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    private TextHandler handler;

    public Controller(String fileName) throws FileNotFoundException {
        TextLoader loader = new TextLoader();
        TextSplitter splitter = new TextSplitter();
        handler = new TextHandler();

        String text = loader.loadTextFromFile(fileName);
        List<Sentence> sentences = splitter.getListOfSentences(text);
        handler.setSentences(sentences);
    }

    public List<Sentence> getText() {
        List<Sentence> text = handler.getSentences();
        logText(text);
        return text;
    }

    public long countMaxSentencesWithIdentityWord() {
        long result = handler.countMaxSentencesWithIdentityWord();
        logger.info("returned max count of sentences with identity word [" + result + "]");
        return result;
    }

    public List<Sentence> getLengthSortedSentences() {
        List<Sentence> text = handler.getLengthSortedSentences();
        logText(text);
        return text;
    }

    public Optional<String> getUniqueWordFromFirstSentence() {
        Optional<String> uniqueWord = handler.getUniqueWordFromFirstSentence();
        if (uniqueWord.isPresent()) {
            logger.info("returned unique word from first sentence [" + uniqueWord.get() + "]");
        } else {
            logger.info("first sentence has no unique word");
        }
        return uniqueWord;
    }

    public List<String> getWordsFromInterrogativeSentences(int length) {
        List<String> words = handler.getWordsFromInterrogativeSentences(length);
        logger.info("returned words from interrogative sentences with " + length + " length " + words);
        return words;
    }

    public List<Sentence> replaceVowelBeginningWordWithLongestWord() {
        List<Sentence> sentences = handler.replaceVowelBeginningWordWithLongestWord();
        logText(sentences);
        return sentences;
    }

    private void logText(List<Sentence> sentences) {
        StringBuilder builder = new StringBuilder();
        sentences.forEach(s -> builder.append(s).append("\n"));
        logger.info("returned text\n" +
                builder.deleteCharAt(builder.length() - 1).toString());
    }

    public List<String> getSortedWords() {
        List<String> sortedWords = handler.getSortedWords();
        logger.info("returned sorted words with tab for new letter");
        sortedWords.forEach(word -> logger.info(word));
        return sortedWords;
    }

    public List<String> getSortedWordsByVowelsPercentage() {
        List<String> sortedWords = handler.getSortedWordsByVowelsPercentage();
        logger.info("returned sorted words by vowels percentage");
        sortedWords.forEach(word -> logger.info(word));
        return sortedWords;
    }

    public List<String> getVowelBeginningWordsSortedByFirstConsonant() {
        List<String> sortedWords = handler.getVowelBeginningWordsSortedByFirstConsonant();
        logger.info("returned vowel beginning words which are sorted by consonant");
        sortedWords.forEach(word -> logger.info(word));
        return sortedWords;
    }
}
