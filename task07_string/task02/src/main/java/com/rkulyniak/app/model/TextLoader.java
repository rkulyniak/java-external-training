package com.rkulyniak.app.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Scanner;

public class TextLoader {

    public String loadTextFromFile(String fileName) throws FileNotFoundException {
        StringBuilder builder = new StringBuilder();
        try (Scanner scanner = new Scanner(getFile(fileName))) {
            while (scanner.hasNext()) {
                builder.append(scanner.nextLine()).append(" ");
            }
        }
        return builder.toString();
    }

    private File getFile(String fileName) throws FileNotFoundException {
        URL url = ClassLoader.getSystemClassLoader().getResource(fileName);
        if (url == null) {
            throw new FileNotFoundException(fileName + " not found");
        }
        return new File(url.getFile());
    }
}
