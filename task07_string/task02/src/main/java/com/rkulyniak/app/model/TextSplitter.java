package com.rkulyniak.app.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TextSplitter {

    public List<Sentence> getListOfSentences(String text) {
        text = removeNewLineSpaces(text);
        text = removeDoubleWhitespaces(text);
        String[] sentences = splitTextOnSentences(text);
        return setSentencesToList(sentences);
    }

    private String removeNewLineSpaces(String text) {
        return text.replaceAll("\n", "");
    }

    private String removeDoubleWhitespaces(String text) {
        return text.trim().replaceAll("\\s{2,}", " ");
    }

    private String[] splitTextOnSentences(String text) {
        return text
                .replace(". ", ".\n")
                .replace("? ", "?\n")
                .replace("! ", "!\n")
                .split("\n");
    }

    private List<Sentence> setSentencesToList(String[] arrayOfSentences) {
        return Arrays.stream(arrayOfSentences)
                .map(Sentence::new)
                .collect(Collectors.toList());
    }
}
