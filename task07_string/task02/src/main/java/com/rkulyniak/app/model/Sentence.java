package com.rkulyniak.app.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Sentence {

    private List<String> words;
    private SentenceType type;

    public Sentence(String sentence) {
        words = Arrays.stream(sentence.replaceAll("[^a-zA-Z\\s]", "").split(" "))
                .map(String::new)
                .collect(Collectors.toList());
        type = SentenceType.getSentenceType(sentence.charAt(sentence.length() - 1));
    }

    public Sentence(List<String> words, SentenceType type) {
        this.words = words;
        this.type = type;
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }

    public SentenceType getType() {
        return type;
    }

    public SentenceType getSentenceType() {
        return type;
    }

    @Override
    public String toString() {
        return words.stream()
                .collect(Collectors.joining(" ", "[", "] -> " + type));
    }

    enum SentenceType {
        NARRATIVE, INTERROGATIVE, EXCLAMATORY;

        static SentenceType getSentenceType(char endingSymbol) {
            if (endingSymbol == '.') {
                return SentenceType.NARRATIVE;
            }
            if (endingSymbol == '?') {
                return SentenceType.INTERROGATIVE;
            }
            if (endingSymbol == '!') {
                return SentenceType.EXCLAMATORY;
            }
            throw new IllegalArgumentException("invalid ending symbol");
        }
    }
}
