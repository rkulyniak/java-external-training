package com.rkulyniak.app;

import com.rkulyniak.app.controller.Controller;
import com.rkulyniak.app.view.MainMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;

public class Application {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            Controller controller = new Controller("file.txt");
            MainMenu mainMenu = new MainMenu(controller);

            logger.info("the application has started execution");
            mainMenu.launch();
            logger.info("the application has finished execution");
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            logger.error("the app has been terminated; " + e.getMessage());
        }
    }
}
