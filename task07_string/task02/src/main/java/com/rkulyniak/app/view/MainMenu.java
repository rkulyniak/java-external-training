package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.rkulyniak.app.view.UserInterface.inputNumber;

public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Print text");
            put("2", "2 - Count max sentences with identity word");
            put("3", "3 - Print sentences sorted by length");
            put("4", "4 - Print unique word from first sentence");
            put("5", "5 - Print words from interrogative sentences");
            put("6", "6 - Print sentences with replaced vowel beginning and longest words");
            put("7", "7 - Print sorted words with tab for new letter");
            put("8", "8 - Print sorted words by vowels percentage");
            put("9", "9 - Print vowel beginning words which are sorted by first consonant");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", controller::getText);
            put("2", controller::countMaxSentencesWithIdentityWord);
            put("3", controller::getLengthSortedSentences);
            put("4", controller::getUniqueWordFromFirstSentence);
            put("5", () -> controller.getWordsFromInterrogativeSentences(inputNumber("length")));
            put("6", controller::replaceVowelBeginningWordWithLongestWord);
            put("7", controller::getSortedWords);
            put("8", controller::getSortedWordsByVowelsPercentage);
            put("9", controller::getVowelBeginningWordsSortedByFirstConsonant);
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
