package com.rkulyniak.app.view;

import java.util.Scanner;

class UserInterface {

    private static Scanner input = new Scanner(System.in);

    static int inputNumber(String text) {
        int number;
        while (true) {
            try {
                System.out.print(text + ": ");
                number = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
                continue;
            }
            break;
        }
        return number;
    }
}
