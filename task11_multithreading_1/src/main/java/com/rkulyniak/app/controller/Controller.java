package com.rkulyniak.app.controller;

import com.rkulyniak.app.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    public void startPingPongGame() {
        try {
            int millis = 3000;
            new PingPong(System.out).startGame(millis);
            logger.info("ping-pong game has been executed; duration=" + millis + " ms");
        } catch (IllegalArgumentException e) {
            logger.info("unable to execute ping-pong game; " + e.getMessage());
        }
    }

    public void printFibonacciSequence(int length) {
        try {
            FibonacciSequence sequence = new FibonacciSequence(length);
            logger.info("returned fibonacci sequence " + Arrays.toString(sequence.getSequence()));
            logger.info("sum of elements=" + sequence.getSumOfSequenceElements());
        } catch (IllegalArgumentException e) {
            logger.info("unable to get fibonacci sequence; " + e.getMessage());
        }
    }

    public void startThreadScheduledSleeping(int threadCount) {
        try {
            new ThreadScheduledSleeping(threadCount).start();
            logger.info("thread scheduled sleeping task has been executed; " +
                    "count of threads=" + threadCount);
        } catch (IllegalArgumentException e) {
            logger.info("unable to execute thread scheduled sleeping task; " + e.getMessage());
        }
    }

    public void startMoneyTransferringService() {
        BankAccount bankAccount = new BankAccount();
        double balance;

        balance = bankAccount.runOnSameMonitor();
        logger.info("bank account after running methods on the same monitor = " + balance);
        balance = bankAccount.runOnDifferentMonitors();
        logger.info("bank account after running methods on different monitors = " + balance);
    }

    public void startPipeCommunication() {
        PipeCommunication.start();
        logger.info("pipe communication has been executed");
    }
}
