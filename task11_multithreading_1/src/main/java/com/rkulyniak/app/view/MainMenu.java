package com.rkulyniak.app.view;

import com.rkulyniak.app.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.rkulyniak.app.view.UserInterface.inputInteger;

public class MainMenu implements Menu {

    private Controller controller;

    public MainMenu(Controller controller) {
        this.controller = controller;
    }

    @Override
    public String initMenuName() {
        return "MENU";
    }

    @Override
    public Map<String, String> initDisplay() {
        return new LinkedHashMap<>() {{
            put("1", "1 - Ping-Pong game");
            put("2", "2 - Fibonacci sequence");
            put("3", "3 - Thread scheduled sleeping");
            put("4", "4 - Money transferring service");
            put("5", "5 - Pipe communication");
            put("Q", "Q - exit");
        }};
    }

    @Override
    public Map<String, Runnable> initExecution() {
        return new LinkedHashMap<>() {{
            put("1", () -> controller.startPingPongGame());
            put("2", () -> controller.printFibonacciSequence(inputInteger("length")));
            put("3", () -> controller.startThreadScheduledSleeping(inputInteger("threads")));
            put("4", () -> controller.startMoneyTransferringService());
            put("5", () -> controller.startPipeCommunication());
            put("Q", () -> System.out.println("\nBye!"));
        }};
    }
}
