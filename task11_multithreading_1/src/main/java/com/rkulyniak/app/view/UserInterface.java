package com.rkulyniak.app.view;

import java.util.Scanner;

public class UserInterface {

    private static Scanner input = new Scanner(System.in);

    static int inputInteger(String text) {
        while (true) {
            try {
                System.out.print(text + ": ");
                return Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("invalid input; please re-enter");
            }
        }
    }
}
