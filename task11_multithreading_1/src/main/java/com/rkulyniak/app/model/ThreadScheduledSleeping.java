package com.rkulyniak.app.model;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadScheduledSleeping {

    private ScheduledExecutorService scheduledService;

    public ThreadScheduledSleeping(int threadsCount) {
        if (threadsCount <= 0) {
            throw new IllegalArgumentException("count of threads must be positive");
        }
        scheduledService = Executors.newScheduledThreadPool(threadsCount);
    }

    public void start() {
        Runnable task = () -> {
            System.out.println(Thread.currentThread().getName() + " started execution");
            System.out.println(Thread.currentThread().getName() + " is sleeping...");
            System.out.println(Thread.currentThread().getName() + " finished execution");
        };
        Random random = new Random();

        scheduledService.schedule(task, random.nextInt(10), TimeUnit.SECONDS);
        scheduledService.schedule(task, random.nextInt(10), TimeUnit.SECONDS);
        scheduledService.schedule(task, random.nextInt(10), TimeUnit.SECONDS);
        scheduledService.schedule(task, random.nextInt(10), TimeUnit.SECONDS);
        scheduledService.schedule(task, random.nextInt(10), TimeUnit.SECONDS);
        scheduledService.schedule(task, random.nextInt(10), TimeUnit.SECONDS);

        try { Thread.sleep(10000); } catch (InterruptedException ignored) { }
        scheduledService.shutdownNow();
    }
}
