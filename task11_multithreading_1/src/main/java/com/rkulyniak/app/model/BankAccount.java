package com.rkulyniak.app.model;

public class BankAccount {

    private static double balance = 0;
    private final Object monitor1 = new Object();
    private final Object monitor2 = new Object();

    private void depositOnMonitor1(double amount) {
        synchronized (monitor1) {
            balance += amount;
        }
    }

    private void depositOnMonitor2(double amount) {
        synchronized (monitor2) {
            balance += amount;
        }
    }

    private void withdrawOnMonitor1(double amount) {
        synchronized (monitor1) {
            balance -= amount;
        }
    }

    private void withdrawOnMonitor2(double amount) {
        synchronized (monitor2) {
            balance -= amount;
        }
    }

    public double runOnSameMonitor() {
        Thread t1 = null;
        Thread t2 = null;

        for (int i = 0; i < 100_000; i++) {
            t1 = new Thread(() -> depositOnMonitor1(100));
            t2 = new Thread(() -> withdrawOnMonitor1(100));
            t1.start();
            t2.start();
        }

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException ignored) { }

        return balance;
    }

    public double runOnDifferentMonitors() {
        Thread t1 = null;
        Thread t2 = null;

        for (int i = 0; i < 100_000; i++) {
            t1 = new Thread(() -> depositOnMonitor1(100));
            t2 = new Thread(() -> withdrawOnMonitor2(100));
            t1.start();
            t2.start();
        }

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException ignored) { }

        return balance;
    }
}
