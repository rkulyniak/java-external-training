package com.rkulyniak.app.model;

import java.io.IOException;

public class PipeCommunication {

    public static void start() {
        try {
            Sender sender = new Sender();
            Receiver receiver = new Receiver(sender);

            sender.start();
            receiver.start();

            try { Thread.sleep(5000); } catch (InterruptedException ignored) { }

            sender.interrupt();
            receiver.interrupt();
            try {
                sender.join();
                receiver.join();
            } catch (InterruptedException ignored) { }
        } catch (IOException ignored) { }
    }
}
