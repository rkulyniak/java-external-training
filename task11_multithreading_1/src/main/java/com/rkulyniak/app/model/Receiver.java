package com.rkulyniak.app.model;

import java.io.IOException;
import java.io.PipedReader;

class Receiver extends Thread {

    private PipedReader in;

    Receiver(Sender sender) throws IOException {
        in = new PipedReader(sender.getPipedWriter());
    }

    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("Read: " + (char) in.read());
            }
        } catch (IOException ignored) { }
    }
}
