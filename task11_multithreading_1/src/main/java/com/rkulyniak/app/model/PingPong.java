package com.rkulyniak.app.model;

import java.io.PrintStream;

public class PingPong {

    private PrintStream printStream;

    private Runnable ping;
    private Runnable pong;

    public PingPong(PrintStream printStream) {
        this.printStream = printStream;

        ping = this::makePing;
        pong = this::makePong;
    }

    public void startGame(int millis) {
        if (!isMillisValid(millis)) {
            throw new IllegalArgumentException("invalid millis value");
        }

        Thread pingThread = new Thread(ping);
        Thread pongThread = new Thread(pong);

        startPingPongThreads(pingThread, pongThread);
        sleepMainGameThread(millis);
        interruptPingPongThreads(pingThread, pongThread);
        waitPingPongThreadsDie(pingThread, pongThread);
    }

    private boolean isMillisValid(int millis) {
        return millis >= 0;
    }

    private void startPingPongThreads(Thread pingThread, Thread pongThread) {
        pingThread.start();
        pongThread.start();
    }

    private void sleepMainGameThread(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.err.println(Thread.currentThread().getName() + " was interrupted");
        }
    }

    private void interruptPingPongThreads(Thread pingThread, Thread pongThread) {
        pingThread.interrupt();
        pongThread.interrupt();
    }

    private void waitPingPongThreadsDie(Thread pingThread, Thread pongThread) {
        try {
            pingThread.join();
            pongThread.join();
        } catch (InterruptedException e) {
            System.err.println(Thread.currentThread().getName() + " was interrupted");
        }
    }

    private synchronized void makePing() {
        printStream.println("-- start ping --");
        try {
            while (!Thread.currentThread().isInterrupted()) {
                wait();
                Thread.sleep(500);
                printStream.println("ping");
                notify();
            }
        } catch (InterruptedException ignored) { }
        printStream.println("-- finish ping --");
    }

    private synchronized void makePong() {
        printStream.println("-- start pong --");
        try {
            while (!Thread.currentThread().isInterrupted()) {
                notify();
                wait();
                Thread.sleep(500);
                printStream.println("pong");
            }
        } catch (InterruptedException ignored) { }
        printStream.println("-- finish pong --");
    }
}
