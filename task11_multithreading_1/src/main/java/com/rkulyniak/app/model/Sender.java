package com.rkulyniak.app.model;

import java.io.IOException;
import java.io.PipedWriter;

class Sender extends Thread {

    private PipedWriter out;

    Sender() {
        out = new PipedWriter();
    }

    PipedWriter getPipedWriter() { return out; }

    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                for (char c = 'A'; c <= 'z'; c++) {
                    out.write(c);
                    sleep(250);
                }
            }
        } catch (IOException | InterruptedException ignored) { }
    }
}
