package com.rkulyniak.app.model;

import java.util.Arrays;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FibonacciSequence {

    private int F1;
    private int F2;
    private int F3;
    private AtomicInteger index;

    private int[] sequence;

    public FibonacciSequence(int length) {
        F1 = 0;
        F2 = 1;
        F3 = F1 + F2;
        index = new AtomicInteger(3);

        buildSequence(length);
    }

    public int[] getSequence() {
        return Arrays.copyOf(sequence, sequence.length);
    }

    public int getSumOfSequenceElements() {
        Callable<Integer> sumOfElements = () -> Arrays.stream(sequence).sum();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> result = executorService.submit(sumOfElements);

        int sum;
        try {
            sum = result.get();
        } catch (InterruptedException | ExecutionException ignored) {
            sum = 0;
        }
        return sum;
    }

    private void buildSequence(int length) {
        if (!isLengthValid(length)) {
            throw new IllegalArgumentException("invalid length value");
        }

        if (length == 1) {
            sequence = new int[]{F1};
        } else if (length == 2) {
            sequence = new int[]{F1, F2};
        } else {
            buildSequenceForMoreThanTwoElements(length);
        }
    }

    private boolean isLengthValid(int length) {
        return length > 0;
    }

    private void buildSequenceForMoreThanTwoElements(int length) {
        sequence = new int[length];
        initializeFirstThreeElements();

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Runnable updater = this::updateFibonacciNumbers;
        Runnable writer = () -> sequence[index.getAndIncrement()] = F3;

        while (index.get() < length) {
            try {
                executorService.submit(updater).get();
                executorService.submit(writer).get();
            } catch (InterruptedException | ExecutionException ignored) { }
        }
    }

    private void initializeFirstThreeElements() {
        sequence[0] = F1;
        sequence[1] = F2;
        sequence[2] = F3;
    }

    private void updateFibonacciNumbers() {
        F1 = F2;
        F2 = F3;
        F3 = F1 + F2;
    }
}
